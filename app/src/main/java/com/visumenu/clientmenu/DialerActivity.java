package com.visumenu.clientmenu;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.visumenu.clientmenu.Utils.ContactsLoader;
import com.visumenu.clientmenu.Utils.ViewUtil;
import com.visumenu.clientmenu.fragment.CallsFragment_;
import com.visumenu.clientmenu.fragment.ContactsFragment_;
import com.visumenu.clientmenu.fragment.DialerFragment_;
import com.visumenu.clientmenu.fragment.FavoritesFragment_;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.User;
import com.visumenu.clientmenu.service.DataFetchService;
import com.visumenu.clientmenu.service.EventMessage;
import com.visumenu.clientmenu.service.FetchDataConstants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by user on 12.05.16.
 */

@EActivity(R.layout.activity_dialer)
public class DialerActivity extends AppCompatActivity {

    private static final int REQUEST_PERMISSION_CALL = 100;
    private static final int REQUEST_READ_CONTACTS = 101;
    private static final int REQUEST_CALL_LOG = 102;

    @ViewById
    TabLayout dialerTabs;

    Fragment currentFragment;

    MaterialDialog progressDialog;

    String phone;

    @AfterViews
    void initDialer(){
//        performReadContactsWithCheckPermission();
//        CallUtils.getCallDetails(this);
//        new ContactsLoader(this).fetchContacts();
        User user = Database.getInstance(this).getUser();
        if (user == null || user.getAccessToken() == null){
            startActivity(new Intent(this, IvrActivity.class));
            finish();
        }

        progressDialog = new MaterialDialog.Builder(this)
                .title(R.string.please_wait)
                .content(R.string.loading_ivrs)
                .progress(true, 0)
                .canceledOnTouchOutside(false)
                .cancelable(false)
                .build();

        Toolbar toolbar = ((Toolbar) findViewById(R.id.toolbar));
        if (toolbar != null)
            toolbar.setContentInsetsAbsolute(0, 0);

        openDialer();
        ViewUtil.closeKeyboard(this, currentFragment.getView());
        dialerTabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()){
                    case 0:
                        openDialer();
                        break;

                    case 1:
                        openCallHistory();
                        break;

                    case 2:
                        openFavorites();
                        break;

                    case 3:
                        openContacts();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        Typeface tfTabs =
                Typeface.createFromAsset(getAssets(), "HelveticaNeueCyr-Roman.otf");
        ViewUtil.changeTabsFont(dialerTabs, tfTabs, true, R.style.ShadowText);
    }

    void changeFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, currentFragment)
                .addToBackStack(currentFragment.getClass().getName())
                .commit();
    }

    void openDialer(){
        currentFragment = new DialerFragment_();
        changeFragment();
    }

    void openCallHistory(){
        currentFragment = new CallsFragment_();
        changeFragment();
    }

    void openFavorites(){
        currentFragment = new FavoritesFragment_();
        changeFragment();
    }

    void openContacts(){
        currentFragment = new ContactsFragment_();
        changeFragment();
    }

    public void cancelSendNumber(){
        setResult(RESULT_CANCELED);
        finish();
    }

    void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBackPressed() {
        cancelSendNumber();
    }

    public void showMenu(String ivrNumber){
        Intent intent = new Intent(this, IvrActivity.class);
        intent.putExtra(FetchDataConstants.IVR_NUMBER, ivrNumber);
        startActivity(intent);
    }

    public void openCompanies(){
        Intent intent = new Intent(this, IvrActivity.class);
        intent.putExtra(IvrActivity.OPEN_DIALER, false);
        startActivity(intent);
    }

    public void performReadContactsWithCheckPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_READ_CONTACTS);
            return;
        }
        new ContactsLoader(this).fetchContacts();
    }

    public void performDialWithCheckPermission(String numberString) {
        if (!numberString.equals("")) {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        REQUEST_PERMISSION_CALL);

                phone = numberString;
                return;
            }
            startActivity(getIntentDialAndStoreNumber(numberString));
        }
    }

    private Intent getIntentDialAndStoreNumber(String numberString){
        Util.storeOutgoingPhoneNumber(getApplicationContext(), numberString);
        if(numberString.contains("#"))
            numberString = numberString.replaceAll("#", Uri.encode("#"));
        Uri number = Uri.parse("tel:" + numberString);
        return new Intent(Intent.ACTION_CALL, number);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CALL: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(getIntentDialAndStoreNumber(phone));
                } else {
                    showAlertDialog("Call permission required", null);
                }
            }
            break;

            case REQUEST_READ_CONTACTS:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    new ContactsLoader(this).fetchContacts();
                } else {
                    showAlertDialog("Read contacts permission required", null);
                }
                break;


        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus eventBus = EventBus.getDefault();
        eventBus.register(this);
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    public void updateIVRlist(){
        progressDialog.show();
        Intent intent = new Intent(this, DataFetchService.class);
        intent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_IVRS_VALUE);
        startService(intent);
    }

    void proceedError(String errorMessage, MaterialDialog.SingleButtonCallback callback){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        showAlertDialog(errorMessage, callback);
    }

    public void showAlertDialog(String message, MaterialDialog.SingleButtonCallback callback) {
        showDialog(getString(R.string.error), message, null, callback);
    }

    public void showDialog(String title, String message, View view,
                           MaterialDialog.SingleButtonCallback positiveCallback){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(this)
                .title(title)
                .onPositive( positiveCallback == null ?
                        new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog dialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        } : positiveCallback)
                .positiveText(android.R.string.ok);

        if(view == null)
            builder.content(message);
        else
            builder.customView(view, true);

        builder.show();
    }

    public void dismissProgressDialog() {
        progressDialog.dismiss();
    }

    @Subscribe
    public void onEvent(final EventMessage message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (message.getType()){

                    case FetchDataConstants.TYPE_ERROR:
                        dismissProgressDialog();
                        proceedError(message.getMessage(), null);
                        break;

                    case FetchDataConstants.TYPE_SUCCESS:
                        dismissProgressDialog();
                        View view = getCurrentFocus();
                        if(view != null)
                            ViewUtil.openKeyboard(DialerActivity.this, view);
                        break;
                }
            }
        });
    }

    public void sendSearchRequest(String query) {
//        if(currentFragment instanceof CompaniesListFragment_) {
//            View view =
//                    ((CompaniesListFragment_) currentFragment)
//                            .getSearchCompaniesView();
//            ViewUtil.closeKeyboard(IvrActivity.this, view);
//        }

        progressDialog.show();
        Intent intent = new Intent(this, DataFetchService.class)
                .putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_IVRS_SEARCH)
                .putExtra(FetchDataConstants.SEARCH_STRING, query);
        startService(intent);
    }

}
