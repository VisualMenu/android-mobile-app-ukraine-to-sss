package com.visumenu.clientmenu.request;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 30.03.16.
 */
public class UserRegisterResponse {

    String token;

    @SerializedName("cognito_token")
    String cognitoToken;

    long expires;
    @SerializedName("required_fields")
    List<String> requiredFields;

    UserProfile profile;

    @SerializedName("sms_code_valid")
    Integer phoneNumberIsValid;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCognitoToken() {
        return cognitoToken;
    }

    public void setCognitoToken(String cognitoToken) {
        this.cognitoToken = cognitoToken;
    }

    public long getExpires() {
        return expires;
    }

    public void setExpires(long expires) {
        this.expires = expires;
    }

    public List<String> getRequiredFields() {
        return requiredFields;
    }

    public void setRequiredFields(List<String> requiredFields) {
        this.requiredFields = requiredFields;
    }

    public UserProfile getProfile() {
        return profile;
    }

    public void setProfile(UserProfile profile) {
        this.profile = profile;
    }

    public Integer getPhoneNumberIsValid() {
        return phoneNumberIsValid;
    }

    public void setPhoneNumberIsValid(Integer phoneNumberIsValid) {
        this.phoneNumberIsValid = phoneNumberIsValid;
    }
}
