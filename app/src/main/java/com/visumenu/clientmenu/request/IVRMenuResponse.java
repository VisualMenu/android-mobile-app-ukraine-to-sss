package com.visumenu.clientmenu.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 13.04.16.
 */
public class IVRMenuResponse {

    String success;

    String message;

    @SerializedName("file")
    MenuResponse menuBody;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MenuResponse getMenuBody() {
        return menuBody;
    }

    public void setMenuBody(MenuResponse menuBody) {
        this.menuBody = menuBody;
    }
}
