package com.visumenu.clientmenu.request;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by user on 11.03.16.
 */

public interface API {

    @GET("{url}")
    Call<IVRsResponse> getIVRs(@Path(value = "url", encoded = true) String url,
                               @Query("token") String token,
                               @Query("list_type") String type);

    @GET("{url}")
    Call<IVRsResponse> getIVRs(@Path(value = "url", encoded = true) String url,
                               @Query("token") String token,
                               @Query("list_type") String type,
                               @Query("filter") String query);

    @GET("{url}")
    Call<IVRMenuResponse> getMenu(@Path(value = "url", encoded = true) String url,
                                  @Query("token") String token,
                                  @Query("ivr_number") String ivrNumber);

    @GET("{url}")
    Call<ResponseRecent> getRecent(@Path(value = "url", encoded = true) String url,
                               @Query("token") String token);

    @GET("{url}")
    Call<CallStatusResponse> getCallbackStatus(@Path(value = "url", encoded = true) String url,
                                         @Query("token") String token,
                                         @Query("callback") String callbackToken);

    @GET("api/dtmf/get/top")
    Call<IVRMenuResponse> getDtmfTop(@Query("token") String token,
                                  @Query("ivr_number") String ivrNumber);

    @FormUrlEncoded
    @POST("api/registration/")
    Call<ResponseBody> register(@Field("type") String type,
                                @Field("email") String email,
                                @Field("phone_number") String phoneNumber,
                                @Field("password") String password,
                                @Field("first_name") String firstName,
                                @Field("last_name") String lastName);


    //login by credentials
    @FormUrlEncoded
    @POST("api/authorization/")
    Call <ResponseBody> login(@Field("type") String type,
                              @Field("email") String login,
                              @Field("password") String password);

    @FormUrlEncoded
    @POST("api/registration/")
    Call <ResponseBody> registerViaSocialNetwork(@Field("type") String type,
                                                 @Field("social_network") String socialNetwork,
                                                 @Field("identificator") String userId,
                                                 @Field("phone_number") String phoneNumber,
                                                 @Field("first_name") String firstName,
                                                 @Field("last_name") String lastName);

    @FormUrlEncoded
    @POST("api/sms/verification/")
    Call <ResponseBody> verifySMSCode(@Field("code") String code,
                                      @Field("phone_number") String phoneNumber,
                                      @Field("token") String token);

    @FormUrlEncoded
    @POST("api/sms/request/")
    Call <ResponseBody> requestSMSCode(@Field("phone_number") String phoneNumber,
                                       @Field("token") String token);

    @FormUrlEncoded
    @PUT("api/profile/update/")
    Call<ResponseBody> updateProfile(@Field("token") String token,
                                     @Field("username") String username,
                                     @Field("password") String password,
                                     @Field("phone_number") String phoneNumber,
                                     @Field("first_name") String firstName,
                                     @Field("last_name") String lastName,
                                     @Field("address") String address,
                                     @Field("email") String email,
                                     @Field("gender") String gender);


    @POST("/getToken")
    Call<Object> getToken(@Field("name") String name,
                          @Field("key") String key,
                          @Field("device_id") String deviceId,
                          @Field("secret_key") String keySecret);

    @FormUrlEncoded
    @POST("/api/tracking/")
    Call<ResponseBody> track(@Field("token") String token,
                       @Field("events") String data);

    @FormUrlEncoded
    @POST("api/password/forgot")
    Call<ResponseBody> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("api/password/reset")
    Call<ResponseBody> resetPassword(@Field("code") String code,
                                     @Field("device") String deviceId,
                                     @Field("password") String password);

    @FormUrlEncoded
    @POST("api/feedback/send/")
    Call<ResponseBody> feedback(@Field("token") String token,
                                @Field("ivr_number") String ivrNumber,
                                @Field("dtmf_seq") String dtmfSeq,
                                @Field("rate") String rate,
                                @Field("description") String description);

    @FormUrlEncoded
    @POST("api/callback/request/")
    Call<ResponseBody> callBack(@Field("token") String token,
                          @Field("ivr_number") String ivrNumber,
                          @Field("dtmf_seq") String dtmfSeq,
                          @Field("dtmf_time") String stime,
                          @Field("sip_number") String sipNumber);

    @FormUrlEncoded
    @POST("api/dtmf/favorites/add")
    Call<ResponseBody> postLike(@Field("token") String token,
                                @Field("ivr_number") String ivrNumber,
                                @Field("dtmf_seq") String dtmfSeq,
                                @Field("dtmf_stime") String stime);

    @DELETE("api/dtmf/favorites/delete")
    Call<ResponseBody> deleteLike(@Query("token") String token,
                                  @Query("ivr_number") String ivrNumber,
                                  @Query("dtmf_seq") String dtmfSeq,
                                  @Query("dtmf_stime") String stime);

    @Multipart
    @POST("api/record/call/add")
    Call<ResponseBody> sendRecordFile(@Part("token") RequestBody token,
                                      @Part("ivr_number") RequestBody ivrNumber,
                                      @Part("phone_number") RequestBody phoneNumber,
                                      @Part("file\"; filename=\"record.amr\" ") RequestBody file);

    @DELETE("api/callback/delete")
    Call<CallStatusResponse> cancelCallbackRequest(@Query("token") String token,
                                             @Query("callback") String callbackToken);
}
