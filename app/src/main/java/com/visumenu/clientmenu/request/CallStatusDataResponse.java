package com.visumenu.clientmenu.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 02.06.16.
 */
public class CallStatusDataResponse {

    @SerializedName("channel_state")
    int channelState;

    @SerializedName("ivr_number")
    String ivrNumber;

    @SerializedName("ivr_menu_dtmf_seq")
    String ivrMenuDtmfSeq;

    @SerializedName("ivr_menu_stime")
    String ivrMenuStime;

    @SerializedName("ivr_cli_number")
    String ivrCliNumber;

    @SerializedName("ivr_call_status")
    String ivrCallStatus;

    @SerializedName("ivr_created_datetime")
    String ivrCreatedDatetime;

    @SerializedName("ivr_calling_type")
    String ivrCallingType;

    public int getChannelState() {
        return channelState;
    }

    public void setChannelState(int channelState) {
        this.channelState = channelState;
    }

    public String getIvrNumber() {
        return ivrNumber;
    }

    public void setIvrNumber(String ivrNumber) {
        this.ivrNumber = ivrNumber;
    }

    public String getIvrMenuDtmfSeq() {
        return ivrMenuDtmfSeq;
    }

    public void setIvrMenuDtmfSeq(String ivrMenuDtmfSeq) {
        this.ivrMenuDtmfSeq = ivrMenuDtmfSeq;
    }

    public String getIvrMenuStime() {
        return ivrMenuStime;
    }

    public void setIvrMenuStime(String ivrMenuStime) {
        this.ivrMenuStime = ivrMenuStime;
    }

    public String getIvrCliNumber() {
        return ivrCliNumber;
    }

    public void setIvrCliNumber(String ivrCliNumber) {
        this.ivrCliNumber = ivrCliNumber;
    }

    public String getIvrCallStatus() {
        return ivrCallStatus;
    }

    public void setIvrCallStatus(String ivrCallStatus) {
        this.ivrCallStatus = ivrCallStatus;
    }

    public String getIvrCreatedDatetime() {
        return ivrCreatedDatetime;
    }

    public void setIvrCreatedDatetime(String ivrCreatedDatetime) {
        this.ivrCreatedDatetime = ivrCreatedDatetime;
    }

    public String getIvrCallingType() {
        return ivrCallingType;
    }

    public void setIvrCallingType(String ivrCallingType) {
        this.ivrCallingType = ivrCallingType;
    }
}
