package com.visumenu.clientmenu.request;

import com.google.gson.annotations.SerializedName;
import com.visumenu.clientmenu.model.RecentItem;

import java.util.List;

/**
 * Created by user on 14.04.16.
 */
public class IVRsRecent {

    @SerializedName("IVR")
    List<RecentItem> recentItemList;

    String success;
    String version;

    public List<RecentItem> getRecentItemList() {
        return recentItemList;
    }

    public void setRecentItemList(List<RecentItem> recentItemList) {
        this.recentItemList = recentItemList;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
