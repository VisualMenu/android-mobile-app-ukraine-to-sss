package com.visumenu.clientmenu.request;

import com.google.gson.annotations.SerializedName;
import com.visumenu.clientmenu.model.IVRMenuItem;

import java.util.List;

/**
 * Created by user on 14.03.16.
 */
public class MenuResponse {

    @SerializedName("ivr_number")
    private String ivrNumber;

    @SerializedName("ivr_name")
    private String ivrName;

    @SerializedName("ivr_version")
    private String ivrVersion;

    @SerializedName("ivr_menus")
    private List<IVRMenuItem> menus;

    public String getIvrNumber() {
        return ivrNumber;
    }

    public void setIvrNumber(String ivrNumber) {
        this.ivrNumber = ivrNumber;
    }

    public String getIvrName() {
        return ivrName;
    }

    public void setIvrName(String ivrName) {
        this.ivrName = ivrName;
    }

    public String getIvrVersion() {
        return ivrVersion;
    }

    public void setIvrVersion(String ivrVersion) {
        this.ivrVersion = ivrVersion;
    }

    public List<IVRMenuItem> getMenus() {
        return menus;
    }

    public void setMenus(List<IVRMenuItem> menus) {
        this.menus = menus;
    }
}
