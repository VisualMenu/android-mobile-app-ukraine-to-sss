package com.visumenu.clientmenu.request;

import android.content.Context;
import android.util.Log;

import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Util;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.service.FetchDataConstants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 09.08.16.
 */
public class IVRsResponseCallback implements Callback<IVRsResponse> {

    final String TAG = "IVRsResponseCallback";

    boolean searchResult;
    Context mContext;

    public IVRsResponseCallback(Context context, boolean searchResult) {
        this.searchResult = searchResult;
        this.mContext = context;
    }

    @Override
    public void onResponse(Call<IVRsResponse> call, Response<IVRsResponse> response) {

        IVRsResponse ivrsResponse = response.body();
        if (ivrsResponse == null){
            Log.e(TAG, "response null");
            if (searchResult)
                Util.sendMessageEvent(FetchDataConstants.TYPE_SUCCESS, null);
            return;
        }
        Database database = Database.getInstance(mContext);
        if(!searchResult)
            database.clearIvrList();
        if (ivrsResponse.getIvRsListResponse().getIvrsList() != null)
            database.addToIVRs(ivrsResponse.getIvRsListResponse().getIvrsList(), searchResult);
        if (!searchResult)
            Util.setLastUpdateIVRs(mContext, System.currentTimeMillis());
        Util.sendMessageEvent(FetchDataConstants.TYPE_SUCCESS, null);
        Log.d(TAG, "response " + ivrsResponse.toString() + " isSearch " + searchResult);
    }

    @Override
    public void onFailure(Call<IVRsResponse> call, Throwable t) {
        Log.e(TAG, "error fetching IVRs");
        t.printStackTrace();
        Util.sendMessageEvent(FetchDataConstants.TYPE_ERROR,
                mContext.getString(R.string.failed_loading_ivrs));
    }

}
