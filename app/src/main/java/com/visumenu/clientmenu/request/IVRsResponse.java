package com.visumenu.clientmenu.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 11.03.16.
 */

public class IVRsResponse {

    @SerializedName("ivr_list")
    IVRsListResponse ivRsListResponse;

    String message;

    public IVRsListResponse getIvRsListResponse() {
        return ivRsListResponse;
    }

    public void setIvRsListResponse(IVRsListResponse ivRsListResponse) {
        this.ivRsListResponse = ivRsListResponse;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
