package com.visumenu.clientmenu.request;

/**
 * Created by user on 15.04.16.
 */
public class CallStatusResponse {

    String success;
    String message;

    CallStatusDataResponse data;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public CallStatusDataResponse getData() {
        return data;
    }

    public void setData(CallStatusDataResponse data) {
        this.data = data;
    }
}
