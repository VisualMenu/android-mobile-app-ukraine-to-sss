package com.visumenu.clientmenu.request;

import com.google.gson.annotations.SerializedName;
import com.visumenu.clientmenu.model.IVR;

import java.util.List;

/**
 * Created by user on 12.04.16.
 */
public class IVRsListResponse {
    @SerializedName("IVR")
    List<IVR> ivrsList;

    String status;
    String version;

    public List<IVR> getIvrsList() {
        return ivrsList;
    }

    public String getStatus() {
        return status;
    }

    public String getVersion() {
        return version;
    }
}
