package com.visumenu.clientmenu.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 05.05.16.
 */
public class UserProfile {
    String username;

    @SerializedName("phone_number")
    String phoneNumber;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
