package com.visumenu.clientmenu.request;

import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 14.04.16.
 */
public class ResponseRecent {

    String success;
    String message;

    @SerializedName("ivr_list")
    IVRsRecent recent;

    String status;
    String version;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public IVRsRecent getRecent() {
        return recent;
    }

    public void setRecent(IVRsRecent recent) {
        this.recent = recent;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
