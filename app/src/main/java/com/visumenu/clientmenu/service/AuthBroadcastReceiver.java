package com.visumenu.clientmenu.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Broadcast receiver for mainActivity.
 * Receive intents success or not registration and authorization
 */

public abstract class AuthBroadcastReceiver extends BroadcastReceiver {

    final String TAG = "AuthBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {

        if(!intent.getAction().equals(FetchDataConstants.AUTH_ACTION))
            return;
        Log.d(TAG, "receive with auth action");
        int request_code = intent.getIntExtra(FetchDataConstants.REQUEST_CODE, 0);
        int status = intent.getIntExtra(FetchDataConstants.STATUS, 0);
        boolean full_user_data = intent.getBooleanExtra(FetchDataConstants.FULL_USER_INFO, true);
        switch (request_code){
            case FetchDataConstants.CODE_SIGN_UP:
                Log.d(TAG, "sign up");
                if(status == FetchDataConstants.CODE_SUCCESS){
                    Log.d(TAG, "sign up success");
                    onSuccess(full_user_data, true);
                }else if(status == FetchDataConstants.CODE_AUTH_SUCCESS_WITHOUT_CONFIRM) {
                    Log.d(TAG, "user signed up without phonenumber confirmation");
                    onSuccess(full_user_data, false);
                }else{
                    onFailure(intent.getStringExtra(FetchDataConstants.ERROR_MESSAGE));
                    Log.d(TAG, "sign up failure");
                }

                break;

            case FetchDataConstants.CODE_SIGN_IN:
                Log.d(TAG, "sign in");
                if(status == FetchDataConstants.CODE_SUCCESS){
                    Log.d(TAG, "sign in success");
                    onSuccess(full_user_data, true);
                }else if(status == FetchDataConstants.CODE_AUTH_SUCCESS_WITHOUT_CONFIRM) {
                    Log.d(TAG, "user signed in without phonenumber confirmation");
                    onSuccess(full_user_data, false);
                } else {
                    onFailure(intent.getStringExtra(FetchDataConstants.ERROR_MESSAGE));
                    Log.d(TAG, "sign in failure");
                }

                break;
        }
    }

    abstract public void onSuccess(boolean fullUserData, boolean phoneNumberConfirmed);
    abstract public void onFailure(String errorMessage);
}
