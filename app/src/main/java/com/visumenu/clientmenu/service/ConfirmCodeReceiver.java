package com.visumenu.clientmenu.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import com.visumenu.clientmenu.BuildConfig;
import com.visumenu.clientmenu.R;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;

/**
 * Created by user on 21.11.16.
 */

public class ConfirmCodeReceiver extends BroadcastReceiver{

    final String TAG = "ConfirmCodeReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle pudsBundle = intent.getExtras();
        Object[] pdus = (Object[]) pudsBundle.get("pdus");
        SmsMessage messages = SmsMessage.createFromPdu((byte[]) pdus[0]);
        String phoneNumber = messages.getDisplayOriginatingAddress();

        Log.d(TAG, "phoneNumber " + phoneNumber);

        String[] numbers = context.getResources().getStringArray(R.array.phone_numbers_confirm);

        try {
            for(String number : numbers){
                if(phoneNumber.contains(number)) {
                    sendCodeToFragment(messages);
                    return;
                }
            }

            if (BuildConfig.DEBUG && phoneNumber.contains("380935415063")) {
                sendCodeToFragment(messages);
            }
        } catch (ParseException e) {
            Log.e(TAG, e.getMessage());
        }

    }

    void sendCodeToFragment(SmsMessage messages) throws ParseException   {
        String messageBody = messages.getMessageBody();
        Log.d(TAG,  messageBody);
        String[] messageParts = messageBody.split(":");
        if (messageParts.length == 0) {
            throw new ParseException("Incorrect message body", 0);
        }

        EventMessage message =
                new EventMessage(
                        FetchDataConstants.TYPE_CONFIRMATION_CODE_RECEIVED,
                        messageParts[messageParts.length - 1].trim()
                );

        EventBus.getDefault().post(message);
    }


}
