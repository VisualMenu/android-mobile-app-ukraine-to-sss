package com.visumenu.clientmenu.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.Preferences;
import com.visumenu.clientmenu.Util;
import com.visumenu.clientmenu.Utils.DtmfUtils;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.IVR;
import com.visumenu.clientmenu.model.User;

import java.io.IOException;

/**
 * Receiver for intercept and end call
 */

public class GSMCallReceiver extends BroadcastReceiver {

    private static final long CALL_TIMEOUT = 10000;

    final String TAG = "GSMCallReceiver";
    Context context;
    private static MediaRecorder recorder;
    private static String phoneNumber;
    private static Handler mHandler;
    private static boolean recordStarted = false;
//    private static WavRecorder wavRecorder;
//    private static ExtAudioRecorder wavRecorder;
    private static final String DIRECTORY_NAME = "visumenu";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive " + intent.getAction());
        this.context = context;

//        if(intent.getAction().equals("android.intent.action.NEW_OUTGOING_CALL")){
//            abortBroadcast();
//            return;
//        }

        SharedPreferences sp =
                context.getSharedPreferences(
                        Preferences.CALL_PREFERECES, Context.MODE_PRIVATE);
        if(intent.getAction().equals("android.intent.action.PHONE_STATE")){
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            Log.d(TAG, "state changed to " + state);
            if(state.equals(TelephonyManager.EXTRA_STATE_RINGING) ||
                    state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)){
                Log.d(TAG, "phone state " + state);

                String phoneNumber = Util.getOutGoingPhoneNumber(context);

                if(phoneNumber != null && !phoneNumber.equals("")){
                    Util.storeOutgoingPhoneNumber(context, "");
                    Log.d(TAG, "starting calling activity");
//                    notifyAboutOutgoingCallFromDialer(context, phoneNumber);
                    return;
                }



                long time = sp.getLong(Preferences.TIME_LAST_CALL, 0);

                if(time == 0)
                    return;

                SharedPreferences.Editor editor =  sp.edit();
                if(time - System.currentTimeMillis() == CALL_TIMEOUT){
                    setLastCallTime(editor, 0);
                    return;
                }

                String number = sp.getString(Preferences.OUTGOING_CALL_NUMBER, "");

                if(number.equals("")){
                    setLastCallTime(editor, 0);
                    return;
                }

                Util.endCall(context);

                editor.putString(Preferences.OUTGOING_CALL_NUMBER, "");
                setLastCallTime(editor, 0);
//
//                openMenu(context, number);
            }
            if(state.equals(TelephonyManager.EXTRA_STATE_IDLE)){
                if(recorder != null){
                    try {
                        stopRecord();
                    } catch (Exception e){
                      Log.e(TAG, "error stoping record");
                    }

                }
                Util.storeOutgoingPhoneNumber(context, "");
                Log.d(TAG, "call ended");
            }
            return;
        }

        String phoneNumber =
                intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        Log.d(TAG, "phonenumber " + phoneNumber);

        GSMCallReceiver.phoneNumber = phoneNumber;
        int comeCount = phoneNumber.length() - phoneNumber.replace(",", "").length();
        int startRecordDelayInSeconds =
                comeCount * DtmfUtils.COME_DELAY_IN_SECONDS;
        mHandler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    startRecord();
                    recordStarted = true;
                }catch (Exception e){
                    Log.e(TAG, "error starting recodrd");
                    e.printStackTrace();
                }
            }
        };
        mHandler.postDelayed(runnable, startRecordDelayInSeconds * 1000);

        if(phoneNumber == null)
            return;

//        String storedPhoneNumber = Util.getOutGoingPhoneNumber(context);
//
//        if(storedPhoneNumber != null
//                && phoneNumber.equals(storedPhoneNumber)
//                && !storedPhoneNumber.equals("")){
//            abortBroadcast();
//            return;
//        }

        IVR ivr = Database.getInstance(context).findIVRbyNumber(phoneNumber);

        if(ivr != null){
            sp.edit().putString(Preferences.OUTGOING_CALL_NUMBER, phoneNumber)
                    .putLong(Preferences.TIME_LAST_CALL, System.currentTimeMillis())
                    .apply();
        }

        Log.d(TAG, "phone number " + phoneNumber);

    }

    void setLastCallTime(SharedPreferences.Editor editor, long time){
        editor.putLong(Preferences.TIME_LAST_CALL, time)
                .apply();
    }

    void openMenu(Context context, String phoneNumber){
        Intent intent = new Intent(context, IvrActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(FetchDataConstants.IVR_NUMBER, phoneNumber);
        context.startActivity(intent);
    }

    void startRecord() throws IOException {
        Log.d(TAG, "startRecord");
        recorder = new MediaRecorder();
        recorder.setAudioChannels(1);
        recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        else
            recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        String dirPAth = Util.createDirectory(context, DIRECTORY_NAME);
        recorder.setOutputFile(dirPAth + "/record.amr");
        Log.d(TAG, "filename " + dirPAth + "/record.amr");
        recorder.prepare();
        recorder.start();
    }

    void stopRecord(){
        Log.d(TAG, "stopRecord");
        if(!recordStarted)
            return;

        if(mHandler != null){
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }

        recordStarted = false;
        recorder.stop();
        recorder.release();
        Intent sendFileIntent = new Intent(context, SendStatDataService.class);
        String dirPAth = Util.createDirectory(context, DIRECTORY_NAME);
        String path = (dirPAth + "/record.amr");
        User user = Database.getInstance(context).getUser();
        if (user != null)
            sendFileIntent.putExtra(FetchDataConstants.SEND_TYPE, FetchDataConstants.CODE_SEND_FILE)
                    .putExtra(FetchDataConstants.PHONE_NUMBER, user.getPhoneNumber())
                    .putExtra(FetchDataConstants.IVR_NUMBER, phoneNumber)
                    .putExtra(FetchDataConstants.FILE_PATH, path);

        context.startService(sendFileIntent);
        recorder = null;
        phoneNumber = null;
    }
//
//     void notifyAboutOutgoingCallFromDialer(final Context context, final String phoneNumber){
//         Handler handler = new Handler();
//         handler.postDelayed(new Runnable() {
//             @Override
//             public void run() {
//                 context.startActivity(new Intent(context, DialerActivity.class)
//                         .putExtra(FetchDataConstants.PHONE_NUMBER, phoneNumber)
//                         .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//             }
//         }, 2000);
//
//     }

}
