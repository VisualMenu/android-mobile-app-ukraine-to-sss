package com.visumenu.clientmenu.service;

/**
 * Constants for data fetch
 */

public class FetchDataConstants {

    public static final int CODE_IVRS_VALUE = 0x01;
    public static final int CODE_MENU_VALUE = 0x02;
    public static final int CODE_RECENT_VALUE = 0x08;
    public static final int CODE_SIGN_UP = 0x03;
    public static final int CODE_SIGN_IN = 0x04;
    public static final int CODE_UPDATE_PROFILE = 0x05;

    public static final int CODE_SEND_STAT = 0;
    public static final int CODE_SEND_FEEDBACK = 1;
    public static final int CODE_SEND_FILE = 2;

    public static final int CODE_FORGOT_PASSWORD = 0x06;
    public static final int CODE_RESET_PASSWORD = 0x07;
    public static final int CODE_REQUEST_CALLBACK = 0x09;
    public static final int CODE_CHANGED_CALLBACK_STATUS = 0x0A;
    public static final int CODE_LIKE = 0x0B;
    public static final int CODE_FETCH_DETMF_TOP = 0x0D;
    public static final int CODE_CANCEL_CALLBACK = 0x0E;

    public static final int CODE_IVRS_SEARCH = 0x0F;

    public static final int CODE_REQUEST_SMS = 0x10;
    public static final int CODE_VERIFY_SMS_CODE = 0x11;

    public static final String SEARCH_STRING = "search_string";
    public static final String ADD_LIKE = "add_like";

    public static final String REQUEST_CODE = "request_code";
    public static final String IVR_NUMBER = "ivr_number";
    public static final String IVR_NAME = "ivr_name";
    public static final String BUCKET_NAME = "visumenu-ivr-json";
    public static final String CODE_RESET_PWD = "code_forgot_pwd";

    public static final int CODE_SUCCESS = 0x10;

    public static final int CODE_AUTH_SUCCESS_WITHOUT_CONFIRM = 0x11;
    public static final int CODE_FAILED = 0xffffffff;

    public static final int CODE_SMS_INVALID = 0;

    public static final String VERIFICATION_CODE = "verification_code";
    public static final String AUTH_ACTION = "com.visumenu.clientmenu.auth_action";
    public static final String FULL_USER_INFO = "full_user_info";

    public static final String SIP_NUMBER = "sip_number";
    //User data fields
    public static final String USERNAME = "username";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String EMAIL = "email";
    public static final String GENDER = "gender";
    public static final String FIRST_NAME = "first_name";
    public static final String LAST_NAME = "last_name";
    public static final String ADDRESS = "address";
    public static final String PASSWORD = "password";
    public static final String MESSAGE = "message";

    public static final String ACCESS_TOKEN = "access_token";

    public static final String SOCIAL_NETWORK = "social_network";
    public static final String USER_ID_SOCIAL_NETWORK = "identificator";

    public static final String ERROR_MESSAGE = "error message";
    public static final String STATUS = "status";

    public static final String SUCCESS = "success";
    public static final String FAILED = "failed";

    public static final String SIGN_UP = "sign_up";
    public static final String SIGN_IN = "sign_in";

    public static final String SEND_TYPE = "send_type";

    public static final String DTMF_SEQ = "dtmf_seq";
    public static final String RATE = "rate";
    public static final String FEEDBACK_TEXT = "description";

    public static final String S_TIME = "s_time";
    public static final String STATUS_CALLBACK_MESSAGE = "status";
    public static final String SP_SIP_INFO = "sip_info";
    public static final String CALLBACK_TOKEN = "callback_token";
    public static final String ERROR = "error";
    public static final String IS_CONNECT_ALIVE = "is_connect_alive";

    public static final String VALIDATION_NUMBER = "sms_code_valid";
    public static final String VALIDATION_NUMBER_FROM_SIGNUP = "validation_number_from_sign_up";

    public static final String FILE_PATH = "path";

    public static final int TYPE_ERROR = 0xffffffff;
    public static final int TYPE_PHONE_NUMBER_NOT_CONFIRMED = 0xfffffffe;
    public static final int TYPE_CALLSTATUS_CHANGED = 0x01;
    public static final int TYPE_MENU_LOADED = 0x02;
    public static final int TYPE_FEEDBACK = 0x03;
    public static final int TYPE_SUCCESS = 0x04;
    public static final int TYPE_CONFIRMATION_CODE_RECEIVED = 0x05;
    public static final int TYPE_CONFIRMATION_CODE_VALID = 0x06;

    public static final String NO_RESPONSE = "No response from server";
    public static final String STATUS_CALLBACK_CANCELLED = "Callback has been successfully canceled.";
    public static final String SEND_CALLBACK_REQ_TIME = "send_callback_req_time";
    public static final String STATUS_SENDING_REQUEST = "sending request";
    public static final String STATUS_FAILURE = "failure";
    public static final String CALLBACK_CANCELLED = "callback_cancelled";
    public static final String CALLBACK_WAIT_TIME = "approx_wait_time";
    public static final String LAST_UPDATE_IVR_LIST = "last_update_ivr_list";

    public static final int CHANNEL_STATE_OPENED = 1;
    public static final int CHANNEL_STATE_CLOSED = 0;

    public final static String TYPE_AGENT = "AGENT";
    public final static String TYPE_INPUT = "INPUT";

    public final static String LIST_TYPE_LIMITED = "LIMITED";
    public final static String LIST_TYPE_SUMMARY = "SUMMARY";
}
