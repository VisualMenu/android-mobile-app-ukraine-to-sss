package com.visumenu.clientmenu.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.visumenu.clientmenu.Util;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.User;
import com.visumenu.clientmenu.request.API;

import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 20.04.16.
 */
public class RecentLikeService extends IntentService{

    final String TAG = "RecentLikeService";

    public RecentLikeService(String name) {
        super(name);
    }

    public RecentLikeService(){
        this("RecentLikeService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int requestCode = intent.getIntExtra(FetchDataConstants.REQUEST_CODE, -1);
        switch (requestCode){

            case FetchDataConstants.CODE_LIKE:
                boolean addLike = intent.getBooleanExtra(FetchDataConstants.ADD_LIKE, false);
                String dtmf = intent.getStringExtra(FetchDataConstants.DTMF_SEQ);
                String stime = intent.getStringExtra(FetchDataConstants.S_TIME);
                proceedLike(intent.getStringExtra(FetchDataConstants.IVR_NUMBER), dtmf, stime, addLike);
                break;
        }
    }

    void proceedLike(String ivrNumber, String dtmfSeq, String stime, boolean addLike){
        API api = Util.getMainAPI(getApplicationContext());

        User user = Database.getUser(Realm.getInstance(
                new RealmConfiguration.Builder(getApplicationContext()).build()
        ));

        if(api == null || user == null || user.getAccessToken() == null)
            return;

        Call<ResponseBody> call;

        if(addLike)
            call = api.postLike(user.getAccessToken(), ivrNumber, dtmfSeq, stime);
        else
            call = api.deleteLike(user.getAccessToken(), ivrNumber, dtmfSeq, stime);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if(response.isSuccess())
                        Log.d(TAG, "response is " + response.body().string());
                    else
                        Log.d(TAG, "response is " + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "error ", t);
            }
        });
    }

}
