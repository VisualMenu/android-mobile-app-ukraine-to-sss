package com.visumenu.clientmenu.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.visumenu.clientmenu.BuildConfig;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Util;
import com.visumenu.clientmenu.Utils.CallbackStatus;
import com.visumenu.clientmenu.Utils.DtmfUtils;
import com.visumenu.clientmenu.Utils.ResponseCallback;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.User;
import com.visumenu.clientmenu.request.API;
import com.visumenu.clientmenu.request.CallStatusDataResponse;
import com.visumenu.clientmenu.request.CallStatusResponse;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 14.04.16.
 */
public class CallbackSendService extends IntentService {
    final String TAG = "CallbackSendService";

    TimerTask timerTask;
    private static Timer mTimer;

    private static final int REPEAT_TIME = 5 * 1000;
    private static final int START_SCHEDULE_TIME = 5 * 1000;

    public CallbackSendService(){
        this("CallbackSendService");
    }

    public CallbackSendService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int requestCode = intent.getIntExtra(FetchDataConstants.REQUEST_CODE, -1);
        Log.d(TAG, "request code " + requestCode);
        switch (requestCode){

            case FetchDataConstants.CODE_REQUEST_CALLBACK:
                sendCallback(intent);
                break;

            case FetchDataConstants.CODE_CHANGED_CALLBACK_STATUS:
                checkStatus();
                break;

            case FetchDataConstants.CODE_CANCEL_CALLBACK:
                cancelCallback();
                break;
        }
    }

    void sendCallback(Intent intent){
        API api = Util.getMainAPI(getApplicationContext());
        if(api == null)
            return;

        Realm realm = Realm.getInstance(
                new RealmConfiguration.Builder(getApplicationContext()).build()
        );
        User user = Database.getUser(realm);

        if(user == null || user.getAccessToken() == null)
            return;

        String stime = intent.getStringExtra(FetchDataConstants.S_TIME);
        String ivrNumber =  intent.getStringExtra(FetchDataConstants.IVR_NUMBER);
        String dtmf = intent.getStringExtra(FetchDataConstants.DTMF_SEQ);
        Call<ResponseBody> call = api.callBack(
                user.getAccessToken(),
                ivrNumber,
                dtmf,
                stime,
                intent.getStringExtra(FetchDataConstants.SIP_NUMBER)
        );

        int stimeValue = getLastStime(stime);

        if(stimeValue == 0)
            stimeValue = 60;

        Util.writeDtmf(getApplicationContext(), dtmf);
        Util.writeIvrNumber(getApplicationContext(), ivrNumber);
        Util.writeSTime(getApplicationContext(), stimeValue);

        Log.d(TAG, "url is " + call.request().url());
        Log.d(TAG, "request body" + call.request().toString());
        CallbackStatus callbackStatus = new CallbackStatus();
        callbackStatus.setSendFeedback(false);
        callbackStatus.setShowEstTime(false);
        callbackStatus.setChannelState(FetchDataConstants.CHANNEL_STATE_OPENED);
        sendNotification(FetchDataConstants.STATUS_SENDING_REQUEST,
                callbackStatus);
        call.enqueue(new ResponseCallback() {
            @Override
            public void onFailure(Throwable t) {
                Log.d(TAG, "onFailure " + t.toString());
                CallbackStatus callbackStatus = new CallbackStatus();
                callbackStatus.setSendFeedback(false);
                callbackStatus.setShowEstTime(false);
                callbackStatus.setChannelState(FetchDataConstants.CHANNEL_STATE_CLOSED);
                sendNotification(FetchDataConstants.STATUS_FAILURE,
                        callbackStatus);
            }

            @Override
            public void onSuccess(ResponseBody responseBody){

                JsonParser parser = new JsonParser();
                try {
                    JsonElement jsonObject = parser.parse(responseBody.string());
                    String callbackToken =
                            jsonObject.getAsJsonObject().get("callback").getAsString();
                    int stime = jsonObject.getAsJsonObject()
                                    .get(FetchDataConstants.CALLBACK_WAIT_TIME)
                                    .getAsInt();
                    Log.d(TAG, "callback token " + callbackToken);
                    Util.writeCallbackToken(getApplicationContext(), callbackToken);
                    Util.writeSipPreference(getApplicationContext(),
                            FetchDataConstants.SEND_CALLBACK_REQ_TIME,
                            System.currentTimeMillis());
                    Util.writeSTime(getApplicationContext(), stime);
                    cancelTimer();
                    scheduleRequestCallbackStatus(START_SCHEDULE_TIME, REPEAT_TIME);
                    checkStatus();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(ResponseBody responseBody) {
                CallbackStatus callbackStatus = new CallbackStatus();
                callbackStatus.setSendFeedback(false);
                callbackStatus.setShowEstTime(false);
                callbackStatus.setChannelState(FetchDataConstants.CHANNEL_STATE_CLOSED);
                sendNotification(FetchDataConstants.STATUS_FAILURE,
                        callbackStatus);
                Util.sendErrorEvent(getApplicationContext(), responseBody);
                try{
                    Log.d(TAG, "onError " + responseBody.string());
                }catch (IOException e){
                    Log.e(TAG, "responsebody ", e);
                }
            }
        });
    }

    private int getLastStime(String stime){
        if(stime.equals(""))
            return  0;

        if(stime.contains(DtmfUtils.STIME_SEPARATOR)){
            String[] stimes = stime.split("\\" + DtmfUtils.STIME_SEPARATOR);
            if(stimes.length > 0)
                return Integer.parseInt(stimes[stimes.length - 1]);
        }

        return Integer.parseInt(stime);
    }

    void checkStatus(){
        Log.d(TAG, "checkStatus");
        SharedPreferences sp = getSharedPreferences(FetchDataConstants.SP_SIP_INFO, MODE_PRIVATE);
        String callbackToken = sp.getString(FetchDataConstants.CALLBACK_TOKEN, "");

        if(callbackToken.equals(""))
            return;

        Realm realm = Realm.getInstance(
                new RealmConfiguration.Builder(getApplicationContext()).build()
        );
        User user = Database.getUser(realm);

        if(user == null || user.getAccessToken() == null)
            return;

        API api = Util.getMainAPI(getApplicationContext());
        if(api == null)
            return;

        Call<CallStatusResponse> call = api.getCallbackStatus(getString(R.string.url_callback_status),
                user.getAccessToken(),
                callbackToken);

        Log.d(TAG, "checkStatus url is " + call.request().url().toString());

        call.enqueue(new Callback<CallStatusResponse>() {
            @Override
            public void onResponse(Call<CallStatusResponse> call, Response<CallStatusResponse> response) {
                if(response.isSuccess()){
                    CallStatusResponse callStatusResponseBody = response.body();
                    String callStatus = callStatusResponseBody.getMessage();
                    int channelState = callStatusResponseBody.getData().getChannelState();
                    CallbackStatus callbackStatus = new CallbackStatus();
                    callbackStatus.setChannelState(channelState);
                    if(channelState == FetchDataConstants.CHANNEL_STATE_CLOSED){
                        cancelTimer();
                        Util.writeConnectionIsAlive(getApplicationContext(), false);
                        callbackStatus.setSendFeedback(true);
                    } else {
                        callbackStatus.setSendFeedback(false);
                        Util.writeConnectionIsAlive(getApplicationContext(), true);
                    }

                    callbackStatus.setShowEstTime(true);
                    Log.d(TAG, "status " + callStatus + " state " + channelState +
                            " datettime created "
                            + callStatusResponseBody.getData().getIvrCreatedDatetime());
                    sendNotification(callStatus, callbackStatus);
                }else{
                    Log.d(TAG, "response failed " + response.body());
                }
            }

            @Override
            public void onFailure(Call<CallStatusResponse> call, Throwable t) {
                Log.d(TAG, "response general failed ", t);
            }
        });
    }

    void cancelCallback(){
        Log.d(TAG, "cancelCallback");
        SharedPreferences sp = getSharedPreferences(FetchDataConstants.SP_SIP_INFO, MODE_PRIVATE);
        String callbackToken = sp.getString(FetchDataConstants.CALLBACK_TOKEN, "");

        if(callbackToken.equals(""))
            return;

        Realm realm = Realm.getInstance(
                new RealmConfiguration.Builder(getApplicationContext()).build()
        );
        User user = Database.getUser(realm);
        if(user == null || user.getAccessToken() == null)
            return;

        API api = Util.getMainAPI(getApplicationContext());
        if(api == null)
            return;

        Call<CallStatusResponse> call = api.cancelCallbackRequest(
                user.getAccessToken(),
                callbackToken);

        Log.d(TAG, "cancelCallback url is " + call.request().url());

        call.enqueue(new Callback<CallStatusResponse>() {
            @Override
            public void onResponse(Call<CallStatusResponse> call, Response<CallStatusResponse> response) {
                if(response.isSuccess()){
                    CallStatusResponse responseBody = response.body();
                    CallStatusDataResponse data = responseBody.getData();
                    int channelState;
                    String callStatus = responseBody.getMessage();
                    if(data == null)
                        channelState = callStatus.equals(FetchDataConstants.STATUS_CALLBACK_CANCELLED) ?
                                FetchDataConstants.CHANNEL_STATE_CLOSED :
                                FetchDataConstants.CHANNEL_STATE_OPENED;
                    else
                        channelState = data.getChannelState();
                    Log.d(TAG, "status " + callStatus + " state " + channelState);
                    CallbackStatus callbackStatus = new CallbackStatus();
                    callbackStatus.setChannelState(channelState);
                    if(channelState == FetchDataConstants.CHANNEL_STATE_CLOSED) {
                        Util.writeConnectionIsAlive(getApplicationContext(), false);
                        cancelTimer();
                    }

                    callbackStatus.setSendFeedback(false);
                    callbackStatus.setShowEstTime(false);
                    sendNotification(callStatus, callbackStatus);
                }else{
                    Log.d(TAG, "response failed " + response.body());
                }
            }

            @Override
            public void onFailure(Call<CallStatusResponse> call, Throwable t) {
                Log.d(TAG, "response general failed ", t);
            }
        });
    }

    void cancelTimer(){
        if(mTimer != null){
            mTimer.cancel();
            mTimer = null;
        }
    }

    void scheduleRequestCallbackStatus(long schedule, long repeating){
        initTimerTask();
        mTimer = new Timer();
        mTimer.schedule(timerTask, schedule, repeating);
    }

    void initTimerTask(){
        timerTask = new TimerTask() {

            @Override
            public void run() {
                checkStatus();
            }
        };
    }

    void sendNotification(String message, CallbackStatus state){
        if(BuildConfig.DEBUG && false)
            Log.d(TAG, "status updated to " + message);
        else{
            EventMessage eventMessage =
                    new EventMessage(FetchDataConstants.TYPE_CALLSTATUS_CHANGED, message);
            int stime = Util.getStime(getApplicationContext());
            long reqTime = Util.getRequestCallbackTime(getApplicationContext());

            if(stime == 0 || reqTime == 0){
                Log.e(TAG, "stime " + stime + " reqtime " + reqTime);
                return;
            }

            eventMessage.setData(state);
            EventBus.getDefault().post(eventMessage);
        }
    }
}
