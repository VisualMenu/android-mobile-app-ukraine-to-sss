package com.visumenu.clientmenu.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.sip.SipAudioCall;
import android.net.sip.SipProfile;
import android.util.Log;

import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.Util;

/**
 * Created by user on 11.04.16.
 */
public class IncomingSIPCallReceiver extends BroadcastReceiver {
    /**
     * Processes the incoming call, answers it, and hands it over to the
     * MainActivity
     * @param context The context under which the receiver is running.
     * @param intent The intent being received.
     */
    final String TAG = "IncomingSIPCallReceiver";
    SipAudioCall.Listener listener;
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive");
        SipAudioCall incomingCall = null;
//        Util.cancelRequestCallbackStatus(context);
        Util.cancelNotification(context);
        try {
            listener = new SipAudioCall.Listener() {
                @Override
                public void onRinging(SipAudioCall call, SipProfile caller) {
                    try {
                        Log.d(TAG, "onRinging");
                        call.answerCall(30);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            IvrActivity wtActivity = (IvrActivity) context;
            incomingCall = wtActivity.mSipManager.takeAudioCall(intent, listener);
            wtActivity.call = incomingCall;
            wtActivity.updateStatus("incomingCall");
            wtActivity.onSipCall();
        } catch (Exception e) {
            Log.e(TAG, "incoming call error", e);
            if (incomingCall != null) {
                incomingCall.close();
            }
        }
    }
}
