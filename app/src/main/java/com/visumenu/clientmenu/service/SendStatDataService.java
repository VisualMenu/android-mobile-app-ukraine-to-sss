package com.visumenu.clientmenu.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.visumenu.clientmenu.Util;
import com.visumenu.clientmenu.Utils.MenuStatItemSerializer;
import com.visumenu.clientmenu.Utils.ResponseCallback;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.MenuStatItem;
import com.visumenu.clientmenu.model.User;
import com.visumenu.clientmenu.request.API;

import java.io.File;
import java.io.IOException;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 22.03.16.
 */
public class SendStatDataService extends IntentService {

    final String TAG = "SendStatDataService";
    private static final String EVENT_TYPE = "event_type";
    private static final String EVENT_TYPE_TAP = "tap";
    private static final String DATA = "data";

    public SendStatDataService(){
        super("SendStatDataService");
        Log.d(TAG, "service started");
    }

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public SendStatDataService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "onHandleIntent");
        int type = intent.getIntExtra(FetchDataConstants.SEND_TYPE, -1);
        switch (type){
            case FetchDataConstants.CODE_SEND_STAT:
                sendStat();
                break;

            case FetchDataConstants.CODE_SEND_FEEDBACK:
                sendFeedback(intent);
                break;

            case FetchDataConstants.CODE_SEND_FILE:
                sendRecord(intent);
                break;
        }
    }

    void sendStat(){
        Gson gson = null;

        try {
            gson = new GsonBuilder()
                    .setExclusionStrategies(new ExclusionStrategy() {
                        @Override
                        public boolean shouldSkipField(FieldAttributes f) {
                            return f.getDeclaringClass().equals(RealmObject.class);
                        }

                        @Override
                        public boolean shouldSkipClass(Class<?> clazz) {
                            return false;
                        }
                    })
                    .registerTypeAdapter(Class.forName("io.realm.MenuStatItemRealmProxy"),
                            new MenuStatItemSerializer())

                    .create();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            Log.e(TAG, "error serialize", e);
            return;
        }

        Realm realm = Realm.getInstance(
                new RealmConfiguration.Builder(getApplicationContext()).build()
        );
        List<MenuStatItem> unsentItems =
                Database.getUnSentStatInfo(realm);

        Log.d(TAG, "unsent size " + unsentItems.size());

        if(unsentItems.size() == 0)
            return;

        JsonElement jsonUnsentItems  = gson.toJsonTree(unsentItems);
        String json = gson.toJson(unsentItems);

        JsonObject jsonObject = new JsonObject();

        jsonObject.addProperty(EVENT_TYPE, EVENT_TYPE_TAP);
        jsonObject.add(DATA, jsonUnsentItems);
        JsonArray jsonArray = new JsonArray();
        jsonArray.add(jsonObject);

        Log.d(TAG, "final json is " + jsonObject.toString());
        Log.d(TAG, "json data " + jsonArray.toString());
        Log.d(TAG, "data for sent is " + json);
        API api = Util.getMainAPI(getApplicationContext());
        if(api == null)
            return;

        User user = Database.getUser(realm);
        if(user == null){
            return;
        }

        Call<ResponseBody> call = api.track(user.getAccessToken(), jsonArray.toString());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    ResponseBody responseBody;
                    if(response.isSuccess()){
                        responseBody = response.body();
                        String responseString = responseBody == null ? null : responseBody.string();
                        Log.d(TAG, "register response " +
                                responseString);

                    }else{
                        responseBody = response.errorBody();
                        Log.d(TAG, "register response error " +
                                (responseBody == null ? null : responseBody.string()));
                        Util.sendErrorEvent(getApplicationContext(), responseBody);
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "register: http code " + response.code());
                Log.d(TAG, "register: message " + response.message());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Util.sendMessageEvent(FetchDataConstants.TYPE_ERROR, FetchDataConstants.NO_RESPONSE);
            }
        });
    }

    void sendFeedback(Intent intent){
        Log.d(TAG, "sendFeedback");

        Realm.setDefaultConfiguration(new RealmConfiguration.Builder(getApplicationContext()).build());
        User user = Database.getUser(
                Realm.getInstance(new RealmConfiguration.Builder(getApplicationContext()).build())
        );
        if(user == null){
            return;
        }

        String accessToken = user.getAccessToken();

        String ivr = intent.getStringExtra(FetchDataConstants.IVR_NUMBER);
        String dtmfSeq = intent.getStringExtra(FetchDataConstants.DTMF_SEQ);
        String rate = intent.getStringExtra(FetchDataConstants.RATE);
        String description = intent.getStringExtra(FetchDataConstants.FEEDBACK_TEXT);

        API api = Util.getMainAPI(getApplicationContext());

        if(api == null)
            return;

        Call<ResponseBody> call = api.feedback(accessToken, ivr, dtmfSeq, rate, description);

        Log.d(TAG, "sendFeedback url is " + call.request().url());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    ResponseBody responseBody;
                    if(response.isSuccess()){
                        responseBody = response.body();
                        String responseString = responseBody == null ? null : responseBody.string();
                        Log.d(TAG, "register response " +
                                responseString);

                    }else{
                        responseBody = response.errorBody();
                        Log.d(TAG, "register response error " +
                                (responseBody == null ? null : responseBody.string()));
                        Util.sendErrorEvent(getApplicationContext(), responseBody);
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "register: http code " + response.code());
                Log.d(TAG, "register: message " + response.message());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Util.sendMessageEvent(FetchDataConstants.TYPE_ERROR, FetchDataConstants.NO_RESPONSE);
            }
        });
    }

    void sendRecord(Intent intent){
        Log.d(TAG, "sendRecord");
        Realm.setDefaultConfiguration(new RealmConfiguration.Builder(getApplicationContext()).build());
        User user = Database.getUser(
                Realm.getInstance(new RealmConfiguration.Builder(getApplicationContext()).build())
        );
        if(user == null){
            return;
        }

        String accessToken = user.getAccessToken();
        String ivrNumber = intent.getStringExtra(FetchDataConstants.IVR_NUMBER);
        String phoneNumber = intent.getStringExtra(FetchDataConstants.PHONE_NUMBER);
        String path = intent.getStringExtra(FetchDataConstants.FILE_PATH);

        API api = Util.getMainAPI(getApplicationContext());
        if(api == null)
            return;

        if (phoneNumber == null)
            return;

        Log.d(TAG, "sending file " + path);

        File record = new File(path);
        Log .d(TAG, "file " + (record.exists() ? "exist" : "not exist"));
        MediaType plainText = MediaType.parse("text/plain");
        RequestBody fileBody = RequestBody.create(MediaType.parse("audio/*"), record);
        RequestBody accessTokenBody = RequestBody.create(plainText, accessToken);
        RequestBody ivrNumberBody = RequestBody.create(plainText, ivrNumber);
        RequestBody phoneNumberBody = RequestBody.create(plainText, phoneNumber);

        try {
            Log.d(TAG, "filebody length " + fileBody.contentLength());
        } catch (IOException e) {
            e.printStackTrace();
        }

        Call<ResponseBody> call = api.sendRecordFile(
                accessTokenBody,
                ivrNumberBody,
                phoneNumberBody,
                fileBody
                );

        call.enqueue(new ResponseCallback() {
            @Override
            public void onFailure(Throwable t) {
                Util.sendMessageEvent(FetchDataConstants.TYPE_ERROR, FetchDataConstants.NO_RESPONSE);
                Log.e(TAG, "failure ", t);
            }

            @Override
            public void onSuccess(ResponseBody responseBody) throws IOException {
                Log.d(TAG, "success " + responseBody.string());
            }

            @Override
            public void onError(ResponseBody responseBody) throws IOException {
                Log.e(TAG, "error sending file " +
                        (responseBody == null ? "null" : responseBody.string()));
//                Log.d(TAG, "error " + responseBody.string());
//                Util.sendErrorEvent(responseBody);
            }
        });
    }
}
