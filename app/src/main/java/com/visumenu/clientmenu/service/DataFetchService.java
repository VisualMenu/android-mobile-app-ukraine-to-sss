package com.visumenu.clientmenu.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Util;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.IVR;
import com.visumenu.clientmenu.model.User;
import com.visumenu.clientmenu.request.API;
import com.visumenu.clientmenu.request.IVRMenuResponse;
import com.visumenu.clientmenu.request.IVRsResponse;
import com.visumenu.clientmenu.request.IVRsResponseCallback;
import com.visumenu.clientmenu.request.ResponseRecent;

import io.realm.RealmObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 10.03.16.
 */

public class DataFetchService extends Service{

    final String TAG = "DataFetchService";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent == null)
            return super.onStartCommand(null, flags, startId);

        int code = intent.getIntExtra(FetchDataConstants.REQUEST_CODE, -1);

        Log.d(TAG, "code is " + code);
        switch (code){

            case FetchDataConstants.CODE_IVRS_VALUE:
                fetchIVRs(null);
                break;

            case FetchDataConstants.CODE_IVRS_SEARCH:
                String searchString = intent.getStringExtra(FetchDataConstants.SEARCH_STRING);
                fetchIVRs(searchString);
                break;

            case FetchDataConstants.CODE_MENU_VALUE:
                String ivrNumber = intent.getStringExtra(FetchDataConstants.IVR_NUMBER);
                fetchMenu(ivrNumber);
                break;

            case FetchDataConstants.CODE_FETCH_DETMF_TOP:
                fetchMenuTop(intent.getStringExtra(FetchDataConstants.IVR_NUMBER));
                break;

            case FetchDataConstants.CODE_RECENT_VALUE:
                fetchRecent();
                break;
        }

        return super.onStartCommand(intent, flags, startId);
    }


    public void fetchIVRs(final String searchString){
        Log.d(TAG, "fetchIVRs");

        User user = Database.getInstance(getApplicationContext()).getUser();

        if(user == null || user.getAccessToken() == null)
            return;

        API api = getApi();

        if (api == null)
            return;

        Log.d(TAG, "accesstoken is " + user.getAccessToken());
        Call<IVRsResponse> call;

        boolean isSearch = searchString != null && !searchString.equals("");

        if(isSearch){
            Database.getInstance(getApplicationContext()).removeSearchResults();
            call = api.getIVRs(
                    getString(R.string.url_companies),
                    user.getAccessToken(),
                    FetchDataConstants.LIST_TYPE_SUMMARY,
                    searchString);
        } else
            call = api.getIVRs(
                    getString(R.string.url_companies),
                    user.getAccessToken(),
                    FetchDataConstants.LIST_TYPE_LIMITED
            );

        Log.d(TAG, "url is " + call.request().url());

        call.enqueue(new IVRsResponseCallback(getApplicationContext(), isSearch));
    }

    private void fetchRecent() {
        User user = Database.getInstance(getApplicationContext()).getUser();

        if(user == null || user.getAccessToken() == null)
            return;

        API api = getApi();

        if(api == null)
            return;

        Log.d(TAG, "accesstoken is " + user.getAccessToken());

        Call<ResponseRecent> call =
                api.getRecent(getString(R.string.url_recent), user.getAccessToken());

        Log.d(TAG, "recent url is " + call.request().url());
        call.enqueue(new Callback<ResponseRecent>() {
            @Override
            public void onResponse(Call<ResponseRecent> call, Response<ResponseRecent> response) {
                ResponseRecent body = response.body();
                if(body == null){
                    Log.e(TAG, "response body null");
                    Util.sendErrorEvent(getApplicationContext(), response.errorBody());
                    return;
                }
                Log.d(TAG, "response OK");
                Log.d(TAG, "list size " + body.getRecent().getRecentItemList().size());
                Database.getInstance(getApplicationContext())
                        .addRecentItems(body.getRecent().getRecentItemList());
            }

            @Override
            public void onFailure(Call<ResponseRecent> call, Throwable t) {
                Util.sendMessageEvent(FetchDataConstants.TYPE_ERROR, FetchDataConstants.NO_RESPONSE);
                Log.d(TAG, "recent get failure", t);
            }
        });

    }

    public void fetchMenu(final String ivrNumber){
        Database database = Database.getInstance(getApplicationContext(), false);
        IVR ivr = database.findIVRbyNumber(ivrNumber);
        String ivrVersion = database.getIVRVersion(ivrNumber);
        Log.d(TAG, "ivr version locally " + ivrVersion + " ivr version " + ivr.getIvrVersion());
        Log.d(TAG, "ivr menu items count " + ivr.getMenuItems().size() +
                " isEmpty " + ivr.getMenuItems().isEmpty());
        if(ivr.getIvrVersion().equals(ivrVersion)
                && ivr.getMenuItems().isEmpty())
            return;

        User user = Database.getInstance(getApplicationContext()).getUser();

        if(user == null || user.getAccessToken() == null)
            return;

        API api = getApi();

        if (api == null)
            return;

        Log.d(TAG, "accesstoken is " + user.getAccessToken());
        Call<IVRMenuResponse> call = api.getMenu(getString(R.string.url_ivr_menu),
                user.getAccessToken(),
                ivrNumber);

        Log.d(TAG, "url is " + call.request().url());

        call.enqueue(new Callback<IVRMenuResponse>() {

            @Override
            public void onResponse(Call<IVRMenuResponse> call, Response<IVRMenuResponse> response) {

                IVRMenuResponse ivrMenuResponse = response.body();
                if (ivrMenuResponse == null) {
                    Util.sendErrorEvent(getApplicationContext(), response.errorBody());
                    Log.e(TAG, "response null");
                    return;
                }

                Database database = Database.getInstance(getApplicationContext(), false);
                database.updateIVRMenu(ivrNumber, ivrMenuResponse.getMenuBody().getMenus());
                database.setIVRVersion(ivrNumber, ivrMenuResponse.getMenuBody().getIvrVersion());
                Log.d(TAG, "response " + ivrMenuResponse.toString());
            }

            @Override
            public void onFailure(Call<IVRMenuResponse> call, Throwable t) {
                Log.e(TAG, "error fetching IVRs");
                t.printStackTrace();
                Util.sendMessageEvent(FetchDataConstants.TYPE_ERROR, FetchDataConstants.NO_RESPONSE);
            }
        });
    }

    public void fetchMenuTop(final String ivrNumber){
        User user = Database.getInstance(getApplicationContext()).getUser();

        if(user == null || user.getAccessToken() == null)
            return;

        API api = getApi();

        if (api == null)
            return;

        Call<IVRMenuResponse> call = api.getDtmfTop(user.getAccessToken(),
                ivrNumber);

        Log.d(TAG, "url is " + call.request().url());

        call.enqueue(new Callback<IVRMenuResponse>() {

            @Override
            public void onResponse(Call<IVRMenuResponse> call, Response<IVRMenuResponse> response) {

                IVRMenuResponse ivrMenuResponse = response.body();
                if (ivrMenuResponse == null) {
                    Log.e(TAG, "response null");
                    Util.sendErrorEvent(getApplicationContext(), response.errorBody());
                    return;
                }

                Database database = Database.getInstance(getApplicationContext(), false);
                database.updateIVRMenu(ivrNumber, ivrMenuResponse.getMenuBody().getMenus());
                database.setIVRVersion(ivrNumber, ivrMenuResponse.getMenuBody().getIvrVersion());
                Log.d(TAG, "response " + ivrMenuResponse.toString());
            }

            @Override
            public void onFailure(Call<IVRMenuResponse> call, Throwable t) {
                Log.e(TAG, "error fetching IVRs");
                t.printStackTrace();
                Util.sendMessageEvent(FetchDataConstants.TYPE_ERROR, FetchDataConstants.NO_RESPONSE);
            }
        });
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
    }

    private API getApi(){

        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.main_api_hostname))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(API.class);
    }

}
