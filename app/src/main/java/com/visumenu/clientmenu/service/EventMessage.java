package com.visumenu.clientmenu.service;

/**
 * Created by user on 05.05.16.
 */
public class EventMessage {

    int type;
    String message;
    Object data;

    public EventMessage(int type, String message) {
        this.type = type;
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
