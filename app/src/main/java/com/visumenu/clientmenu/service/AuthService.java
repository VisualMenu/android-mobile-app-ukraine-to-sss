package com.visumenu.clientmenu.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Util;
import com.visumenu.clientmenu.Utils.ResponseCallback;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.User;
import com.visumenu.clientmenu.request.API;
import com.visumenu.clientmenu.request.UserRegisterResponse;

import java.io.IOException;

import io.realm.RealmObject;
import okhttp3.ResponseBody;
import retrofit2.Call;

/**
 * Service for
 *
 */
public class AuthService extends Service {

    final String TAG = "AuthService";

    private static final String REGISTER_TYPE_CREDENTIALS = "credentials";
    private static final String REGISTER_TYPE_SOCIAL = "social_network";

    User user;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if(intent == null)
            return super.onStartCommand(null, flags, startId);

        int requestCode = intent.getIntExtra(FetchDataConstants.REQUEST_CODE, 0);
        switch (requestCode){

            case FetchDataConstants.CODE_SIGN_UP:
                register(intent);
                break;

            case FetchDataConstants.CODE_SIGN_IN:
                login(intent);
                break;

            case FetchDataConstants.CODE_UPDATE_PROFILE:
                updateProfile(intent);
                break;

            case FetchDataConstants.CODE_FORGOT_PASSWORD:
                forgotPwd(intent);
                break;

            case FetchDataConstants.CODE_RESET_PASSWORD:
                resetPwd(intent);
                break;

            case FetchDataConstants.CODE_REQUEST_SMS:
                requestVerificationCode();
                break;

            case FetchDataConstants.CODE_VERIFY_SMS_CODE:
                verifySMSCode(intent);
                break;
        }
        return START_STICKY;
    }

    private void register(Intent intent){

        API api = Util.getMainAPI(getApplicationContext());

        if(api == null)
            return;

        Call<ResponseBody> call;
        user = new User();
        Database.getInstance(getApplicationContext()).removeUserInfo();
        String email = intent.getStringExtra(FetchDataConstants.EMAIL);
        user.setLogin(email);

        String firstName = intent.getStringExtra(FetchDataConstants.FIRST_NAME);
        String lastName = intent.getStringExtra(FetchDataConstants.LAST_NAME);
        String phoneNumber = intent.getStringExtra(FetchDataConstants.PHONE_NUMBER);
        user.setPhoneNumber(phoneNumber);

        if(intent.hasExtra(FetchDataConstants.PASSWORD)){
            String password = intent.getStringExtra(FetchDataConstants.PASSWORD);
            Log.d(TAG, "register by credential");
            call = api.register(REGISTER_TYPE_CREDENTIALS,
                    email,
                    phoneNumber,
                    password,
                    firstName,
                    lastName
            );

            user.setPassword(password);
            user.setEmail(email);

        }else{
//            user.setAccessToken(intent.getStringExtra(FetchDataConstants.ACCESS_TOKEN));
            Log.d(TAG, "register by social networks");
            call = api.registerViaSocialNetwork(REGISTER_TYPE_SOCIAL,
                    intent.getStringExtra(FetchDataConstants.SOCIAL_NETWORK),
                    intent.getStringExtra(FetchDataConstants.USER_ID_SOCIAL_NETWORK),
                    phoneNumber,
                    firstName,
                    lastName);
        }

        Log.d(TAG, "url " + call.request().url());
        Log.d(TAG, "body " + call.request().body());
        call.enqueue(new ResponseCallback() {
            @Override
            public void onFailure(Throwable t) {

            }

            @Override
            public void onSuccess(ResponseBody responseBody) {
                try{
                    String responseString = responseBody == null ? null : responseBody.string();
                    Log.d(TAG, "register response " +
                            responseString);

                    if(responseBody != null)
                        parseRegisterBody(responseString, user);
                }catch (IOException e){
                    Log.e(TAG, "error register", e);
                }
            }

            @Override
            public void onError(ResponseBody responseBody) {
                parseAuthError(responseBody);
            }
        });
    }

    private void login(Intent intent){
        API api = Util.getMainAPI(getApplicationContext());

        if(api == null)
            return;

        String email = intent.getStringExtra(FetchDataConstants.EMAIL);
        String password = intent.getStringExtra(FetchDataConstants.PASSWORD);

        user = new User();

        user.setLogin(email);
        Call<ResponseBody> call =
                api.login(REGISTER_TYPE_CREDENTIALS,
                        intent.getStringExtra(FetchDataConstants.EMAIL),
                        intent.getStringExtra(FetchDataConstants.PASSWORD)
                );

        user.setPassword(password);
        user.setEmail(email);

        Log.d(TAG, "url " + call.request().url());
        Log.d(TAG, "body " + call.request().body());

        call.enqueue(new ResponseCallback() {
            @Override
            public void onFailure(Throwable t) {

            }

            @Override
            public void onSuccess(ResponseBody responseBody) {
                try {
                    parseRegisterBody(responseBody.string(), user);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Database.getInstance(AuthService.this).updateUser(user);
                Intent authIntent = new Intent(FetchDataConstants.AUTH_ACTION);
                authIntent.putExtra(FetchDataConstants.FULL_USER_INFO, true);
                authIntent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_SIGN_UP);
                authIntent.putExtra(FetchDataConstants.STATUS, FetchDataConstants.CODE_SUCCESS);
                sendBroadcast(authIntent);
            }

            @Override
            public void onError(ResponseBody responseBody) {
                parseAuthError(responseBody);
            }
        });
    }

    private void updateProfile(Intent intent){

        API api = Util.getMainAPI(getApplicationContext());

        if(api == null)
            return;

        user = new User();

        String accessToken = intent.getStringExtra(FetchDataConstants.ACCESS_TOKEN);
        String userName = intent.getStringExtra(FetchDataConstants.USERNAME);
        String password = intent.getStringExtra(FetchDataConstants.PASSWORD);
        String phoneNumber = intent.getStringExtra(FetchDataConstants.PHONE_NUMBER);
        String gender = intent.getStringExtra(FetchDataConstants.GENDER);
        String firstName = intent.getStringExtra(FetchDataConstants.FIRST_NAME);
        String lastName = intent.getStringExtra(FetchDataConstants.LAST_NAME);
        String address = intent.getStringExtra(FetchDataConstants.ADDRESS);
        String email = intent.getStringExtra(FetchDataConstants.EMAIL);
        Log.d(TAG, "access token " + accessToken);

        user.setAccessToken(accessToken);
        user.setLogin(userName);
        user.setPhoneNumber(phoneNumber);
        user.setPassword(password);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmail(email);
        user.setAddress(address);
        user.setGender(gender);

//        Call<ResponseBody> call = api.updateProfile(user);
        Call<ResponseBody> call =
                api.updateProfile(
                       accessToken,
                        userName,
                        password,
                        phoneNumber,
                        firstName,
                        lastName,
                        address,
                        email,
                        gender);

        Log.d(TAG, "url " + call.request().url());
        Log.d(TAG, "body " + call.request().body());
        call.enqueue(new ResponseCallback() {
            @Override
            public void onFailure(Throwable t) {
//                Database.getInstance(AuthService.this).updateUser(user);
//                Intent authIntent = new Intent(FetchDataConstants.AUTH_ACTION);
//                authIntent.putExtra(FetchDataConstants.FULL_USER_INFO, true);
//                authIntent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_SIGN_UP);
//                authIntent.putExtra(FetchDataConstants.STATUS, FetchDataConstants.CODE_SUCCESS);
//                sendBroadcast(authIntent);
            }

            @Override
            public void onSuccess(ResponseBody responseBody) {
                Database.getInstance(AuthService.this).updateUser(user);
                Intent authIntent = new Intent(FetchDataConstants.AUTH_ACTION);
                authIntent.putExtra(FetchDataConstants.FULL_USER_INFO, true);
                authIntent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_SIGN_UP);
                authIntent.putExtra(FetchDataConstants.STATUS, FetchDataConstants.CODE_SUCCESS);
                sendBroadcast(authIntent);
            }

            @Override
            public void onError(ResponseBody responseBody) {
              parseAuthError(responseBody);
            }
        });

    }

    private User parseRegisterBody(String responseString, User user) throws IOException {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .create();

        Log.d(TAG, "parseRegisterBody: body string " + responseString);
        UserRegisterResponse response =
                gson.fromJson(responseString, UserRegisterResponse.class);

        user.setAccessToken(response.getToken());
        user.setCognitoToken(response.getCognitoToken());

        if(response.getProfile() != null) {
            String phoneNumber = response.getProfile().getPhoneNumber();
            user.setPhoneNumber(phoneNumber);
            Log.d(TAG, "phoneNumber from profile");
        }

        Integer phoneNumberConfirmed = response.getPhoneNumberIsValid();

        int status = phoneNumberConfirmed == null ?
                FetchDataConstants.CODE_SUCCESS :
                FetchDataConstants.CODE_AUTH_SUCCESS_WITHOUT_CONFIRM;

        Intent authIntent = new Intent(FetchDataConstants.AUTH_ACTION);
        authIntent.putExtra(FetchDataConstants.FULL_USER_INFO,
                response.getRequiredFields() == null || response.getRequiredFields().size() == 0);
        authIntent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_SIGN_UP);
        authIntent.putExtra(FetchDataConstants.STATUS, status);
        Database.getInstance(this).updateUser(user);
        sendBroadcast(authIntent);
        return user;
    }

    void forgotPwd(Intent intent){
        Log.d(TAG, "forgotPwd");
        String email = intent.getStringExtra(FetchDataConstants.EMAIL);
        if(email == null || email.equals(""))
            return;

        API api = Util.getMainAPI(getApplicationContext());
        if(api == null)
            return;

        Log.d(TAG, "forgotPwd email " + email);

        Call<ResponseBody> call = api.forgotPassword(email);
        call.enqueue(new ResponseCallback() {
            @Override
            public void onFailure(Throwable t) {

            }

            @Override
            public void onSuccess(ResponseBody responseBody) {

            }

            @Override
            public void onError(ResponseBody responseBody) {
                Util.sendErrorEvent(getApplicationContext(), responseBody);
            }
        });
    }

    void resetPwd(Intent intent){
        String code = intent.getStringExtra(FetchDataConstants.CODE_RESET_PWD);
        String deviceId = Util.getDeviceId(getApplicationContext());
        String password = intent.getStringExtra(FetchDataConstants.PASSWORD);
        if(code == null || deviceId == null || password == null){
            Log.e(TAG, "incorrect resetPwd data " + code + " " + deviceId + " " + password);
            return;
        }

        API api = Util.getMainAPI(getApplicationContext());

        if(api == null)
            return;

        Call<ResponseBody> call = api.resetPassword(code, deviceId, password);
        call.enqueue(new ResponseCallback() {
            @Override
            public void onFailure(Throwable t) {

            }

            @Override
            public void onSuccess(ResponseBody responseBody) {

            }

            @Override
            public void onError(ResponseBody responseBody) {
                Util.sendErrorEvent(getApplicationContext(), responseBody);
            }
        });
    }

    private void verifySMSCode(Intent intent) {
        User user =  Database.getInstance(getApplicationContext()).getUser();
        API api = Util.getMainAPI(getApplicationContext());

        final boolean fromSignUp =
                intent.getBooleanExtra(FetchDataConstants.VALIDATION_NUMBER_FROM_SIGNUP, false);

        if (api == null)
            return;

        String code = intent.getStringExtra(FetchDataConstants.VERIFICATION_CODE);
        if (code == null)
            return;

        Call<ResponseBody> call =
                api.verifySMSCode(code, user.getPhoneNumber(), user.getAccessToken());
        call.enqueue(new ResponseCallback() {
            @Override
            public void onFailure(Throwable t) {

            }

            @Override
            public void onSuccess(ResponseBody responseBody) {

                if (fromSignUp) {
                    Intent authIntent = new Intent(FetchDataConstants.AUTH_ACTION);
                    authIntent.putExtra(FetchDataConstants.FULL_USER_INFO, true);
                    authIntent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_SIGN_UP);
                    authIntent.putExtra(FetchDataConstants.STATUS, FetchDataConstants.CODE_SUCCESS);
                    sendBroadcast(authIntent);
                } else {
                    try {
                        Util.sendMessageEvent(
                                FetchDataConstants.TYPE_CONFIRMATION_CODE_VALID,
                                responseBody.string()
                        );
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onError(ResponseBody responseBody) {
                Util.sendErrorEvent(getApplicationContext(), responseBody);
            }
        });
    }

    private void requestVerificationCode() {
        User user =  Database.getInstance(getApplicationContext()).getUser();
        API api = Util.getMainAPI(getApplicationContext());

        if (api == null)
            return;

        Call<ResponseBody> call = api.requestSMSCode(user.getPhoneNumber(), user.getAccessToken());
        call.enqueue(new ResponseCallback() {
            @Override
            public void onFailure(Throwable t) {

            }

            @Override
            public void onSuccess(ResponseBody responseBody) {

            }

            @Override
            public void onError(ResponseBody responseBody) {
                Util.sendErrorEvent(getApplicationContext(), responseBody);
            }
        });


    }

    private void parseAuthError(ResponseBody responseBody){
        String message = null;
        try {
            String json = responseBody.string();
            Log.d(TAG, "error " + json);
            if (json == null)
                message = getString(R.string.unknown_error);
            else {
                JsonObject errorJson = new JsonParser().parse(json).getAsJsonObject();
                JsonElement jsonElement;
                if (errorJson == null ||
                        (jsonElement = errorJson.get(FetchDataConstants.ERROR)) == null)
                    message = getString(R.string.unknown_error);
                else
                    message = jsonElement.getAsString();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Send broadcast error message " + message);
        Database.getInstance(AuthService.this).updateUser(user);
        Intent authIntent = new Intent(FetchDataConstants.AUTH_ACTION);
        authIntent.putExtra(FetchDataConstants.FULL_USER_INFO, true);
        authIntent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_SIGN_UP);
        authIntent.putExtra(FetchDataConstants.STATUS, FetchDataConstants.CODE_FAILED);
        authIntent.putExtra(FetchDataConstants.ERROR_MESSAGE, message);
        sendBroadcast(authIntent);
    }
}
