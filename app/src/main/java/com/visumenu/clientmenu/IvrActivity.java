package com.visumenu.clientmenu;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.net.sip.SipAudioCall;
import android.net.sip.SipErrorCode;
import android.net.sip.SipException;
import android.net.sip.SipManager;
import android.net.sip.SipProfile;
import android.net.sip.SipRegistrationListener;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.visumenu.clientmenu.Utils.CallUtils;
import com.visumenu.clientmenu.Utils.CallbackStatus;
import com.visumenu.clientmenu.Utils.DtmfUtils;
import com.visumenu.clientmenu.Utils.Favorites;
import com.visumenu.clientmenu.fragment.CompaniesListFragment;
import com.visumenu.clientmenu.fragment.CompaniesListFragment_;
import com.visumenu.clientmenu.fragment.ConfirmationPhoneNumberFragment;
import com.visumenu.clientmenu.fragment.ConfirmationPhoneNumberFragment_;
import com.visumenu.clientmenu.fragment.LoginFragment_;
import com.visumenu.clientmenu.fragment.MenuListFragment_;
import com.visumenu.clientmenu.fragment.RegisterFragment_;
import com.visumenu.clientmenu.fragment.UpdateProfileFragment_;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.RecentDtmf;
import com.visumenu.clientmenu.model.User;
import com.visumenu.clientmenu.service.AuthBroadcastReceiver;
import com.visumenu.clientmenu.service.AuthService;
import com.visumenu.clientmenu.service.CallbackSendService;
import com.visumenu.clientmenu.service.ConfirmCodeReceiver;
import com.visumenu.clientmenu.service.DataFetchService;
import com.visumenu.clientmenu.service.EventMessage;
import com.visumenu.clientmenu.service.FetchDataConstants;
import com.visumenu.clientmenu.service.IncomingSIPCallReceiver;
import com.visumenu.clientmenu.service.RecentLikeService;
import com.visumenu.clientmenu.service.SendStatDataService;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.text.ParseException;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import io.fabric.sdk.android.Fabric;

public class IvrActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "PazbV89dfBMVqoOof1G0735DB";
    private static final String TWITTER_SECRET = "mh2unzC3VJht5eMbVBPSV2ItejXjWzStefKQ7VGGNUH9Xe27zc";

    public static final String OPEN_DIALER = "open_dialer";
    public static final String AUTH_BY_SOCIAL_NETWORK = "auth_with_social_network";

    private static final int REQUEST_CODE_DIALER = 0xFF;
    private static final int REQUEST_PERMISSION_CALL = 100;
    public SipManager mSipManager = null;
    public SipProfile mSipProfile = null;
    public SipAudioCall call;

    MediaPlayer player;
    Toolbar toolbar;
    ActionBarDrawerToggle toggle;
    final String TAG = "MainActivity";
    Fragment currentFragment;
    AuthBroadcastReceiver authReceiver;
    ConfirmCodeReceiver smsCodeReceiver;
    IncomingSIPCallReceiver callReceiver;
    MaterialDialog progressDialog;
    ProgressBar callProgressBar;
    MaterialDialog needPhoneConfirmationDialog;
    MaterialDialog confirmationDialog;
    TextView tvTime;
    TimerTask timerTask;
    RelativeLayout rlCallProgressLayout;
    AppCompatTextView tvCallbackStatus;
    ImageView btnCancelCallback;
//    ImageView ivDialer;
    SearchView searchBar;
    Uri phoneUri;
    private static Timer mTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        if(!BuildConfig.DEBUG){
            Fabric.with(this, new Crashlytics(), new Twitter(authConfig));
            logUser();
        } else
            Fabric.with(this, new Twitter(authConfig));
        Favorites.setFavorites(Database.getInstance(this).getFavorites());

//        Util.addTestContact(this);

        FacebookSdk.sdkInitialize(getApplicationContext());
        progressDialog = new MaterialDialog.Builder(this)
                .title(R.string.please_wait)
                .content(R.string.please_wait)
                .progress(true, 0)
                .build();

        if (Favorites.getFavorites().size() == 0)
            Database.getInstance(getApplicationContext()).fetchLikesNumbers();
//        Util.requestCallbackStatus(getApplicationContext());
        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        searchBar = (SearchView) findViewById(R.id.searchBar);
        callProgressBar = (ProgressBar) findViewById(R.id.callProgressBar);
        btnCancelCallback = (ImageView) findViewById(R.id.ivCancelCallback);
        tvTime = (TextView) findViewById(R.id.tvTime);
        rlCallProgressLayout = (RelativeLayout) findViewById(R.id.progressLayout);
//        ivDialer = (ImageView) toolbar.findViewById(R.id.ivDialer);
//        ivDialer.setOnClickListener(this);
        if (rlCallProgressLayout != null) {
            rlCallProgressLayout.setVisibility(View.GONE);
        }

//        searchBar.setOnSearchClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ivDialer.setVisibility(View.GONE);
//            }
//        });

        tvCallbackStatus = (AppCompatTextView) findViewById(R.id.tvCallbackStatus);
        callProgressBar.setVisibility(View.GONE);
        btnCancelCallback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelCallback();
            }
        });

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        enableDrawer(false);
        drawer.setDrawerListener(toggle);
        (navigationView.getHeaderView(0).findViewById(R.id.ivNavIcon))
                .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.closeDrawer(GravityCompat.START);
            }
        });

        toggle.syncState();
        if(false)//sip functionality now is not required
            initSip();
        //TODO move add receiver to method, which called reg or auth fragment
        addAuthReceiver();
        if (savedInstanceState != null)
            return;

        String ivrNumber =
                getIntent().getStringExtra(FetchDataConstants.IVR_NUMBER);

        boolean openDialer = getIntent().getBooleanExtra(OPEN_DIALER, true);
        if (ivrNumber == null){
            User user = Database.getInstance(this).getUser();
            if (user == null || user.getAccessToken() == null){
                openLogin();
                return;
            }

            if(openDialer)
                openDialer();
            else
                switchToCompanies(false);

        }else
            openMenu(ivrNumber, true);

    }

    public void showProgressDialog(String title, String content) {
        if(progressDialog == null)
            return;
        progressDialog.setTitle(title);
        progressDialog.setContent(content);
        progressDialog.show();
    }

    public void showWaitDialog(){
        showProgressDialog(getString(R.string.please_wait), getString(R.string.please_wait));
    }

    public void dismissProgressDialog() {
        progressDialog.dismiss();
    }

    public void initSip() {
        if (mSipManager == null)
            mSipManager = SipManager.newInstance(this);
        Log.d(TAG, "SIP: " +
                "\napi supprted " + SipManager.isApiSupported(this) +
                "\nsip wifi only " + SipManager.isSipWifiOnly(this) +
                "\nvoip supported " + SipManager.isVoipSupported(this));
        if (mSipManager == null) {
            Toast.makeText(this, "Sip not supported", Toast.LENGTH_LONG).show();
            return;
        }
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.SipDemo.INCOMING_CALL");
        callReceiver = new IncomingSIPCallReceiver();
        this.registerReceiver(callReceiver, filter);
        SipProfile.Builder builder = null;
        try {
            builder = new SipProfile.Builder("1002", "52.11.136.252");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        builder.setPassword("1002");
        builder.setAuthUserName("1002");
//        builder.setOutboundProxy("52.11.136.252");
//        builder.setAuthUserName("1002");
        mSipProfile = builder.build();
        registerSip();
    }

    SipRegistrationListener registrationListener = new SipRegistrationListener() {

        public void onRegistering(String localProfileUri) {
            updateStatus("Registering with SIP Server...");
        }

        public void onRegistrationDone(String localProfileUri, long expiryTime) {
            updateStatus("Ready");
//            try {
//                mSipManager.register(mSipProfile, 30, registrationListener);
//            } catch (SipException e) {
//                e.printStackTrace();
//            }
        }

        public void onRegistrationFailed(String localProfileUri, int errorCode,
                                         String errorMessage) {
            if (errorCode == SipErrorCode.DATA_CONNECTION_LOST)
                registerSip();
            else if (errorCode != SipErrorCode.IN_PROGRESS)
                Toast.makeText(IvrActivity.this,
                        "sip error: " + errorMessage,
                        Toast.LENGTH_LONG)
                        .show();
            updateStatus("Registration failed.  Please check settings.");
            Log.d(TAG, "error message " + errorMessage + " error code " + errorCode);
        }
    };

    public void updateStatus(String status) {
        Log.d(TAG, "sip status: " + status);
    }

    public void logoCenter() {
        if (toolbar == null)
            return;

        ImageView ivLogo = (ImageView) toolbar.findViewById(R.id.ivLogo);
//        toolbar.findViewById(R.id.ivNavImage).setVisibility(View.GONE);

        ivLogo.setImageResource(R.drawable.ic_logo);
        ivLogo.setScaleType(ImageView.ScaleType.FIT_CENTER);
        RelativeLayout.LayoutParams layoutParams =
                new RelativeLayout.LayoutParams(ivLogo.getLayoutParams());
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
        ivLogo.setLayoutParams(layoutParams);
    }

    public void enableDrawer(boolean enable) {
        toggle.setDrawerIndicatorEnabled(enable);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getFragmentManager().getBackStackEntryCount() <= 1)
                finish();
            else{
                if(currentFragment instanceof  MenuListFragment_)
                    setDialerAndSearchVisibility(View.VISIBLE);
                getFragmentManager().popBackStack();

                View fragmentContainer = findViewById(R.id.fragment_container);
                if (fragmentContainer == null)
                    return;

                fragmentContainer.post(new Runnable() {
                    @Override
                    public void run() {
                        currentFragment = getFragmentManager().findFragmentById(R.id.fragment_container);
                        Log.d(TAG, "currentfragment " + currentFragment.getClass().getName());
                    }
                });

            }
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currentFragment = getFragmentManager().findFragmentById(R.id.fragment_container);
    }

    @Override
    protected void onResume() {
        if (mTimer != null && !Util.getConnectAlive(getApplicationContext())) {
           cancelCheckCallbackStatus();
           showFeedBackDialog();
        }

        super.onResume();

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
//        new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... params) {
//                Util.writeSipPreference(getApplicationContext(),
//                        FetchDataConstants.S_TIME,
//                        50);
//
//                Util.writeSipPreference(getApplicationContext(),
//                        FetchDataConstants.SEND_CALLBACK_REQ_TIME,
//                        System.currentTimeMillis());
//
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        rlCallProgressLayout.setVisibility(View.VISIBLE);
//                        callProgressBar.setVisibility(View.VISIBLE);
//                        scheduleTimer(500, 500);
//                    }
//                });
//
//                Log.d(TAG, "phoneUri number is " + CallUtils.getPhoneNumber(getApplicationContext()));
//                return null;
//            }
//        }.execute();
    }

    private void fetchData() {
        progressDialog.show();
        Intent intent = new Intent(IvrActivity.this, DataFetchService.class);
        intent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_IVRS_VALUE);
        startService(intent);
    }

    public void fetchRecent() {
        Intent intent = new Intent(IvrActivity.this, DataFetchService.class);
        intent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_RECENT_VALUE);
        startService(intent);
    }

    public void fetchMenuData(String ivrNumber) {
        Util.fetchMenuData(getApplicationContext(), ivrNumber);
    }

    void switchToCompanies(boolean clearBackStack) {
        setDialerAndSearchVisibility(View.VISIBLE);
        if(clearBackStack)
            clearBackStack();
        fetchData();
        currentFragment = new CompaniesListFragment_();
        changeFragment();
        detachAuthReceiver();
    }

    void openUpdateProfile() {
        currentFragment = new UpdateProfileFragment_();
        changeFragment();
    }

    void initNeedConfimDialog(){
        needPhoneConfirmationDialog = new MaterialDialog
                .Builder(this)
                .title(R.string.phone_number_confirmation)
                .content(R.string.need_number_confirmation)
                .positiveText(R.string.confirm)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        requestConfirmationCode();
                        initConfirmDialog();
                        confirmationDialog.show();
                        dialog.dismiss();
                    }
                })
                .negativeText(R.string.cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
    }

    void initConfirmDialog() {
        confirmationDialog = new MaterialDialog
                .Builder(this)
                .title(R.string.phone_number_confirmation)
                .customView(R.layout.confirm_code_dialog, false)
                .positiveText(R.string.confirm)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        EditText etConfirmCode =
                                (AppCompatEditText) (dialog.findViewById(R.id.etConfirmCode));
                        sendConfirmationCode(etConfirmCode.getText().toString(), false);
                    }
                })
                .neutralText(R.string.resend_code)
                .onNeutral(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        requestConfirmationCode();
                    }
                })
                .negativeText(R.string.cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .autoDismiss(false)
                .build();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            logout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    void logout(){
        Database.getInstance(this).clearAllData();
        clearBackStack();
        Intent intent = new Intent(this, IvrActivity.class);
        Util.setLastUpdateIVRs(getApplicationContext(), 0);
        intent.setFlags(
                Intent.FLAG_ACTIVITY_CLEAR_TOP  |
                Intent.FLAG_ACTIVITY_CLEAR_TASK |
                Intent.FLAG_ACTIVITY_NEW_TASK
        );

        startActivity(intent);
    }

    public void openMenu(String ivrNumber, boolean addToBackStack) {
        Log.d(TAG, "open menu for ivr number " + ivrNumber);
        Database.getInstance(getApplicationContext()).SetRecent(ivrNumber);
        if(!searchBar.isIconified())
            searchBar.setIconified(true);
        searchBar.setVisibility(View.GONE);
//        ivDialer.setVisibility(View.VISIBLE);
        currentFragment = new MenuListFragment_();
        Bundle bundle = new Bundle();
        bundle.putString(FetchDataConstants.IVR_NUMBER, ivrNumber);
        currentFragment.setArguments(bundle);
        changeFragment(addToBackStack);
    }

    void changeFragment() {
        changeFragment(true);
    }


    void changeFragment(boolean addToBackStack) {
        if (currentFragment instanceof LoginFragment_
                || currentFragment instanceof RegisterFragment_
                || currentFragment instanceof MenuListFragment_)
            searchBar.setVisibility(View.GONE);
        else
            searchBar.setVisibility(View.VISIBLE);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container, currentFragment)
                .addToBackStack(currentFragment.getClass().getName())
                .commit();
    }

    public void openRegister() {
        openRegister(null);
    }

    public void openRegister(Bundle args) {
        setDialerAndSearchVisibility(View.GONE);
        if (authReceiver == null)
            addAuthReceiver();
        currentFragment = new RegisterFragment_();
        if (args != null)
            currentFragment.setArguments(args);
        changeFragment();
    }

    public void openLogin() {
        setDialerAndSearchVisibility(View.GONE);
        if (authReceiver == null)
            addAuthReceiver();
        currentFragment = new LoginFragment_();
        changeFragment();
    }

    void setDialerAndSearchVisibility(int visiblity){
//        ivDialer.setVisibility(visiblity);
        searchBar.setVisibility(visiblity);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE_DIALER && resultCode == RESULT_OK){
            String servicePhoneNumber = data.getStringExtra(FetchDataConstants.PHONE_NUMBER);
            if(Database.getInstance(getApplicationContext()).hasIvrNumber(servicePhoneNumber)){
                boolean addToBackStack = currentFragment instanceof MenuListFragment_;
                openMenu(servicePhoneNumber, addToBackStack);
            }

            else{
                String userPhoneNumber = Util.getUserPhoneNumber(getApplicationContext());
                callBack(servicePhoneNumber, "", "", userPhoneNumber, FetchDataConstants.TYPE_AGENT);
            }
            return;
        }

        if (currentFragment != null) {
            currentFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    void addAuthReceiver() {
        IntentFilter filter =
                new IntentFilter(FetchDataConstants.AUTH_ACTION);
        if (authReceiver == null)
            authReceiver = new AuthBroadcastReceiver() {
                @Override
                public void onSuccess(boolean fullUserData, boolean phoneNumberConfirmed) {
                    Log.d(TAG, "auth: onSuccess");
                    dismissProgressDialog();

                    if (phoneNumberConfirmed) {
                        openDialer();
                    } else {
                        openConfirmationFragment();
                    }

//                    switchToCompanies(true);
//                    if(fullUserData)
//                        switchToCompanies();
//                    else
//                        openUpdateProfile();
                }

                @Override
                public void onFailure(String errorMessage) {
                    Log.d(TAG, "auth receiver onFailure " + errorMessage);
                    dismissProgressDialog();
                    showAlertDialog(errorMessage, null);
                }
            };
        registerReceiver(authReceiver, filter);
    }

    private void clearBackStack(){
        FragmentManager fm = getFragmentManager();
        for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
    }

    public void showAlertDialog(String message, MaterialDialog.SingleButtonCallback callback) {
        showDialog(getString(R.string.error), message, null, callback);
    }

    public void showDialog(String title, String message, View view,
                           MaterialDialog.SingleButtonCallback positiveCallback){
        MaterialDialog.Builder builder = new MaterialDialog.Builder(IvrActivity.this)
                .title(title)
                .onPositive( positiveCallback == null ?
                        new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                } : positiveCallback)
                .positiveText(android.R.string.ok);

        if(view == null)
            builder.content(message);
        else
            builder.customView(view, true);

        builder.show();
    }

    void openConfirmationFragment() {
        setDialerAndSearchVisibility(View.GONE);
        if (authReceiver == null)
            addAuthReceiver();

        if(smsCodeReceiver == null) {
            smsCodeReceiver = new ConfirmCodeReceiver();
            IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
            intentFilter.setPriority(999);
            registerReceiver(smsCodeReceiver, intentFilter);
        }

        currentFragment = new ConfirmationPhoneNumberFragment_();
        changeFragment();
    }

    void detachAuthReceiver() {
        if (authReceiver != null) {
            unregisterReceiver(authReceiver);
            authReceiver = null;
        }

        if(smsCodeReceiver != null) {
            unregisterReceiver(smsCodeReceiver);
            smsCodeReceiver = null;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus eventBus = EventBus.getDefault();
        eventBus.register(this);
    }

    @Override
    protected void onStop() {
        closeLocalProfile();
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        detachAuthReceiver();
        if (callReceiver != null)
            unregisterReceiver(callReceiver);
        super.onDestroy();
    }

    @Subscribe
    public void onEvent(final EventMessage message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (message.getType()){

                    case FetchDataConstants.TYPE_ERROR:
                        proceedError(message.getMessage(), null);
                        break;

                    case FetchDataConstants.TYPE_CALLSTATUS_CHANGED:
                        proceedChangingCallStatus(message);
                        break;

                    case FetchDataConstants.TYPE_FEEDBACK:
                        showFeedBackDialog();
                        break;

                    case FetchDataConstants.TYPE_SUCCESS:
                        Log.d(TAG, "fetch data success");
                        dismissProgressDialog();
                        if (currentFragment instanceof CompaniesListFragment){
                            Log.d(
                                    TAG,
                                    "currentFragment is CompaniesListFragment_, " +
                                            "call realmDataWasChanged"
                            );
                            ((CompaniesListFragment) currentFragment).realmDataWasChanged();
                        }

                        break;

                    case FetchDataConstants.TYPE_PHONE_NUMBER_NOT_CONFIRMED:
                        Log.d(TAG, "phone number not confirmed");
                        if ((confirmationDialog != null && confirmationDialog.isShowing()) ||
                                currentFragment instanceof ConfirmationPhoneNumberFragment) {
                            showAlertDialog(message.getMessage(), null);
                        } else {
                            if (needPhoneConfirmationDialog == null)
                                initNeedConfimDialog();
                            needPhoneConfirmationDialog.show();
                        }

                        break;

                    case FetchDataConstants.TYPE_CONFIRMATION_CODE_RECEIVED:
                        Log.d(TAG, "confirmation code received " + message.getMessage());
                        if (currentFragment instanceof ConfirmationPhoneNumberFragment) {
                            ((ConfirmationPhoneNumberFragment) currentFragment)
                                    .setConfirmationCode(message.getMessage());
                        } else if (confirmationDialog != null) {
                            AppCompatEditText etConfirmCode =
                                    (AppCompatEditText) confirmationDialog.findViewById(R.id.etConfirmCode);
                            etConfirmCode.setText(message.getMessage());
                        }

                        break;

                    case FetchDataConstants.TYPE_CONFIRMATION_CODE_VALID:
                        showDialog(
                                getString(R.string.phone_number_confirmation),
                                getString(R.string.phone_number_confirmed),
                                null,
                                null
                        );
                        if (confirmationDialog != null && confirmationDialog.isShowing()) {
                            confirmationDialog.dismiss();
                        }
                        break;
                }
            }
        });
    }

    public void requestConfirmationCode() {
        Intent intent = new Intent(this, AuthService.class);
        intent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_REQUEST_SMS);
        startService(intent);
    }

    public void sendConfirmationCode(String code, boolean fromSignUp) {
        Intent intent = new Intent(this, AuthService.class);
        intent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_VERIFY_SMS_CODE);
        intent.putExtra(FetchDataConstants.VERIFICATION_CODE, code);
        intent.putExtra(FetchDataConstants.VALIDATION_NUMBER_FROM_SIGNUP, fromSignUp);
        startService(intent);
    }

    void proceedError(String errorMessage, MaterialDialog.SingleButtonCallback callback){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
        showAlertDialog(errorMessage, callback);
    }

    void proceedChangingCallStatus(EventMessage message){
//        showProgressDialog(getString(R.string.call_status), getString(R.string.status_is) + status);
        Log.d(TAG, "status changed " + message);
        String msg = message.getMessage();
        CallbackStatus callbackStatus = (CallbackStatus) message.getData();
        int channelState = callbackStatus.getChannelState();
        if(channelState == FetchDataConstants.CHANNEL_STATE_CLOSED
                || FetchDataConstants.STATUS_FAILURE.equals(msg)) {
            cancelCheckCallbackStatus();
            if(callbackStatus.isSendFeedback())
                showFeedBackDialog();
            return;
        }

        if(mTimer == null || timerTask == null)
            scheduleTimer(500, 500);

        tvCallbackStatus.setText(message.getMessage());
        callProgressBar.setVisibility(View.VISIBLE);
        rlCallProgressLayout.setVisibility(View.VISIBLE);
        tvTime.setVisibility(callbackStatus.isShowEstTime() ? View.VISIBLE : View.INVISIBLE);
    }

    void cancelCheckCallbackStatus() {
        callProgressBar.setVisibility(View.GONE);
        rlCallProgressLayout.setVisibility(View.GONE);
        cancelCallbackTimer();
    }

    public void closeLocalProfile() {
        if (mSipManager == null)
            return;

        try {
            if (mSipProfile != null)
                mSipManager.close(mSipProfile.getUriString());
        } catch (Exception ee) {
            Log.d(TAG, "Failed to close local profile.", ee);
        }
    }

    public void callBack(String ivNumber, String dtmf, String sTime, String sipNumber, String subType) {
        Log.d(TAG, "callback with dtmf " + dtmf + " , stime " + sTime);
        if(isCallbackInProgress()){
            showAlertDialog(getString(R.string.callback_already_requested), null);
            return;
        }

        if(FetchDataConstants.TYPE_INPUT.equals(subType)) {
            performDialWithCheckPermission(ivNumber, dtmf, sTime);
            return;
        }

        try {
            if (mSipManager != null && !mSipManager.isRegistered(mSipProfile.getUriString())) {
                initSip();
                Toast.makeText(this, "Sip not initialized.", Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (SipException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(IvrActivity.this, CallbackSendService.class)
                .putExtra(FetchDataConstants.REQUEST_CODE,
                        FetchDataConstants.CODE_REQUEST_CALLBACK)
                .putExtra(FetchDataConstants.IVR_NUMBER, ivNumber)
                .putExtra(FetchDataConstants.DTMF_SEQ, dtmf)
                .putExtra(FetchDataConstants.SIP_NUMBER, sipNumber)
                .putExtra(FetchDataConstants.S_TIME, sTime);
        startService(intent);
    }

    public void sendLike(String ivrNumber, String dtmf, String stime, boolean addLike) {
        Log.d(TAG, "sendLike add like " + addLike);
        Intent intent = new Intent(this, RecentLikeService.class)
                .putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_LIKE)
                .putExtra(FetchDataConstants.IVR_NUMBER, ivrNumber)
                .putExtra(FetchDataConstants.DTMF_SEQ, dtmf)
                .putExtra(FetchDataConstants.S_TIME, stime)
                .putExtra(FetchDataConstants.ADD_LIKE, addLike);
        startService(intent);
    }

    public void sendLike(RecentDtmf recentDtmf, boolean addLike) {
        Log.d(TAG, "sendLike add like " + addLike);
        Intent intent = new Intent(this, RecentLikeService.class)
                .putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_LIKE)
                .putExtra(FetchDataConstants.IVR_NUMBER, recentDtmf.getIVRNumber())
                .putExtra(FetchDataConstants.DTMF_SEQ, recentDtmf.getDtmf())
                .putExtra(FetchDataConstants.S_TIME, recentDtmf.getStime())
                .putExtra(FetchDataConstants.ADD_LIKE, addLike);
        startService(intent);
    }

    private void openDialer() {
        finish();
        Intent intent = new Intent(this, DialerActivity_.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
//        currentFragment = new DialerFragment_();
//        changeFragment();
    }

    void answerSipCall(SipAudioCall incomingCall) throws SipException {
        incomingCall.answerCall(30);
        incomingCall.startAudio();
        incomingCall.setSpeakerMode(true);
        if (incomingCall.isMuted()) {
            incomingCall.toggleMute();
        }
        showEndCallDialog();
    }

    void endCall(SipAudioCall incomingCall) throws SipException {
        incomingCall.endCall();
    }

    public void onSipCall() {
        player = CallUtils.playDefaultRingtone(this);
        showSipAnswerDialog();
    }

    private void stopSipRinging() {
        if (player != null && player.isPlaying())
            player.stop();
    }

    private void showSipAnswerDialog() {
        new MaterialDialog.Builder(this)
                .content(R.string.new_sip_call)
                .positiveText(R.string.accept)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        try {
                            answerSipCall(call);
                        } catch (SipException e) {
                            e.printStackTrace();
                        }
                        stopSipRinging();
                    }
                })
                .negativeText(R.string.decline)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        try {
                            endCall(call);
                        } catch (SipException e) {
                            e.printStackTrace();
                        }
                        stopSipRinging();
                    }
                })
                .canceledOnTouchOutside(false)
                .build()
                .show();
    }

    void registerSip() {
        Intent intent = new Intent();
        intent.setAction("android.SipDemo.INCOMING_CALL");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, Intent.FILL_IN_DATA);
        try {
            Log.d(TAG, "open sip manager");
            mSipManager.open(mSipProfile, pendingIntent, null);
            mSipManager.setRegistrationListener(mSipProfile.getUriString(), registrationListener);
        } catch (SipException e) {
            Log.e(TAG, "sip exception", e);
            e.printStackTrace();
        }
    }

    private void showEndCallDialog() {
        final TextView textView = new TextView(this);
        textView.setLayoutParams(
                new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        int padding = (int)getResources().getDimension(R.dimen.drawable_text_padding);
        textView.setPadding(0, padding, 0, padding);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            int count = 0;
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String time =
                                String.format(Locale.ENGLISH, "%02d:%02d", count / 60, count % 60);
                        textView.setText(time);
                        count++;
                    }
                });
            }
        }, 1000, 1000);
        textView.setTag(timer);
        new MaterialDialog.Builder(this)
                .customView(textView, false)
                .positiveText(R.string.end_call)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        ((Timer)textView.getTag()).cancel();
                        try {
                            endCall(call);

                        } catch (SipException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .canceledOnTouchOutside(false)
                .build()
                .show();
    }

    private void logUser() {
        Crashlytics.setUserIdentifier("andrew");
        Crashlytics.setUserEmail("kahtrus@gmail.com");
        Crashlytics.setUserName("Andrew");
    }

    void scheduleTimer(long schedule, long repeating){
        initTimerTask();
        mTimer = new Timer();
        mTimer.schedule(timerTask, schedule, repeating);
    }

    void cancelCallbackTimer(){
        if(mTimer != null){
            mTimer.cancel();
            mTimer = null;
        }
        timerTask = null;
    }

    void initTimerTask(){
        timerTask = new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        long reqTime = Util.getRequestCallbackTime(getApplicationContext());
                        int stime = Util.getStime(getApplicationContext());
                        callProgressBar.setVisibility(View.VISIBLE);

                        int percentage =
                                (int) (100 - (((stime * 1000) -
                                        (System.currentTimeMillis() - reqTime)) / (stime  * 1000.0)) * 100);
                        int estTimeSeconds = calculateEstTime(stime, reqTime);
                        int minutes = estTimeSeconds / 60;
                        int seconds = estTimeSeconds % 60;
                        if(minutes == 0 && seconds <= 0)
                            Util.writeSipPreference(getApplicationContext(),
                                    FetchDataConstants.SEND_CALLBACK_REQ_TIME,
                                    System.currentTimeMillis());
                        String estTimeString = String.format("%2d:%02d", minutes, seconds);
                        tvTime.setText(estTimeString);
                        Log.d(TAG, "progress " + percentage + " stime " +
                                stime * 1000 + " time left " +
                                (System.currentTimeMillis() - reqTime));
                        callProgressBar.setProgress(percentage);
                    }
                });
            }
        };
    }

    int calculateEstTime(int stime, long reqTime){
        int timeLeftSeconds =  (int) ((System.currentTimeMillis() - reqTime) / 1000);
        return stime - timeLeftSeconds;
    }

    public void cancelCallback(){
        cancelCallbackTimer();
        Intent intent = new Intent(this, CallbackSendService.class);
        intent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_CANCEL_CALLBACK);
        startService(intent);
    }

    public SearchView getSearchBar() {
        return searchBar;
    }

//    public void setIconDialerVisibility(int visibility){
//        ivDialer.setVisibility(visibility);
//    }

    public boolean isCallbackInProgress(){
        return rlCallProgressLayout.getVisibility() == View.VISIBLE;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ivDialer:
                Intent dialerIntent = new Intent(this, DialerActivity.class);
                startActivityForResult(dialerIntent, REQUEST_CODE_DIALER);
                break;

        }
    }

    void showFeedBackDialog(){
        new MaterialDialog.Builder(this)
                .customView(R.layout.feedback_dialog, true)
                .title(R.string.feedback)
                .positiveText(android.R.string.ok)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        EditText etFeedbak = (EditText) dialog.findViewById(R.id.etFeedback);
                        RatingBar rb = (RatingBar) dialog.findViewById(R.id.rbRate);
                        if(etFeedbak.length() == 0)
                            proceedError(getString(R.string.description_required),
                                    new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog,
                                                    @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    showFeedBackDialog();
                                }
                            });
                        else {
                            sendFeedback(String.valueOf(rb.getRating()), etFeedbak.getText().toString());
                            dialog.dismiss();
                        }

                    }
                })
                .negativeText(android.R.string.cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    void sendFeedback(String rate, String feedback){
        Intent sendFeedbackIntent = new Intent(this, SendStatDataService.class);

        sendFeedbackIntent
                .putExtra(FetchDataConstants.SEND_TYPE, FetchDataConstants.CODE_SEND_FEEDBACK)
                .putExtra(
                        FetchDataConstants.IVR_NUMBER,
                        Util.getCallbackIvrNumber(getApplicationContext())
                )
                .putExtra(
                        FetchDataConstants.DTMF_SEQ,
                        Util.getCallbackDtmf(getApplicationContext())
                )
                .putExtra(FetchDataConstants.RATE, rate)
                .putExtra(FetchDataConstants.FEEDBACK_TEXT, feedback);

        startService(sendFeedbackIntent);
    }

    public void sendSearchRequest(String query) {

        showProgressDialog(getString(R.string.please_wait), getString(R.string.please_wait));
        Intent intent = new Intent(this, DataFetchService.class)
                .putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_IVRS_SEARCH)
                .putExtra(FetchDataConstants.SEARCH_STRING, query);
        startService(intent);
    }

    public void performDialWithCheckPermission(String ivNumber, String dtmf, String sTime) {
        if (!ivNumber.equals("")) {
            String uri = "tel://" + DtmfUtils.generateTelUri(
                    BuildConfig.DEBUG ? "5433" : ivNumber, dtmf, sTime
            );
            Uri phoneInputUri = Uri.parse(uri);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CALL_PHONE},
                        REQUEST_PERMISSION_CALL);

                phoneUri = phoneInputUri;
                return;
            }
            startActivity(getIntentDial(phoneInputUri));
        }
    }

    private Intent getIntentDial(Uri phoneUri){

        return new Intent(Intent.ACTION_CALL, phoneUri);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CALL: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(getIntentDial(phoneUri));
                } else {
                    showAlertDialog("Call permission required", null);
                }
            }
            break;

        }
    }
}
