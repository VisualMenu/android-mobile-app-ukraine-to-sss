package com.visumenu.clientmenu.Utils;

/**
 * Created by user on 23.06.16.
 */

import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * EditText which suppresses IME show up.
 */
public class DigitsEditText extends EditText {

    final String TAG = "DigitsEditText";
    ClipboardManager clipboard = null;
    public DigitsEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setInputType(getInputType() | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setShowSoftInputOnFocus(false);
        }
    }
    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        final InputMethodManager imm = ((InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE));
        if (imm != null && imm.isActive(this)) {
            imm.hideSoftInputFromWindow(getApplicationWindowToken(), 0);
        }
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        final boolean ret = super.onTouchEvent(event);
        // Must be done after super.onTouchEvent()
        final InputMethodManager imm = ((InputMethodManager) getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE));
        if (imm != null && imm.isActive(this)) {
            imm.hideSoftInputFromWindow(getApplicationWindowToken(), 0);
        }
        return ret;
    }

    @Override
    public boolean onTextContextMenuItem(int id) {
        if (id != android.R.id.paste)
            return super.onTextContextMenuItem(id);

        String clipboardString = clipboard.getText().toString();
        Log.d(TAG, "clipboard string " + clipboardString);
        int selStart = getSelectionStart();
        int selEnd = getSelectionEnd();

        if (selStart != selEnd)
            getText().replace(selStart, selEnd, Uri.encode(clipboardString));
        else
            getText().insert(selStart, Uri.encode(clipboardString));
        return false;
    }
}
