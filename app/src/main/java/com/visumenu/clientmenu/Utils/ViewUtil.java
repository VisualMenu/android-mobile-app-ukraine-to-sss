package com.visumenu.clientmenu.Utils;

import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visumenu.clientmenu.R;

/**
 * Created by user on 07.04.16.
 */
public class ViewUtil {

    public static void addDividersTab(Context context, TabLayout tabLayout, int[] drawableResIds){

        if(tabLayout == null)
            return;

        Typeface font = Typeface.createFromAsset(context.getAssets(), "OpenSans-Regular.ttf");
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            LinearLayout linearLayout = (LinearLayout)
                    LayoutInflater.from(context).inflate(R.layout.tab_layout, tabLayout, false);

            if(tab == null)
                continue;

            TextView tabTextView = (TextView) linearLayout.findViewById(R.id.tab_title);
            if(i == 0)
                linearLayout.findViewById(R.id.tabDivider).setVisibility(View.INVISIBLE);
            tabTextView.setTypeface(font);
            tabTextView.setText(tab.getText());
            tab.setCustomView(linearLayout);
            tabTextView.setCompoundDrawablesWithIntrinsicBounds(
                    drawableResIds[i], 0, 0, 0);
//            tab.select();
        }
    }

    public static void openKeyboard(Context context, View view){
        InputMethodManager inputMethodManager =
                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null)
            inputMethodManager.showSoftInput(view, 0);
    }

    public static void closeKeyboard(Context context, View view){
        if (view != null) {
            InputMethodManager imm =
                    (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void changeTabsFont(TabLayout tabLayout, Typeface typeface, boolean setApperance, int apperance) {

        ViewGroup vg = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (!(tabViewChild instanceof TextView))
                    continue;

                TextView text = (TextView) tabViewChild;
                text.setTypeface(typeface, Typeface.NORMAL);
                if (setApperance)
                    text.setTextAppearance(text.getContext(), apperance);
            }
        }
    }

}
