package com.visumenu.clientmenu.Utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.AutoCompleteTextView;

/**
 * Created by user on 10.08.16.
 */
public class AutoCompleteTextViewNoEnter extends AutoCompleteTextView {
    public AutoCompleteTextViewNoEnter(Context context) {
        super(context);
    }

    public AutoCompleteTextViewNoEnter(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AutoCompleteTextViewNoEnter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_ENTER)
            return true;

        return super.onKeyDown(keyCode, event);
    }
}
