package com.visumenu.clientmenu.Utils;

import com.visumenu.clientmenu.model.IVRMenuItem;
import com.visumenu.clientmenu.model.ParentDtmfItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 09.06.16.
 */
public class DtmfUtils {

    public static String STIME_SEPARATOR = "|";
    public static int COME_DELAY_IN_SECONDS = 3;
    public static List<ParentDtmfItem> getParentDtmfItemNames(List<IVRMenuItem> list,
                                                              String dtmf, String stime) {
        List<ParentDtmfItem> parentList = new ArrayList<>();
        for (IVRMenuItem ivrMenuItem : list)
            if (dtmf.startsWith(ivrMenuItem.getDtmf())
                    && stime.startsWith(ivrMenuItem.getIvrMenuStime())
                    && !dtmf.equals(ivrMenuItem.getDtmf())){
                ParentDtmfItem parentDtmfItem = new ParentDtmfItem();
                parentDtmfItem.setItemName(ivrMenuItem.getName());
                parentDtmfItem.setLevel(ivrMenuItem.getDtmf().length());
                parentList.add(parentDtmfItem);
            }

        return parentList;
    }

    public static String generateTelUri(String number, String dtmfSeq, String stime) {
        if (dtmfSeq.length() == 0)
            return number;

        String dtmfUriPart;
        if (!stime.contains(STIME_SEPARATOR)) {
            dtmfUriPart = getComesFromStime(stime) + dtmfSeq;
        } else {
            String[] stimes = stime.split("\\" + STIME_SEPARATOR);
            StringBuilder sb = new StringBuilder();

            int actualIndex = 0;
            for (String stimeValue : stimes) {
                if (stimeValue.length() == 0 || stimeValue.equals(STIME_SEPARATOR))
                    continue;

                sb.append(getComesFromStime(stimeValue));
                sb.append(dtmfSeq.charAt(actualIndex));
                actualIndex++;
            }

            dtmfUriPart = sb.toString();
        }

        return number + dtmfUriPart;
    }

    private static String getComesFromStime(String stime){
        int timeInSecond = Integer.parseInt(stime);
        int comesCount = (int) Math.ceil(timeInSecond / (COME_DELAY_IN_SECONDS * 1.0));
        return getComes(comesCount);
    }

    private static String getComes(int count){
        return new String(new char[count]).replace("\0", ",");
    }
}
