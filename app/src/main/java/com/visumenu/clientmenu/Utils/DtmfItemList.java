package com.visumenu.clientmenu.Utils;

import com.visumenu.clientmenu.model.IVRMenuItem;

import java.util.ArrayList;

/**
 * Created by user on 21.04.16.
 */
public class DtmfItemList extends ArrayList<IVRMenuItem> {

    private IVRMenuItem parentItem;

    public IVRMenuItem getParentItem() {
        return parentItem;
    }

    public void setParentItem(IVRMenuItem parentItem) {
        this.parentItem = parentItem;
    }
}
