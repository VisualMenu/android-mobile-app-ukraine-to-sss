package com.visumenu.clientmenu.Utils;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by user on 19.04.16.
 */
public class Favorites {

    private static Set<String> favorites = new HashSet<>();

    private Favorites() {

    }

    public static Set<String> getFavorites() {
        return favorites;
    }


    public static void setFavorites(Set<String> favoritesSet){
        favorites = favoritesSet;
    }
}
