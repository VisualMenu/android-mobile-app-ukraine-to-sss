package com.visumenu.clientmenu.Utils;

import android.util.Log;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 05.04.16.
 */
public abstract class ResponseCallback implements Callback<ResponseBody> {

    final String TAG = "ResponseCallback";

    @Override
    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
        try {
            ResponseBody responseBody;
            if (response.isSuccess()) {
                responseBody = response.body();

                onSuccess(responseBody);
            }else{
                responseBody = response.errorBody();
                onError(responseBody);
            }

//            Log.d(TAG, "update profile response error " +
//                    (responseBody == null ? null : responseBody.string()));

        } catch (IOException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "update profile: http code " + response.code());
        Log.d(TAG, "update profile: message " + response.message());
    }

    @Override
    public void onFailure(Call<ResponseBody> call, Throwable t) {
        onFailure(t);
    }

    abstract public void onFailure(Throwable throwable);
    abstract public void onSuccess(ResponseBody responseBody) throws IOException;
    abstract public void onError(ResponseBody responseBody) throws IOException;

}
