package com.visumenu.clientmenu.Utils;

/**
 * Created by user on 10.06.16.
 */
public class CallbackStatus {

    int channelState;
    boolean sendFeedback;
    boolean showEstTime;

    public int getChannelState() {
        return channelState;
    }

    public void setChannelState(int channelState) {
        this.channelState = channelState;
    }

    public boolean isSendFeedback() {
        return sendFeedback;
    }

    public void setSendFeedback(boolean sendFeedback) {
        this.sendFeedback = sendFeedback;
    }

    public boolean isShowEstTime() {
        return showEstTime;
    }

    public void setShowEstTime(boolean showEstTime) {
        this.showEstTime = showEstTime;
    }
}
