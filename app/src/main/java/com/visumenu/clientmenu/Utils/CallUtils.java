package com.visumenu.clientmenu.Utils;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.provider.CallLog;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;

import java.util.Date;

/**
 * Created by user on 04.05.16.
 */
public class CallUtils {
    public static MediaPlayer playDefaultRingtone(Context context){
        MediaPlayer player = MediaPlayer.create(context,
                Settings.System.DEFAULT_RINGTONE_URI);
        player.start();
        return player;
    }

    public static String getPhoneNumber(Context context){
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
//            return null;
        TelephonyManager tMgr =
                (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
        return tMgr.getLine1Number();
    }

    public static String[] getPartsNumber(String number){
        PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
        try {
            // phone must begin with '+'
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(number, "");
            int countryCode = numberProto.getCountryCode();
            long nationalNumber = numberProto.getNationalNumber();
            String[] numberParts = new String[2];
            numberParts[0] = String.valueOf(countryCode);
            numberParts[1] = String.valueOf(nationalNumber);
            return numberParts;
        } catch (NumberParseException e) {
            Log.e("getPartsNumber", "NumberParseException was thrown: " + e.toString());
        }

        return null;
    }

    public static void getCallDetails(Activity activity) {

        Cursor managedCursor = activity.managedQuery(CallLog.Calls.CONTENT_URI, null,
                null, null, null);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        int cachedName = managedCursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        while (managedCursor.moveToNext()) {
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String name = managedCursor.getString(cachedName);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            Log.d("call log", "\nPhone Number:--- " + phNumber + " \nCall Type:--- "
                    + dir + " \nCall Date:--- " + callDayTime
                    + " \nCall duration in sec :--- " + callDuration
                    + " \nname: " + name);
        }


    }

}
