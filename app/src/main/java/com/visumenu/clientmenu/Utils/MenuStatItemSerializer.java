package com.visumenu.clientmenu.Utils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.visumenu.clientmenu.model.DtmfStatItem;
import com.visumenu.clientmenu.model.GeoItem;
import com.visumenu.clientmenu.model.MenuStatItem;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by user on 01.04.16.
 */
public class MenuStatItemSerializer implements JsonSerializer<MenuStatItem>{

    @Override
    public JsonElement serialize(MenuStatItem src, Type typeOfSrc, JsonSerializationContext context) {
        JsonObject jsonStatItem = new JsonObject();
        jsonStatItem.addProperty("ivr_number", src.getIVRNumber());
        JsonArray dtmfJsonArray = new JsonArray();
        List<DtmfStatItem> dtmfs = src.getDtmfStatItems();

        for(DtmfStatItem dtmf : dtmfs){
            JsonObject jsonDtmf = new JsonObject();
            jsonDtmf.addProperty("dtmf_seq", dtmf.getDtmf());
            jsonDtmf.addProperty("time", dtmf.getSendTime());
            jsonDtmf.addProperty("dtmf_stime", dtmf.getStime());
            dtmfJsonArray.add(jsonDtmf);
        }

        GeoItem geoItem = src.getGeoItem();
        JsonObject jsonGeo = new JsonObject();
        jsonGeo.addProperty("lat", geoItem.getLat());
        jsonGeo.addProperty("lng", geoItem.getLng());
        jsonStatItem.add("geo", jsonGeo);

        jsonStatItem.add("dtmf_data", dtmfJsonArray);

//        jsonObject.addProperty("dtmf_data");
        return jsonStatItem;
    }
}
