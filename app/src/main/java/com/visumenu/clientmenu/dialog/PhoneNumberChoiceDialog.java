package com.visumenu.clientmenu.dialog;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

/**
 * Created by user on 20.07.16.
 */
public class PhoneNumberChoiceDialog extends MaterialDialog {

    final String TAG = "PhoneNumberChoiceDialog";

    protected PhoneNumberChoiceDialog(Builder builder, List<String> phoneNumbers) {
        super(builder);
        ListView listView = (ListView) findViewById(android.R.id.list);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                getContext(),
                android.R.layout.activity_list_item,
                phoneNumbers
        );
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String phoneNumber = (String) parent.getAdapter().getItem(position);
                Log.d(TAG, "phonenumber choiced " + phoneNumber);
            }
        });
    }
}
