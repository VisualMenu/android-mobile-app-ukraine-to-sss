package com.visumenu.clientmenu.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by user on 18.07.16.
 */
public class FavoritesContactItem extends RealmObject {

    @PrimaryKey
    String id;
    Long time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
