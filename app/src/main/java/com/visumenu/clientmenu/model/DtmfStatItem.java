package com.visumenu.clientmenu.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by user on 22.03.16.
 */
public class DtmfStatItem extends RealmObject {

    @SerializedName("dtmf_seq")
    private String dtmf;

    @SerializedName("dtmf_stime")
    private String stime;

    @SerializedName("time")
    private Long sendTime;

    @Expose(serialize = false, deserialize = false)
    private boolean isSent = false;

    public String getDtmf() {
        return dtmf;
    }

    public void setDtmf(String dtmf) {
        this.dtmf = dtmf;
    }

    public Long getSendTime() {
        return sendTime;
    }

    public void setSendTime(Long sendTime) {
        this.sendTime = sendTime;
    }

    public boolean isSent() {
        return isSent;
    }

    public void setIsSend(boolean isSend) {
        this.isSent = isSend;
    }

    public String getStime() {
        return stime;
    }

    public void setStime(String stime) {
        this.stime = stime;
    }
}
