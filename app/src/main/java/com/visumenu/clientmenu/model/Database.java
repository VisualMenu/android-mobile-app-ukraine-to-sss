package com.visumenu.clientmenu.model;

import android.content.Context;
import android.util.Log;

import com.visumenu.clientmenu.Utils.DtmfItemList;
import com.visumenu.clientmenu.Utils.Favorites;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by user on 11.03.16.
 */
public class Database {

    private static Database ourInstance;
    private static final String IVR_NUMBER = "ivrNumber";
    public static final String IVR_MENU_STAT_NUMBER = "IVRNumber";
    private static final String IVR_NAME = "ivrName";
    private static final String IVR_MENU_ITEM_DTMF = "dtmf";
    private static final String DTMF = "dtmf";
    private static final String IS_SENT_STAT_ITEM = "isSent";
    private static final String RECENT_DTMF_NUMBER = "number";
    private static final String ORDER = "order";
    private static final String ORDER_RECENT = "recentOrder";
    private static final String ORDER_FAVORITES = "favoritesOrder";
    private static final String EMAIL = "email";
    private static final String RECENT = "recent";
    private static final String FAVORITES = "favorites";
    private static final String LAST_CALL_TIME = "lastCallTime";
    private static final String CALL_TIME = "callTime";
    private static final String IVR_MENU_S_TIME = "ivrMenuStime";
    private static final String RECENT_DTMF_STIME = "stime";
    private static final String IS_SERACH_RESULT = "isSearchResult";
    private static final String IS_POPULAR = "isPopular";
    private static final String LAST_UPDATE = "lastUpdate";
    private static final String ID = "id";
    final String TAG = "Database";

    private Realm realm;

    public static Database getInstance(Context context) {
        if(ourInstance == null)
            ourInstance = new Database(context.getApplicationContext(), false);
        return ourInstance;
    }

    public static Database getInstance(Context context, boolean createRealm) {
        if(ourInstance == null || createRealm)
            ourInstance = new Database(context.getApplicationContext(), createRealm);
        return ourInstance;
    }

    private Database(Context context, boolean createRealm) {
        Realm.setDefaultConfiguration(new RealmConfiguration.Builder(context).build());
        if (createRealm || realm == null || realm.isClosed())
            realm = Realm.getInstance(new RealmConfiguration.Builder(context).build());

    }

    public void close(){
        if(realm != null && !realm.isClosed()){
            realm.close();
            realm = null;
            ourInstance = null;
        }
    }

    public List<RecentDtmf> getAllRecentDtmf(){
        return realm.where(RecentDtmf.class).equalTo(RECENT, true)
                .findAllSorted(LAST_CALL_TIME, Sort.DESCENDING);
    }

    public List<RecentItem> getAllRecent(){
        return realm.where(RecentItem.class)
                .equalTo(RECENT, true)
                .findAllSorted(CALL_TIME, Sort.DESCENDING);
    }

    public List<RecentDtmf> getRecentDtmfs(String ivrNumber){
        RecentItem recentItem =
                realm.where(RecentItem.class).equalTo(IVR_NUMBER, ivrNumber).findFirst();

        if(recentItem == null)
            return null;

        return recentItem.getRecentDtmfs().where().findAllSorted(LAST_CALL_TIME);
    }

    public List<RecentDtmf> getAllFavorites(){
        RealmResults<RecentDtmf> results =
                realm.where(RecentDtmf.class).equalTo(FAVORITES, true).findAll();
        return results;
    }

    public List<IVR> getPopularIVRs(){
        return realm.where(IVR.class).equalTo(IS_POPULAR, true)
                .findAllSorted(IVR_NAME, Sort.ASCENDING);
    }

    public List<IVR> getSearchResultIVRs(){
        return realm.where(IVR.class).equalTo(IS_SERACH_RESULT, true)
                .findAllSorted(IVR_NAME, Sort.ASCENDING);
    }

    public void removeSearchResults() {
        Log.d(TAG, "removeSearchResults");
        realm.beginTransaction();
        RealmResults<IVR> searchResults =
                realm.where(IVR.class)
                        .equalTo(IS_SERACH_RESULT, true)
                        .equalTo(IS_POPULAR, false)
                        .findAll();
        if (searchResults.size() > 0)
            searchResults.deleteAllFromRealm();
        realm.commitTransaction();

        RealmResults<IVR> populateIVRs =
                realm.where(IVR.class)
                        .equalTo(IS_POPULAR, true)
                        .equalTo(IS_SERACH_RESULT, true)
                        .findAll();

        Log.d(TAG, "popular and search " + populateIVRs);
        if(populateIVRs.size() > 0) {
            realm.beginTransaction();
            for (int i = 0; i < populateIVRs.size(); i++){
                IVR ivr = populateIVRs.get(i);
                Log.d(TAG, "proceed ivr " + ivr.getIvrName());
                Log.d(TAG, "proceed ivr count " + populateIVRs.size() + " item number " + i);
                ivr.setSearchResult(false);
            }
            realm.commitTransaction();

            populateIVRs =
                    realm.where(IVR.class)
                            .equalTo(IS_POPULAR, true)
                            .equalTo(IS_SERACH_RESULT, true)
                            .findAll();

            Log.d(TAG, "popular and search  after update" + populateIVRs);
        }
    }

    public void addToIVRs(List<IVR> ivrs, boolean isSearch){

        realm.beginTransaction();

        long time = System.currentTimeMillis();
        for(IVR ivr : ivrs){
            IVR savedIVR = findIVRbyNumber(ivr.getIvrNumber());
            if(savedIVR == null){
                ivr.setLastUpdate(time);
                ivr.setSearchResult(isSearch);
                ivr.setPopular(!isSearch);
                realm.copyToRealm(ivr);
                continue;
            }

            savedIVR.setIvrVersion(ivr.getIvrVersion());
            savedIVR.setIvrName(ivr.getIvrName());
            savedIVR.setSearchResult(isSearch);
            if (!isSearch)
                savedIVR.setLastUpdate(time);
        }

        realm.commitTransaction();

        if (!isSearch) {
            realm.beginTransaction();
            RealmResults<IVR> oldIVRs =
                    realm.where(IVR.class)
                            .notEqualTo(LAST_UPDATE, time)
                            .equalTo(IS_POPULAR, true)
                            .equalTo(IS_SERACH_RESULT, false)
                            .findAll();
            if(oldIVRs.size() > 0)
                oldIVRs.deleteAllFromRealm();
            realm.commitTransaction();
        }
    }

    public int updateIVRMenu(String number, List<IVRMenuItem> ivrMenuItems){
        IVR ivr = realm.where(IVR.class).equalTo(IVR_NUMBER, number).findFirst();
        if(ivr == null)
            return 0;

        realm.beginTransaction();
        ivr.getMenuItems().clear();
        ivr.getMenuItems().addAll(ivrMenuItems);
        realm.commitTransaction();
        return ivrMenuItems.size();
    }

    public IVR findIVRbyNumber(String number){
        return realm.where(IVR.class).
                equalTo(IVR_NUMBER, number).findFirst();
    }

    public String getIVRMenuVersion(String number){
        IVR ivr  = realm.where(IVR.class).equalTo("ivrNumber", number).findFirst();
        if(ivr == null)
            return null;

        return ivr.getIvrVersion();
    }

    public long getMaxParentRecentCount(List<RecentDtmf> list){
        long max = 0;
        for(RecentDtmf recentDtmf : list){
            long count = recentDtmf.getParentItems().where().count();
            if(count > max)
                max = count;
        }
        return max;
    }

    public List<IVRMenuItem> getIVRMenuItems(String ivrNumber, String dtmf, String stime){
        IVR ivr = realm.where(IVR.class).equalTo(IVR_NUMBER, ivrNumber).findFirst();
        if(ivr == null)
            return null;

        return getIVRMenuItems(ivr, dtmf, stime);
    }

    public Set<String> getFavorites(){
        Set<String> favoritesSet = new HashSet<>();
        List<RecentDtmf> favorites = realm.where(RecentDtmf.class).findAll();
        for(RecentDtmf recentDtmf : favorites)
            favoritesSet.add(recentDtmf.getDtmf() + ":" + recentDtmf.getStime());

        return favoritesSet;
    }

    public void removeFavorites(RecentDtmf recentDtmf){
        realm.beginTransaction();
        if(recentDtmf.isRecent())
            recentDtmf.setFavorites(false);
        else
            recentDtmf.deleteFromRealm();
        realm.commitTransaction();
    }

    public void addFavoritesDtmf(final String ivrNumber,
                                 final String ivrName,
                                 final String dtmfName,
                                 final String dtmf,
                                 final String stime,
                                 final String subType,
                                 final List<ParentDtmfItem> parentNames){

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Log.d(TAG, "addFavoritesDtmf with dtmf " + dtmf + " name " + dtmfName );

                RecentDtmf recentDtmf = getRecenttmf(realm, dtmf, ivrNumber, stime);

                boolean addToFavorites;

                String dtmfStime = dtmf + ":" + stime;

                if (recentDtmf == null) {
                    Log.d(TAG, "create new favorites");
                    recentDtmf = createRecentDtmf(realm, ivrNumber, ivrName,
                            dtmfName, dtmf, stime, subType, parentNames,
                            true, false);
                    addToFavorites = true;
                } else {
                    if(!recentDtmf.isRecent() && recentDtmf.isFavorites()){
                        Log.d(TAG, "removing favorites");
                        recentDtmf.deleteFromRealm();
                        Favorites.getFavorites().remove(dtmfStime);
                        return;
                    }
                    addToFavorites = !recentDtmf.isFavorites();
                }

                Log.d(TAG, addToFavorites ? "adding " : "removing " + dtmfStime);

                if(addToFavorites)
                    Favorites.getFavorites().add(dtmfStime);
                else
                    Favorites.getFavorites().remove(dtmfStime);

                recentDtmf.setLastCallTime(System.currentTimeMillis());

                recentDtmf.setFavorites(addToFavorites);
            }
        });
    }

    public void addRecentDtmf(String ivrNumber, String ivrName, IVRMenuItem menuItem,
                              List<ParentDtmfItem> parentNames) {

        addRecentDtmf(ivrNumber, ivrName, menuItem.getName(), menuItem.getDtmf(),
                menuItem.getIvrMenuStime(), menuItem.getSubtype(), parentNames);
    }

    public void addFavoritesDtmf(String ivrNumber, String ivrName, IVRMenuItem menuItem,
                                 List<ParentDtmfItem> parentNames) {

        addFavoritesDtmf(ivrNumber, ivrName, menuItem.getName(), menuItem.getDtmf(),
                menuItem.getIvrMenuStime(), menuItem.getSubtype(), parentNames);
    }

    public void addFavoritesDtmf(RecentDtmf recentDtmf){
        realm.beginTransaction();
        recentDtmf.setFavorites(!recentDtmf.isFavorites());
        if (recentDtmf.isFavorites())
            Favorites.getFavorites().remove(recentDtmf.getDtmf() + ":" + recentDtmf.getStime());
        else
            Favorites.getFavorites().add(recentDtmf.getDtmf() + ":" + recentDtmf.getStime());
        realm.commitTransaction();
    }

    public void addRecentDtmf(final String ivrNumber,
                              final String ivrName,
                              final String dtmfName,
                              final String dtmf,
                              final String stime,
                              final String subType,
                              final List<ParentDtmfItem> parentItems){

        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Log.d(TAG, "addFavoritesDtmf with dtmf " + dtmf + " name " + dtmfName );

                RecentDtmf recentDtmf = getRecenttmf(realm, dtmf, ivrNumber, stime);

                if(recentDtmf == null) {
                    recentDtmf = createRecentDtmf(realm, ivrNumber, ivrName,
                            dtmfName, dtmf, stime, subType, parentItems,
                            false, true);
                }

                recentDtmf.setLastCallTime(System.currentTimeMillis());

            }
        });
    }

    private RecentDtmf getRecenttmf(Realm realm, String dtmf, String ivrNumber, String stime){
        return realm.where(RecentDtmf.class)
                .equalTo(DTMF, dtmf)
                .equalTo(RECENT_DTMF_NUMBER, ivrNumber)
                .equalTo(RECENT_DTMF_STIME, stime)
                .findFirst();
    }

    private RecentDtmf createRecentDtmf (Realm realm, String ivrNumber,
                                         String ivrName,
                                         String dtmfName,
                                         String dtmf,
                                         String stime,
                                         String subType,
                                         List<ParentDtmfItem> parentItems,
                                         boolean favorites,
                                         boolean recent) {

        RecentDtmf recentDtmf = realm.createObject(RecentDtmf.class);
        recentDtmf.setCompanyName(ivrName);
        recentDtmf.setNumber(ivrNumber);
        recentDtmf.setDtmf(dtmf);
        recentDtmf.setName(dtmfName);
        recentDtmf.setStime(stime);
        recentDtmf.setSubType(subType);
        recentDtmf.setFavorites(false);
        recentDtmf.setFavorites(favorites);
        recentDtmf.setRecent(recent);
        recentDtmf.getParentItems().addAll(parentItems);
        return recentDtmf;
    }

    public List<IVRMenuItem> getIVRMenuItems(IVR ivr, String dtmf, String stime){
        List<IVRMenuItem> ivrMenuItemList =
                ivr.getMenuItems().where().beginsWith("dtmf", dtmf)
                        .beginsWith(IVR_MENU_S_TIME, stime).findAll();

        int dtmfChildLength = dtmf.length() + 1;
        List<IVRMenuItem> ivrMenuResults = new ArrayList<>();

        for(IVRMenuItem menuItem : ivrMenuItemList)
            if(menuItem.getDtmf().length() == dtmfChildLength)
                ivrMenuResults.add(menuItem);

        if(ivrMenuResults.size() == 0 && dtmf.length() == 0)
            ivrMenuResults.addAll(ivr.getMenuItems().where().equalTo("dtmf", "").findAll());

        return ivrMenuResults;
    }



    public List<IVR> getIVRs(String query){
        return realm.where(IVR.class).contains("ivrName", query,Case.INSENSITIVE)
                .or()
                .contains("ivrNumber", query, Case.INSENSITIVE)
                .findAll();
    }

    public void setIVRVersion(String number, String version){
        realm.beginTransaction();
        IVRVersion ivrVersion = realm.createObject(IVRVersion.class);
        ivrVersion.setIvrNumber(number);
        ivrVersion.setIvrMenuVersion(version);
        realm.commitTransaction();
    }

    public String getIVRVersion(String ivrNumber){
        IVRVersion ivrVersion =
                realm.where(IVRVersion.class).equalTo(IVR_NUMBER, ivrNumber).findFirst();
        if (ivrVersion == null)
            return null;

        return ivrVersion.getIvrMenuVersion();
    }

    public List<IVRMenuItem> getIvrMenuItems(String ivrNumber, List<String> dtmfsWithStime){
        if (dtmfsWithStime == null || dtmfsWithStime.size() == 0)
            return null;

        RealmResults<IVRMenuItem> menuItems =
                realm.where(IVR.class).equalTo(IVR_NUMBER, ivrNumber)
                        .findFirst()
                        .getMenuItems().where().findAllSorted(IVR_MENU_ITEM_DTMF);

        List<IVRMenuItem> resultItems = new ArrayList<>();
        for (IVRMenuItem item : menuItems){
            String dtmfstime = item.getDtmf() + ":" + item.getIvrMenuStime();
            if(dtmfsWithStime.contains(dtmfstime))
                resultItems.add(item);
        }

        return resultItems;

    }

    public static HashMap<String, DtmfItemList> listToDtmfMap(List<IVRMenuItem> itemList) {

        HashMap<String, DtmfItemList> hashMap = new HashMap<>();

        for(IVRMenuItem ivrMenuItem : itemList){
            DtmfItemList dtmfItemList = new DtmfItemList();
            dtmfItemList.setParentItem(ivrMenuItem);
            hashMap.put(ivrMenuItem.getDtmf() + ":" + ivrMenuItem.getIvrMenuStime(), dtmfItemList);
        }

        for(IVRMenuItem ivrMenuItem : itemList){
            String dtmf = ivrMenuItem.getDtmf();
            if(ivrMenuItem.getDtmf().length() <= 1)
                continue;

            String stime = ivrMenuItem.getIvrMenuStime();
            int lastDividerPosition = stime.lastIndexOf("|");
            String parentStime = lastDividerPosition > 0 ?
                    stime.substring(0, lastDividerPosition) : "";

            String parentDtmf = dtmf.substring(0, dtmf.length() - 1);
            DtmfItemList childList = hashMap.get(parentDtmf + ":" + parentStime);

            if(childList == null)
                throw new RuntimeException("item " + dtmf + " has no child");

            childList.add(ivrMenuItem);
            hashMap.put(parentDtmf, childList);
        }

        return hashMap;
    }

    public static List<IVRMenuItem> getItemsWithoutChild(HashMap<String, DtmfItemList> map){
        Set<String> dtmfSet = map.keySet();
        List<IVRMenuItem> itemsWithoutChild = new ArrayList<>();
        for(String dtmf : dtmfSet){
            if(dtmf.length() < 2)
                continue;

            List<IVRMenuItem> menuItems = map.get(dtmf);
            if(menuItems.size() == 0)
                itemsWithoutChild.add(((DtmfItemList)menuItems).getParentItem());
        }

        if(itemsWithoutChild.size() == 0) {
            for(String dtmf : dtmfSet){
                itemsWithoutChild.add(((DtmfItemList)map.get(dtmf)).getParentItem());
            }
        }

        Collections.sort(itemsWithoutChild, new Comparator<IVRMenuItem>() {
            @Override
            public int compare(IVRMenuItem ivrMenuItem1, IVRMenuItem ivrMenuItem2) {
                return ivrMenuItem1.getOrder() - ivrMenuItem2.getOrder();
            }
        });

        return itemsWithoutChild;
    }

    public static HashSet<String> getItemKeysWithoutChild(HashMap<String, DtmfItemList> map) {
        Set<String> dtmfSet = map.keySet();
        HashSet<String> itemsWithoutChild = new HashSet<>();
        for (String dtmf : dtmfSet) {
            if (dtmf.length() < 2)
                continue;

            List<IVRMenuItem> menuItems = map.get(dtmf);
            if (menuItems.size() == 0) {
                IVRMenuItem parentItem = ((DtmfItemList)menuItems).getParentItem();
                itemsWithoutChild.add(parentItem.getDtmf() + ":" + parentItem.getIvrMenuStime());
            }
        }

        if (itemsWithoutChild.size() == 0) {
            for (String dtmf : dtmfSet) {
                IVRMenuItem parentItem = map.get(dtmf).getParentItem();
                itemsWithoutChild.add(parentItem.getDtmf() + ":" + parentItem.getIvrMenuStime());
            }
        }

        return itemsWithoutChild;
    }

    public void addStatItem(String ivrNumber, String dtmf, String stime) {
        realm.executeTransactionAsync(new StatTransaction(ivrNumber, dtmf, stime));
    }

    public User getUser() {
        return realm.where(User.class).findFirst();
    }

    public static User getUser(Realm realm) {
        return realm.where(User.class).findFirst();
    }

    public void updateUser(User user) {
        if(user == null)
            return;

        if(user.getEmail() == null)
            user.setEmail("");

        RealmResults<User> results = realm.where(User.class)
                .notEqualTo(EMAIL, user.getEmail()).findAll();
        realm.beginTransaction();
        if(results.size() > 0)
            results.deleteAllFromRealm();

        realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();
    }

    public static List<MenuStatItem> getUnSentStatInfo(Realm realm) {

        if(realm.where(DtmfStatItem.class).equalTo(IS_SENT_STAT_ITEM, true).count() > 0)
            removeAllSent(realm);

        return realm.where(MenuStatItem.class).findAll();
    }

    public static void removeAllSent(Realm realm) {
        RealmResults<DtmfStatItem> results =
                realm.where(DtmfStatItem.class).equalTo(IS_SENT_STAT_ITEM, true).findAll();
        realm.beginTransaction();
        results.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void addRecentItems(final List<RecentItem> list) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for(RecentItem item : list){
                    item.setIsRecent(true);
                    item.setCallTime(item.getRecentOrder());
                }

                realm.copyToRealmOrUpdate(list);
            }
        });
    }

    public void SetFavorites(final String ivrNumber, final String ivrName, final boolean like) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RecentItem item =
                        realm.where(RecentItem.class).equalTo(IVR_NUMBER, ivrNumber).findFirst();
                if(item != null){
                    if(!like && !item.isRecent())
                        item.deleteFromRealm();
                    else
                        item.setIsFavorites(like);
                    return;
                }

                if(!like)
                    return;

                item = realm.createObject(RecentItem.class);
                item.setIsRecent(false);
                item.setIsFavorites(true);
                item.setIvrNumber(ivrNumber);
                item.setIvrName(ivrName);
            }
        });
    }

    public void SetRecent(final String ivrNumber) {
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {

                RecentItem item =
                        realm.where(RecentItem.class).equalTo(IVR_NUMBER, ivrNumber).findFirst();

                if(item != null){
                    item.setIsRecent(true);
                    item.setCallTime(System.currentTimeMillis());
                    Log.d(TAG, "recent changed " + ivrNumber);
                    Log.d(TAG, "ivr number of changed item " + item.getIvrNumber());
                    return;
                }

                IVR ivr = realm.where(IVR.class).equalTo(IVR_NUMBER, ivrNumber).findFirst();

                if(ivr == null){
                    throw new RuntimeException("IVR not found");
//                    return;
                }

                item = realm.createObject(RecentItem.class);
                item.setIsRecent(true);
                item.setIsFavorites(false);
                item.setCallTime(System.currentTimeMillis());
                item.setIvrNumber(ivrNumber);
                item.setIvrName(ivr.getIvrName());
                Log.d(TAG, "recent created " + ivrNumber);
            }
        });
    }

    public static IVRMenuItem getMenuItemByDtmfStime(IVR ivr, String dtmf, String stime) {
        return ivr.getMenuItems().where().equalTo(DTMF, dtmf)
                .equalTo(IVR_MENU_S_TIME, stime).findFirst();
    }

    public void fetchLikesNumbers() {
        RealmResults<RecentDtmf> results =
                realm.where(RecentDtmf.class).equalTo(FAVORITES, true).findAll();
        Set<String> favorites = Favorites.getFavorites();

        for(RecentDtmf item : results)
            favorites.add(item.getIVRNumber());
    }

    public void clearIvrList() {
        realm.beginTransaction();
        realm.where(IVR.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void clearAllData() {
        realm.beginTransaction();
        realm.deleteAll();
        realm.commitTransaction();
    }

    public boolean hasIvrNumber(String ivrNumber) {
        return realm.where(IVR.class).equalTo(IVR_NUMBER, ivrNumber).count() > 0;
    }

    public void removeUserInfo(){
        realm.beginTransaction();
        realm.where(User.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void addFavoriteContact(String contactId){
        Log.d(TAG, "addFavoriteContact id " + contactId);
        realm.beginTransaction();
        FavoritesContactItem contactItem = realm.createObject(FavoritesContactItem.class);
        contactItem.setId(contactId);
        contactItem.setTime(System.currentTimeMillis());
        realm.commitTransaction();
    }

    public void removeContactFromFavorites(String contactId){
        Log.d(TAG, "removeContactFromFavorites id " + contactId);
        realm.beginTransaction();
        RealmQuery<FavoritesContactItem> query =
                realm.where(FavoritesContactItem.class).equalTo(ID, contactId);
        if(query.count() > 0)
            query.findAll().deleteFromRealm(0);
        realm.commitTransaction();
    }

    public Set<String> getFavoritesContactIds(){
        Set<String> favoritesSet = new HashSet<>();
        List<FavoritesContactItem> favorites = realm.where(FavoritesContactItem.class).findAll();
        for (FavoritesContactItem contactItem : favorites)
            favoritesSet.add(contactItem.getId());
        return favoritesSet;
    }

    //Implement later, when api will endpoint of stats
//
//    public void removeAllSent(){
//        List<DtmfStatItem> dtmfItems =
//                realm.where(DtmfStatItem.class).equalTo(IS_SENT_STAT_ITEM, true).findAll();
//        realm.beginTransaction();
//    }
//
//    public List<MenuStatItem> getUnsentStat(){
//        List<MenuStatItem> statList = realm.where(MenuStatItem.class).findAll();
//        if(statList.size() == 0)
//            return null;
//
//        List<MenuStatItem> statItems = new ArrayList<>();
//        for(MenuStatItem item: statList){
//            MenuStatItem statItem = new MenuStatItem();
//            item.getDtmfStatItems();
//            item.getIVRNumber();
//        }
//    }
}
