package com.visumenu.clientmenu.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;

/**
 * Created by user on 14.03.16.
 */
public class IVRMenuItem extends RealmObject {

    @SerializedName("ivr_menu_name")
    private String name;

    @SerializedName("ivr_menu_keywords")
    private String keywords;

    @SerializedName("ivr_menu_dtmf_seq")
    private String dtmf;

    @SerializedName("ivr_menu_subtype")
    private String subtype;

    @SerializedName("ivr_menu_stime")
    private String ivrMenuStime;

    int order;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getDtmf() {
        return dtmf;
    }

    public void setDtmf(String dtmf) {
        this.dtmf = dtmf;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getIvrMenuStime() {
        return ivrMenuStime;
    }

    public void setIvrMenuStime(String ivrMenuStime) {
        this.ivrMenuStime = ivrMenuStime;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
