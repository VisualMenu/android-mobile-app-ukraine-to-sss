package com.visumenu.clientmenu.model;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by user on 21.04.16.
 */
public class RecentDtmf  extends RealmObject{

    String name;

    String dtmf;

    long lastCallTime;

    String stime;

    String companyName, number;

    String subType;

    boolean recent;

    boolean favorites;

    RealmList<ParentDtmfItem> parentItems;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDtmf() {
        return dtmf;
    }

    public void setDtmf(String dtmf) {
        this.dtmf = dtmf;
    }

    public long getLastCallTime() {
        return lastCallTime;
    }

    public void setLastCallTime(long lastCallTime) {
        this.lastCallTime = lastCallTime;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getIVRNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStime() {
        return stime;
    }

    public void setStime(String stime) {
        this.stime = stime;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public boolean isRecent() {
        return recent;
    }

    public void setRecent(boolean recent) {
        this.recent = recent;
    }

    public boolean isFavorites() {
        return favorites;
    }

    public void setFavorites(boolean favorites) {
        this.favorites = favorites;
    }

    public RealmList<ParentDtmfItem> getParentItems() {
        return parentItems;
    }

    public void setParentItems(RealmList<ParentDtmfItem> parentItems) {
        this.parentItems = parentItems;
    }
}
