package com.visumenu.clientmenu.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by user on 14.04.16.
 */
public class RecentItem extends RealmObject {

    @SerializedName("ivr_name")
    String ivrName;

    @PrimaryKey
    @SerializedName("ivr_number")
    String ivrNumber;

    @SerializedName("ivr_version")
    String ivrVersion;

    @SerializedName("order")
    int recentOrder;

    int favoritesOrder;

    boolean isRecent;

    boolean isFavorites;

    long callTime;

    String subType;

    RealmList<RecentDtmf> recentDtmfs;

    public String getIvrName() {
        return ivrName;
    }

    public void setIvrName(String ivrName) {
        this.ivrName = ivrName;
    }

    public String getIvrNumber() {
        return ivrNumber;
    }

    public void setIvrNumber(String ivrNumber) {
        this.ivrNumber = ivrNumber;
    }

    public String getIvrVersion() {
        return ivrVersion;
    }

    public void setIvrVersion(String ivrVersion) {
        this.ivrVersion = ivrVersion;
    }

    public int getRecentOrder() {
        return recentOrder;
    }

    public void setRecentOrder(int recentOrder) {
        this.recentOrder = recentOrder;
    }

    public int getFavoritesOrder() {
        return favoritesOrder;
    }

    public void setFavoritesOrder(int favoritesOrder) {
        this.favoritesOrder = favoritesOrder;
    }

    public boolean isRecent() {
        return isRecent;
    }

    public void setIsRecent(boolean isRecent) {
        this.isRecent = isRecent;
    }

    public boolean isFavorites() {
        return isFavorites;
    }

    public void setIsFavorites(boolean isFavorites) {
        this.isFavorites = isFavorites;
    }

    public long getCallTime() {
        return callTime;
    }

    public void setCallTime(long callTime) {
        this.callTime = callTime;
    }

    public RealmList<RecentDtmf> getRecentDtmfs() {
        return recentDtmfs;
    }

    public void setRecentDtmfs(RealmList<RecentDtmf> recentDtmfs) {
        this.recentDtmfs = recentDtmfs;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }
}
