package com.visumenu.clientmenu.model;

import io.realm.Realm;

/**
 * Created by user on 22.03.16.
 */
public class StatTransaction implements Realm.Transaction {

    String dtmf;
    String ivrNumber;
    String stime;

    public StatTransaction(String ivrNumber, String dtmf, String stime){
        this.dtmf = dtmf;
        this.ivrNumber = ivrNumber;
        this.stime = stime;
    }

    @Override
    public void execute(Realm bgRealm) {
        MenuStatItem item = bgRealm.where(MenuStatItem.class)
                .equalTo(Database.IVR_MENU_STAT_NUMBER, ivrNumber)
                .findFirst();
        if(item == null){
            item = bgRealm.createObject(MenuStatItem.class);
            item.setIVRNumber(ivrNumber);
            GeoItem geoItem = bgRealm.createObject(GeoItem.class);
            geoItem.setLat(16.73737337);
            geoItem.setLng(33.77474747);
            item.setGeoItem(geoItem);
        }

        if(dtmf == null)
            return;

        DtmfStatItem dtmfStatItem = bgRealm.createObject(DtmfStatItem.class);
        dtmfStatItem.setDtmf(dtmf);
        dtmfStatItem.setStime(stime);
        dtmfStatItem.setSendTime(System.currentTimeMillis());
        item.getDtmfStatItems().add(dtmfStatItem);
    }

}
