package com.visumenu.clientmenu.model;

import io.realm.RealmObject;

/**
 * Created by user on 18.03.16.
 */
public class IVRVersion extends RealmObject {

    private String ivrNumber;
    private String ivrMenuVersion;

    public String getIvrNumber() {
        return ivrNumber;
    }

    public void setIvrNumber(String ivrNumber) {
        this.ivrNumber = ivrNumber;
    }

    public String getIvrMenuVersion() {
        return ivrMenuVersion;
    }

    public void setIvrMenuVersion(String ivrMenuVersion) {
        this.ivrMenuVersion = ivrMenuVersion;
    }
}
