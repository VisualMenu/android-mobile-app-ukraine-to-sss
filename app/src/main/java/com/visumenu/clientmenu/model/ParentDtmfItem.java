package com.visumenu.clientmenu.model;

import io.realm.RealmObject;

/**
 * Created by user on 10.06.16.
 */
public class ParentDtmfItem extends RealmObject{

    int level;
    String itemName;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
