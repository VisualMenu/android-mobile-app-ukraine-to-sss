package com.visumenu.clientmenu.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by user on 11.03.16.
 */
public class IVR extends RealmObject{

    @SerializedName("ivr_name")
    private String ivrName;

    @PrimaryKey
    @SerializedName("ivr_number")
    private String ivrNumber;

    @SerializedName("ivr_version")
    private String ivrVersion;

    @SerializedName("is_empty")
    private boolean emptyMenu;

    private int order;

    private boolean isSearchResult;
    private boolean isPopular;
    private long lastUpdate;

    private RealmList<IVRMenuItem> menuItems;

    public String getIvrName() {
        return ivrName;
    }

    public void setIvrName(String ivrName) {
        this.ivrName = ivrName;
    }

    public String getIvrNumber() {
        return ivrNumber;
    }

    public void setIvrNumber(String ivrNumber) {
        this.ivrNumber = ivrNumber;
    }

    public String getIvrVersion() {
        return ivrVersion;
    }

    public void setIvrVersion(String ivrVersion) {
        this.ivrVersion = ivrVersion;
    }

    public RealmList<IVRMenuItem> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(RealmList<IVRMenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public boolean isEmptyMenu() {
        return emptyMenu;
    }

    public void setEmptyMenu(boolean emptyMenu) {
        this.emptyMenu = emptyMenu;
    }

    public boolean isSearchResult() {
        return isSearchResult;
    }

    public void setSearchResult(boolean searchResult) {
        isSearchResult = searchResult;
    }

    public boolean isPopular() {
        return isPopular;
    }

    public void setPopular(boolean popular) {
        isPopular = popular;
    }

    public long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
