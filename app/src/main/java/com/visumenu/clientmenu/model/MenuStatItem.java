package com.visumenu.clientmenu.model;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by user on 22.03.16.
 */

public class MenuStatItem extends RealmObject {

    @SerializedName("ivr_number")
    private String IVRNumber;

    @SerializedName("dtmf_data")
    private RealmList<DtmfStatItem> dtmfStatItems;

    @SerializedName("geo")
    private GeoItem geoItem;

    public String getIVRNumber() {
        return IVRNumber;
    }

    public void setIVRNumber(String IVRNumber) {
        this.IVRNumber = IVRNumber;
    }

    public RealmList<DtmfStatItem> getDtmfStatItems() {
        return dtmfStatItems;
    }

    public void setDtmfStatItems(RealmList<DtmfStatItem> dtmfStatItems) {
        this.dtmfStatItems = dtmfStatItems;
    }

    public GeoItem getGeoItem() {
        return geoItem;
    }

    public void setGeoItem(GeoItem geoItem) {
        this.geoItem = geoItem;
    }
}
