package com.visumenu.clientmenu;

import android.app.Application;

/**
 * Created by user on 11.03.16.
 */
public class VisuMenuApp extends Application {

    private static VisuMenuApp app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
//        TwitterAuthConfig authConfig =  new TwitterAuthConfig("consumerKey", "consumerSecret");
    }

    public static VisuMenuApp getInstance(){
        return app;
    }
}
