package com.visumenu.clientmenu;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.visumenu.clientmenu.Utils.CallUtils;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.User;
import com.visumenu.clientmenu.request.API;
import com.visumenu.clientmenu.service.DataFetchService;
import com.visumenu.clientmenu.service.EventMessage;
import com.visumenu.clientmenu.service.FetchDataConstants;
import com.visumenu.clientmenu.service.PreferencesConstants;
import com.visumenu.clientmenu.service.SendStatDataService;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.security.KeyStore;
import java.security.cert.CertificateFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by user on 10.03.16.
 */
public class Util {

    public static final int NOTIFICATION_ID = 10;

    public static final long THREE_HOURS_IN_MILLIS = 3 * 60 * 60 * 1000;

    public static String getUrl(Context context, int resPartUrl){

        return String.format(context.getString(resPartUrl),
                context.getString(R.string.companies_api_hostname));
    }

    public static void endCall(Context context){
        TelephonyManager telephonyManager = (TelephonyManager)context.getApplicationContext()
                .getSystemService(Context.TELEPHONY_SERVICE);
        Class<TelephonyManager> c = TelephonyManager.class;
        Method getITelephonyMethod = null;
        try {
            getITelephonyMethod = c.getDeclaredMethod("getITelephony",(Class[]) null);
            getITelephonyMethod.setAccessible(true);
            ITelephony iTelephony = (ITelephony) getITelephonyMethod.invoke(telephonyManager, (Object[]) null);
            iTelephony.endCall();
            Log.d("endCall", "endCall......");
        } catch (Exception e) {
            Log.e("endCall", "endCallError", e);
        }
    }

    public static void fetchMenuData(Context context, String ivrNumber){
        Intent intent = new Intent(context, DataFetchService.class);
        intent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_FETCH_DETMF_TOP);
        intent.putExtra(FetchDataConstants.IVR_NUMBER, ivrNumber);
        context.startService(intent);
    }

    public static void setSendStatDataTime(Context context){
        Log.d("setSendStat","update");
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, SendStatDataService.class);
        intent.putExtra(FetchDataConstants.SEND_TYPE, FetchDataConstants.CODE_SEND_STAT);
        PendingIntent alarmIntent =
                PendingIntent.getService(context.getApplicationContext(), 0, intent, 0);
        alarmMgr.set(AlarmManager.RTC, System.currentTimeMillis() + 1 * 60 * 1000, alarmIntent);
    }

//    public static void cancelRequestCallbackStatus(Context context){
//        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
//        alarmMgr.cancel(getRequestCallbackStatusIntent(context));
//    }
//
//    public static void requestCallbackStatus(Context context){
//        Log.d("requestCallbackStatus", "new request");
//        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
//        PendingIntent alarmIntent = getRequestCallbackStatusIntent(context);
//        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
//                System.currentTimeMillis() + 5 * 1000, 60 * 1000, alarmIntent);
//    }
//
//    public static PendingIntent getRequestCallbackStatusIntent(Context context){
//        Intent intent = new Intent(context, CallbackSendService.class);
//        intent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_CHANGED_CALLBACK_STATUS);
//        return PendingIntent.getService(context.getApplicationContext(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
//    }

    public static API getMainAPI(Context context){

        OkHttpClient okHttpClient =
                /*BuildConfig.DEBUG ? getHttpClient(context) :*/ new OkHttpClient();

        if(okHttpClient == null){
//            Log.e(TAG, "okhttpclient is null");
            return null;
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(context.getString(R.string.main_api_hostname))
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit.create(API.class);
    }

    private static OkHttpClient getHttpClient(Context context){
        OkHttpClient okHttpClient = null;

        // only for test
        try{
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream cert = context.getResources().openRawResource(R.raw.cert);
            java.security.cert.Certificate ca;
            ca = cf.generateCertificate(cert);
            cert.close();
            // creating a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // creating a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // creating an SSLSocketFactory that uses our TrustManager
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);
            okHttpClient = new OkHttpClient.Builder()
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    })
                    .sslSocketFactory(sslContext.getSocketFactory())
                    .build();
        }catch (Exception e){
            Log.e("okhttp", "ssl error", e);
        }

        return okHttpClient;
    }

    public static String getDeviceId(Context context){
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static void writeCallbackToken(Context context, String token){
        writeSipPreference(context, FetchDataConstants.CALLBACK_TOKEN, token);
    }

    public static void writeConnectionIsAlive(Context context, boolean alive) {
        writeSipPreference(context, FetchDataConstants.IS_CONNECT_ALIVE, alive);
    }

    public static boolean getConnectAlive(Context context) {
        SharedPreferences sp =
                context.getSharedPreferences(FetchDataConstants.SP_SIP_INFO, Context.MODE_PRIVATE);
        return sp.getBoolean(FetchDataConstants.IS_CONNECT_ALIVE, false);
    }

    public static void writeSipPreference(Context context,  String name, String value){
        context.getSharedPreferences(FetchDataConstants.SP_SIP_INFO, Context.MODE_PRIVATE)
                .edit()
                .putString(name, value)
                .commit();
    }

    public static void writeSipPreference(Context context,  String name, int value){
        context.getSharedPreferences(FetchDataConstants.SP_SIP_INFO, Context.MODE_PRIVATE)
                .edit()
                .putInt(name, value)
                .commit();
    }

    public static void writeSipPreference(Context context,  String name, long value){
        context.getSharedPreferences(FetchDataConstants.SP_SIP_INFO, Context.MODE_PRIVATE)
                .edit()
                .putLong(name, value)
                .commit();
    }

    public static void writeSipPreference(Context context,  String name, boolean value){
        context.getSharedPreferences(FetchDataConstants.SP_SIP_INFO, Context.MODE_PRIVATE)
                .edit()
                .putBoolean(name, value)
                .commit();
    }

    public static String createDirectory(Context context, String name){
        File f = new File(context.getCacheDir(), name);
        if (!f.exists()) {
            f.mkdirs();
        }
        return f.getPath();
    }

    public static void sendErrorEvent(Context context, ResponseBody responseBody){
        String message = null;
        JsonObject errorJson = null;
        try {
            String json = responseBody.string();
            Log.d("sendErrorEvent", "error " + json);

            if (json == null)
                message = context.getString(R.string.unknown_error);
            else {
                errorJson = new JsonParser().parse(json).getAsJsonObject();

                if (errorJson == null) {
                    Util.sendMessageEvent(
                            FetchDataConstants.TYPE_ERROR,
                            context.getString(R.string.unknown_error)
                    );
                    return;
                }

                message = parseErrorMessage(errorJson, context.getString(R.string.unknown_error));
            }

            JsonElement smsCodeValid = errorJson.get(FetchDataConstants.VALIDATION_NUMBER);

            if(smsCodeValid != null && smsCodeValid.getAsInt()== FetchDataConstants.CODE_SMS_INVALID) {
                Util.sendMessageEvent(FetchDataConstants.TYPE_PHONE_NUMBER_NOT_CONFIRMED, message);
                return;
            }

            Util.sendMessageEvent(FetchDataConstants.TYPE_ERROR, message);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String parseErrorMessage(JsonObject errorObject, String defaultErrorMessage)
            throws IOException {

        JsonElement jsonElement;
        if ((jsonElement = errorObject.get(FetchDataConstants.ERROR)) == null)
            return defaultErrorMessage;

        return jsonElement.getAsString();
    }

    public static void cancelNotification(Context context){
        NotificationManager mNotifyMgr =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotifyMgr.cancel(NOTIFICATION_ID);
    }

    public static void sendMessageEvent(int type, String message){
        EventMessage eventMessage = new EventMessage(type, message);
        EventBus.getDefault().post(eventMessage);
    }

    public static void storeOutgoingPhoneNumber(Context context, String number){
        context.getSharedPreferences(PreferencesConstants.CALL_PREF, Context.MODE_PRIVATE)
                .edit()
                .putString(FetchDataConstants.PHONE_NUMBER, number)
                .commit();
    }

    public static String getOutGoingPhoneNumber(Context context){
        SharedPreferences sp =
                context.getSharedPreferences(PreferencesConstants.CALL_PREF, Context.MODE_PRIVATE);
        return sp.getString(FetchDataConstants.PHONE_NUMBER, null);
    }

    public static Long getRequestCallbackTime(Context context){
        SharedPreferences sp =
                context.getSharedPreferences(FetchDataConstants.SP_SIP_INFO, Context.MODE_PRIVATE);
        return sp.getLong(FetchDataConstants.SEND_CALLBACK_REQ_TIME, 0);
    }

    public static int getStime(Context context){
        SharedPreferences sp =
                context.getSharedPreferences(FetchDataConstants.SP_SIP_INFO, Context.MODE_PRIVATE);
        return sp.getInt(FetchDataConstants.S_TIME, 0);
    }

    public static String getUserPhoneNumber(Context context){
        Database database = Database.getInstance(context);
        User user = database.getUser();
        String phoneNumberUnchecked = (user.getPhoneNumber() == null ?
                CallUtils.getPhoneNumber(context.getApplicationContext()) :
                user.getPhoneNumber());

        if(phoneNumberUnchecked == null){
            return null;
        }

        String phoneNumber = ((phoneNumberUnchecked.startsWith("380")) ? "1002" : "") +
                phoneNumberUnchecked;

        return phoneNumber;
    }

    public static void setStime(Context context, int stimeValue){
        Util.writeSipPreference(context,
                FetchDataConstants.S_TIME,
                stimeValue);
    }

    public static void writeDtmf(Context context, String dtmf){
        writeSipPreference(context, FetchDataConstants.DTMF_SEQ, dtmf);
    }

    public static void writeIvrNumber(Context context, String number){
        writeSipPreference(context, FetchDataConstants.IVR_NUMBER, number);
    }

    public static void writeSTime(Context context, int stime){
        Util.writeSipPreference(context,
                FetchDataConstants.S_TIME,
                stime);
    }

    public static String getCallbackDtmf(Context context) {
        SharedPreferences sp =
                context.getSharedPreferences(FetchDataConstants.SP_SIP_INFO, Context.MODE_PRIVATE);
        return sp.getString(FetchDataConstants.DTMF_SEQ, "");
    }

    public static String getCallbackIvrNumber(Context context) {
        SharedPreferences  sp =
                context.getSharedPreferences(FetchDataConstants.SP_SIP_INFO, Context.MODE_PRIVATE);
        return sp.getString(FetchDataConstants.IVR_NUMBER, "");
    }

    private static long getLastUpdateIVRs(Context context){
        SharedPreferences  sp =
                context.getSharedPreferences(PreferencesConstants.DIALER_PREF, Context.MODE_PRIVATE);
        return sp.getLong(FetchDataConstants.LAST_UPDATE_IVR_LIST, 0);
    }

    public static boolean isNeedUpdateIVRs(Context context){
        return  System.currentTimeMillis() - getLastUpdateIVRs(context) > THREE_HOURS_IN_MILLIS;
    }

    public static void setLastUpdateIVRs(Context context, long time){
        SharedPreferences  sp =
                context.getSharedPreferences(PreferencesConstants.DIALER_PREF, Context.MODE_PRIVATE);
        sp.edit().putLong(FetchDataConstants.LAST_UPDATE_IVR_LIST, time).apply();
    }



}
