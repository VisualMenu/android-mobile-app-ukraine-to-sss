package com.visumenu.clientmenu.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visumenu.clientmenu.R;

/**
 * Created by user on 09.03.16.
 */

public class ViewHolder extends RecyclerView.ViewHolder {
    ImageView ivIcon;
    TextView tvName;
    TextView tvNumber;
    TextView tvMenuItemName;
    ImageView ivState;
    LinearLayout llParents;
    ImageView ivStar;

    public ViewHolder(View itemView) {
        super(itemView);

//        ivIcon = (ImageView) itemView.findViewById(R.id.ivIcon);
        tvName = (TextView) itemView.findViewById(R.id.tvName);
        tvNumber = (TextView) itemView.findViewById(R.id.tvNumber);
        tvMenuItemName = (TextView) itemView.findViewById(R.id.tvMenuItemName);
        ivStar = (ImageView) itemView.findViewById(R.id.ivStar);
        ivState = (ImageView) itemView.findViewById(R.id.ivState);
        llParents = (LinearLayout) itemView.findViewById(R.id.llParents);
    }
}
