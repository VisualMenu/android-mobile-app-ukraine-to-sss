package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.visumenu.clientmenu.R;

/**
 * Created by user on 19.04.16.
 */
public class PagerCompaniesAdapter extends BasePagerAdapter {

    private String query;

    public PagerCompaniesAdapter(Context context, String[] tabTitles) {
        super(context, tabTitles);

    }

    @Override
    protected void onDestroyItem(int position, Object object) {

    }

    @Override
    protected RecyclerView.Adapter createAdapter(int position) {
        switch (position){
            case 0:
                CompaniesAdapter companiesAdapter =
                        new CompaniesAdapter(context, R.layout.companies_list_item);
                if(query != null && query.length() > 0)
                    companiesAdapter.searchCompanies(query);
                return companiesAdapter;
            case 1:
                return new RecentAdapter(context, R.layout.recent_item);
            case 2:
                return new FavoritesAdapter(context, R.layout.recent_item);
        }

        return null;
    }

    public void setQuery(String query) {
        this.query = query;
    }
}
