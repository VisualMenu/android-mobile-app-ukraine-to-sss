package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import com.visumenu.clientmenu.DialerActivity_;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.IVR;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmResults;

/**
 * Created by user on 21.06.16.
 */
public class SearchAdapter extends FilterableRealmBaseAdapter {

    final String TAG = "SearchAdapter";

    Database database;
    LayoutInflater inflater;
    AutoCompleteTextView avCompanies;
    int layout;
    String previousQuery;
    Context context;

    public SearchAdapter(Context context, @LayoutRes int layout, RealmResults realmObjectList, AutoCompleteTextView avCompanies) {
        super(context, layout, realmObjectList, avCompanies);
        database = Database.getInstance(context);
        this.layout = layout;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.avCompanies = avCompanies;

        avCompanies.setText(avCompanies.getText());
    }

    @Override
    protected List performRealmFiltering(@NonNull CharSequence constraint, RealmResults results) {
        String query = constraint.toString().replaceAll("\n", "");
        Log.d(TAG, "performRealmFiltering " + query);
        if(query != null && query.length() > 2) {
            if(!query.equals(previousQuery)) {
                ((DialerActivity_) context).sendSearchRequest(query);
                previousQuery = query;
            }

            return database.getSearchResultIVRs();
        }

        return new ArrayList<IVR>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null)
            convertView = inflater.inflate(layout, parent, false);

        ((TextView) convertView).setText(((IVR) getItem(position)).getIvrName());
        return convertView;
    }
}
