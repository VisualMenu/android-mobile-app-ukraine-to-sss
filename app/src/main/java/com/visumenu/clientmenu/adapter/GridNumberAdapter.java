package com.visumenu.clientmenu.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.visumenu.clientmenu.R;

import java.util.List;

/**
 * Created by user on 03.05.16.
 */
public class GridNumberAdapter extends RecyclerView.Adapter<NumberViewHolder> {

    private static final int TYPE_BOTTOM_VIEWS = 1;
    private static final int TYPE_NUMPAD = 2;
    List<String> items;
    Context mContext;
    int layoutRes;
    float textSize;
    int textColor;
    OnDialerButtonClickListener mNumClickListener;
    OnCallClickListener mCallClickListener;
    OnVoicemailButtonClickListener mVoicemailButtonClickListener;
    Typeface tf;
    int viewHorizontalPadding, viewVerticalPadding;
    LayoutInflater li;
    int screenWidth;
    EditText etPhoneNumber;
    final String TAG = "GridNumberAdapter";

    public GridNumberAdapter(Context context, List<String> items, int resource, EditText etPhoneNumber) {
        this.items = items;
        this.etPhoneNumber = etPhoneNumber;
        mContext = context;
        layoutRes = resource;
        textSize = mContext.getResources().getDimension(R.dimen.dialer_text_size);
        viewHorizontalPadding =
                (int) mContext.getResources().getDimension(R.dimen.dialer_item_horizontal_padding);
        viewVerticalPadding =
                (int) mContext.getResources().getDimension(R.dimen.dialer_item_vert_padding);
        textColor = mContext.getResources().getColor(R.color.dialerTextColor);
        li = LayoutInflater.from(mContext);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        ((Activity) mContext).getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenWidth = displaymetrics.widthPixels;
        tf = Typeface.createFromAsset(mContext.getAssets(), "Geometria-Light.otf");
    }

    @Override
    public NumberViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = li.inflate(layoutRes, parent, false);
        NumberViewHolder holder = new NumberViewHolder(view);
        if(viewType == TYPE_NUMPAD)
            holder.ivNumber.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mNumClickListener.onButtonClick(((String) view.getTag()));
                }
            });

        holder.ivNumber.getLayoutParams().width = (int) (screenWidth / 3.2);
        holder.ivNumber.getLayoutParams().height = screenWidth / 5;
        holder.tvNumber.setTypeface(tf);
        return holder;
    }

    @Override
    public void onBindViewHolder(NumberViewHolder holder, int position) {
        TextView tvItem = holder.tvNumber;
        String number = items.get(position);
        tvItem.setText(number);
        holder.ivNumber.setTag(number);
        int gravity;
        if(position % 3 == 0)
            gravity = Gravity.LEFT | Gravity.CENTER_VERTICAL;
        else if((position - 1) % 3 == 0)
            gravity = Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL;
        else
            gravity = Gravity.RIGHT | Gravity.CENTER_VERTICAL;

        switch (position){

            case 10:
                holder.ivNumber.setImageResource(R.drawable.dialer_num_selector);
                String zeroButtonText = tvItem.getText() + " +";
                tvItem.setText(zeroButtonText);
                holder.ivNumber.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        mNumClickListener.onButtonClick("+");
                        return true;
                    }
                });
                break;

            case 12:
                holder.ivNumber.setImageResource(R.drawable.dialer_voice_message_selector);
                holder.ivNumber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mVoicemailButtonClickListener != null)
                            mVoicemailButtonClickListener.onVoicemailButtonClick();
                    }
                });
                break;

            case 13:
                holder.ivNumber.setImageResource(R.drawable.dialer_call_selector);
                holder.ivNumber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "call click");
                        if(mCallClickListener != null)
                            mCallClickListener.onCallClick(etPhoneNumber.getText().toString());
                    }
                });
                break;

            case 14:
                holder.ivNumber.setImageResource(R.drawable.dialer_clear_selector);
                holder.ivNumber.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        removePreviousDigitIfPossible();
                    }
                });
                holder.ivNumber.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {
                        etPhoneNumber.getText().clear();
                        return false;
                    }
                });
                break;

            default:
                holder.ivNumber.setImageResource(R.drawable.dialer_num_selector);
        }

        tvItem.setGravity(gravity);
    }

    private void removePreviousDigitIfPossible() {
        final int currentPosition = etPhoneNumber.getSelectionStart();
        if(etPhoneNumber.getSelectionStart() != etPhoneNumber.getSelectionEnd()){
            etPhoneNumber.getText().delete(etPhoneNumber.getSelectionStart(),
                    etPhoneNumber.getSelectionEnd());
            return;
        }

        if (currentPosition > 0) {
            etPhoneNumber.setSelection(currentPosition);
            etPhoneNumber.getText().delete(currentPosition - 1, currentPosition);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setOnDialerButtonClickListener(OnDialerButtonClickListener listener){
        mNumClickListener = listener;
    }

    public void setOnCallClickListener(OnCallClickListener mCallClickListener) {
        this.mCallClickListener = mCallClickListener;
    }

    public void setOnVoicemailButtonClickListener(
            OnVoicemailButtonClickListener onVoicemailButtonClickListener
    ) {
        this.mVoicemailButtonClickListener = onVoicemailButtonClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        if(position < 12)
            return TYPE_NUMPAD;

        return TYPE_BOTTOM_VIEWS;
    }
}

