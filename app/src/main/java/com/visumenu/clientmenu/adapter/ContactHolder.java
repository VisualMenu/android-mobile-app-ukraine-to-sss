package com.visumenu.clientmenu.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.visumenu.clientmenu.R;

/**
 * Created by user on 15.07.16.
 */
public class ContactHolder extends RecyclerView.ViewHolder {

    TextView tvName;
    ImageView ivPhoto;
    ImageView ivStar;
    TextView tvGroupName;

    public ContactHolder(View itemView) {
        super(itemView);
        tvName = (TextView) itemView.findViewById(R.id.tvName);
        ivPhoto = (ImageView) itemView.findViewById(R.id.ivPhoto);
        ivStar = (ImageView) itemView.findViewById(R.id.ivStar);
        tvGroupName = (TextView) itemView.findViewById(R.id.tvGroupName);
    }
}
