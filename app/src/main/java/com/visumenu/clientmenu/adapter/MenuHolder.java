package com.visumenu.clientmenu.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.visumenu.clientmenu.R;

/**
 * Created by user on 14.03.16.
 */
public class MenuHolder extends RecyclerView.ViewHolder {
    TextView tvName;
    TextView tvDescriptioin;
    TextView tvTopInfo;
    ImageView ivItemState;
    ImageView ivStar;

    public MenuHolder(View itemView) {

        super(itemView);
        tvName = (TextView) itemView.findViewById(R.id.tvName);
        tvDescriptioin = (TextView) itemView.findViewById(R.id.tvNumber);
        tvTopInfo = (TextView) itemView.findViewById(R.id.tvTopInfo);
        ivItemState = (ImageView) itemView.findViewById(R.id.ivItemState);
        ivStar = (ImageView) itemView.findViewById(R.id.ivStar);
    }
}
