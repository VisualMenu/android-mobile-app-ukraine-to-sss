package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.List;

import io.realm.RealmChangeListener;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by user on 21.06.16.
 */
public abstract class FilterableRealmBaseAdapter<T extends RealmObject> extends ArrayAdapter<T> implements Filterable {

    private RealmResults<T> mRealmObjectList;
    private List<T> mResults;
     AutoCompleteTextView avCompanies;
    final String TAG = "FilterableRealmAdapter";

    private RealmChangeListener mListener = new RealmChangeListener() {

        @Override
        public void onChange(Object element) {
            Log.d(TAG, "onChange()");
            mResults = mRealmObjectList;
            avCompanies.setText(avCompanies.getText());
            avCompanies.setSelection(avCompanies.length());
            notifyDataSetChanged();
        }
    };

    public FilterableRealmBaseAdapter(Context context, @LayoutRes int layout, RealmResults<T> realmObjectList, AutoCompleteTextView avCompanies) {
        super(context, layout);
        mRealmObjectList = realmObjectList;
        this.avCompanies= avCompanies;
        mRealmObjectList.addChangeListener(mListener);

    }

    @Override
    public int getCount() {
        return mResults == null ? 0 : mResults.size();
    }

    @Override
    public T getItem(int position) {
        return mResults == null ? null : mResults.get(position);
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            private boolean mHasResults = false;

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults results = new FilterResults();
                results.count = mHasResults ? 1 : 0;
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                // back on the main thread, we can do the query and notify
                if (constraint != null) {
                    mResults = mRealmObjectList;
                    performRealmFiltering(constraint, mRealmObjectList);
                    mHasResults = mResults.size() > 0;
                    notifyDataSetChanged();
                }
            }
        };
    }

    protected abstract List<T> performRealmFiltering(@NonNull CharSequence constraint, RealmResults<T> results);
}
