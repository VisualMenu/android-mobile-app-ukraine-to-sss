package com.visumenu.clientmenu.adapter;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.visumenu.clientmenu.DialerActivity_;
import com.visumenu.clientmenu.R;

import java.util.List;
import java.util.Set;

/**
 * Created by user on 19.07.16.
 */
public class FavoritesContactsAdapter extends ContactsAdapter {

    public FavoritesContactsAdapter(Context context, Cursor cursor, Set<String> contactIds) {
        super(context, cursor);

        favorites = contactIds;
    }

    @Override
    public void onBindViewHolder(ContactHolder viewHolder, Cursor cursor) {
        View container = ((ViewGroup) viewHolder.itemView).getChildAt(1);
        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contactId = (String) v.getTag();
                List<String> numbers = getPhoneNumbersById(contactId);
                if(numbers != null)
                    ((DialerActivity_) getContext()).performDialWithCheckPermission(numbers.get(0));
            }
        });
        if (cursor.getPosition() % 2 == 0)
            viewHolder.itemView.setBackgroundResource(R.color.bgListItemLight);
        else
            viewHolder.itemView.setBackgroundResource(R.color.bgListItemDark);
        String name = cursor.getString(columnIndexName);
        viewHolder.tvGroupName.setVisibility(View.GONE);
        viewHolder.tvName.setTypeface(contentFont);
        viewHolder.tvName.setText(name);
        String id = cursor.getString(columnIndexID);
        container.setTag(id);
        String photoUri = cursor.getString(columnThumbnail);
        if (photoUri != null)
            viewHolder.ivPhoto.setImageURI(Uri.parse(photoUri));
        else
            viewHolder.ivPhoto.setImageResource(R.drawable.contact_image);
        viewHolder.ivStar.setImageResource(R.drawable.ic_star_green_pressed);
        viewHolder.ivStar.setVisibility(View.GONE);
        viewHolder.ivStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contactId = (String) ((RelativeLayout) v.getParent()).getTag();
                favorites.remove(contactId);
                database.removeContactFromFavorites(contactId);
                swapCursor(initCursor(getContext(), favorites));
                Log.d(TAG, "like click " + contactId);
            }
        });
    }

    public static Cursor initCursor(Context context, Set<String> favorites){
        ContentResolver cr = context.getContentResolver();
        StringBuilder sb = new StringBuilder();
        if(favorites.size() == 0)
            return null;
        for (String favorite : favorites){
            sb.append(favorite);
            sb.append(",");
        }
        sb.deleteCharAt(sb.length() - 1);
        String where = ContactsContract.Contacts.HAS_PHONE_NUMBER + " > 0 AND " +
                ContactsContract.Contacts._ID + " IN (" + sb.toString() + ")";
        Log.d(TAG, "init cursor where " + where);
        return cr.query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                where,
                null,
                ContactsContract.Contacts.DISPLAY_NAME
        );
    }

    @Override
    protected void initFavorites() {

    }
}
