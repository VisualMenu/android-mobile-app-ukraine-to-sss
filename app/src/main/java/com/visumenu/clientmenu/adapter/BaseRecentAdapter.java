package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.RecentDtmf;
import com.visumenu.clientmenu.service.FetchDataConstants;

import java.util.List;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Adapter for Recent list
 */

public abstract class BaseRecentAdapter extends IVRCallAdapter<ViewHolder> {

    final String TAG = "BaseRecentAdapter";

    private long maxParentsCount = 0;
    protected List<RecentDtmf> mRecent;
    int layout;
    Typeface font;
    int defaultTextColor, favoritesTextColor;
    protected int lightItemColor, darkItemColor;

    private RealmChangeListener mListener = new RealmChangeListener() {
        @Override
        public void onChange(Object element) {
            Log.d(TAG, "onChange()");
            setMaxParentsCount();
            notifyDataSetChanged();
        }
    };

    public BaseRecentAdapter(Context context, int resId) {
        setContext(context);
        mRecent = getData();
        setMaxParentsCount();
        Log.d(TAG, "recent size " + mRecent.size());
        layout = resId;
        ((RealmResults<RecentDtmf>) mRecent).addChangeListener(mListener);
        font = Typeface.createFromAsset(mContext.getAssets(), "OpenSans-Regular.ttf");
        Resources res = mContext.getResources();
        defaultTextColor = res.getColor(R.color.list_text_color);
        favoritesTextColor = res.getColor(R.color.favorites_text_color);
        lightItemColor = res.getColor(R.color.mainMenuItemLight);
        darkItemColor = res.getColor(R.color.mainMenuItemDark);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View companyItem = LayoutInflater.from(mContext).inflate(layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(companyItem);
        viewHolder.tvName.setTypeface(font);
        for (int i = 0; i < maxParentsCount; i++)
            viewHolder.llParents.addView(new TextView(mContext),
                    new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT));

        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return mRecent.size();
    }


    public RecentDtmf getItem(int position){
        return mRecent.get(position);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        ((RealmResults) mRecent).removeChangeListeners();
        super.onDetachedFromRecyclerView(recyclerView);
    }

    public abstract List<RecentDtmf> getData();

    protected void setStateIcon(ViewHolder holder, String subType){
        if(FetchDataConstants.TYPE_AGENT.equals(subType)){
            holder.ivState.setImageResource(R.drawable.ic_call_icon);
        }else if(FetchDataConstants.TYPE_INPUT.equals(subType)){
            holder.ivState.setImageResource(R.drawable.ic_info_icon);
        }else
            holder.ivState.setImageResource(R.drawable.ic_call_icon_no_agent);
    }

    void setMaxParentsCount(){
        maxParentsCount = Database.getInstance(mContext).getMaxParentRecentCount(mRecent);
    }
}