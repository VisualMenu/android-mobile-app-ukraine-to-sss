package com.visumenu.clientmenu.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.visumenu.clientmenu.R;

/**
 * Created by user on 14.07.16.
 */
public class CallsHistoryHolder extends RecyclerView.ViewHolder {

    TextView tvName;
    TextView tvDateTime;
    ImageView ivPhoto;

    public CallsHistoryHolder(View itemView) {
        super(itemView);
        tvName = (TextView) itemView.findViewById(R.id.tvName);
        tvDateTime = (TextView) itemView.findViewById(R.id.tvDateTime);
        ivPhoto = (ImageView) itemView.findViewById(R.id.ivPhoto);
    }
}
