package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by user on 19.04.16.
 */
public abstract class BasePagerAdapter extends PagerAdapter {

    final String TAG = "ViewPagerAdapter";

    public static final int COUNT_TABS = 3;
    Context context;
    String[] tabTitles;
    RecyclerView[] recyclerViews = new RecyclerView[3];

    public BasePagerAdapter(Context context, String[] tabTitles) {
        this.context = context;
        this.tabTitles = tabTitles;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Log.d(TAG, "instantiateItem " + position);
        RecyclerView rv = new RecyclerView(context);
        rv.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(context);
        rv.setLayoutManager(mLayoutManager);
//        rv.setBackgroundResource(android.R.color.darker_gray);
        recyclerViews[position] = rv;
        RecyclerView.Adapter adapter = createAdapter(position);
        rv.setAdapter(adapter);
        container.addView(rv);
        rv.setTag(position);
        return rv;
    }

    public RecyclerView.Adapter getAdapter(int position){
        RecyclerView rv = recyclerViews[position];
        if(rv == null)
            return null;
        return rv.getAdapter();
    }

    @Override
    public int getCount() {
        return COUNT_TABS;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        onDestroyItem(position, object);
    }

    protected abstract void onDestroyItem(int position, Object object);


    protected abstract RecyclerView.Adapter createAdapter(int position);


    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
