package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Utils.CallUtils;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.ParentDtmfItem;
import com.visumenu.clientmenu.model.RecentDtmf;
import com.visumenu.clientmenu.model.User;

import java.util.List;

/**
 * Created by user on 01.06.16.
 */
public abstract class IVRCallAdapter<T extends RecyclerView.ViewHolder>
        extends RecyclerView.Adapter<T>{

    protected Context mContext;

    final String TAG = "IVRCallAdapter";

    long lastClickTime = 0;

    private static final int CLICK_INTERVAL = 500;

    protected void call(RecentDtmf recentDtmf){

        showCallDialog(
                recentDtmf.getIVRNumber(),
                recentDtmf.getCompanyName(),
                recentDtmf.getDtmf(),
                recentDtmf.getName(),
                recentDtmf.getStime(),
                recentDtmf.getSubType(),
                recentDtmf.getParentItems()
        );
    }

    protected void call(String ivrNumber, String companyName, String dtmf,
                        String name, String sTime, String subType, List<ParentDtmfItem> parentItems){
        showCallDialog(ivrNumber, companyName, dtmf, name, sTime, subType, parentItems);
    }

    private void showCallDialog(final String ivrNumber, final String companyName,
                                final String dtmf, final String name, final String sTime,
                                final String subtype, final List<ParentDtmfItem> parentItems){

        Database database = Database.getInstance(mContext);
        User user = database.getUser();
        String phoneNumberUnchecked = (user.getPhoneNumber() == null ?
                CallUtils.getPhoneNumber(mContext.getApplicationContext()) :
                user.getPhoneNumber());

        if(phoneNumberUnchecked == null){
            Toast.makeText(mContext, "You don't have phone number", Toast.LENGTH_SHORT).show();
            return;
        }

        final String phoneNumber = ((phoneNumberUnchecked.startsWith("380")) ? "1002" : "") +
                phoneNumberUnchecked;
        Log.d(TAG, "calling to " + ivrNumber + " with dtmf " + dtmf +
                " callback phonenumber " + phoneNumber);
        new MaterialDialog.Builder(mContext)
                .title(R.string.call)
                .content(mContext.getString(R.string.call_message) + " "
                        + companyName + " "
                        + mContext.getString(R.string.with_number) + " "
                        + ivrNumber + "?"
                        + "\n Agent will call back to " + phoneNumber)
                .positiveText(R.string.call)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        Database.getInstance(mContext).addRecentDtmf(ivrNumber,
                                companyName, name, dtmf, sTime, subtype, parentItems);
//                        if(!BuildConfig.DEBUG)
                        ((IvrActivity)mContext)
                                .callBack(ivrNumber, dtmf, sTime, phoneNumber, subtype);
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .negativeText(R.string.cancel)
                .show();
    }

    protected void updateClickTime(){
        lastClickTime = System.currentTimeMillis();
    }

    protected boolean enableClick(){
        return System.currentTimeMillis() - lastClickTime > CLICK_INTERVAL;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    public Context getContext() {
        return mContext;
    }
}
