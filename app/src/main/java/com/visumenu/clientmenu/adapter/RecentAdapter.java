package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Util;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.ParentDtmfItem;
import com.visumenu.clientmenu.model.RecentDtmf;

import java.util.List;

/**
 * Created by user on 20.04.16.
 */
public class RecentAdapter extends BaseRecentAdapter {

    public RecentAdapter(Context context, int resId) {
        super(context, resId);
    }

    @Override
    public List<RecentDtmf> getData() {
        return Database.getInstance(mContext.getApplicationContext()).getAllRecentDtmf();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final RecentDtmf recentDtmf = mRecent.get(position);
        holder.tvName.setText(recentDtmf.getCompanyName());
        holder.tvNumber.setText(recentDtmf.getIVRNumber());

        holder.itemView.setBackgroundColor(position % 2 == 0 ? lightItemColor : darkItemColor);
        String menuItemName = recentDtmf.getName();
        if(menuItemName == null || menuItemName.length() == 0){
            holder.tvMenuItemName.setVisibility(View.GONE);
        }else{
            holder.tvMenuItemName.setVisibility(View.VISIBLE);
            holder.tvMenuItemName.setText(menuItemName);
        }

        setStateIcon(holder, recentDtmf.getSubType());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!enableClick())
                    return;
                Database.getInstance(mContext)
                        .addStatItem(recentDtmf.getIVRNumber(),
                                recentDtmf.getDtmf(),
                                recentDtmf.getStime());
                Log.d(TAG, "setSendStatDataTime");
                Util.setSendStatDataTime(mContext);

                call(recentDtmf);
                updateClickTime();
            }
        });

        holder.ivStar.setImageResource(
                recentDtmf.isFavorites() ? R.drawable.ic_star_green_pressed : R.drawable.ic_star_green
        );

        holder.ivStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!enableClick())
                    return;

                ImageView ivStar = (ImageView) view;
                boolean liked = recentDtmf.isFavorites();
                ivStar.setImageResource(liked ? R.drawable.ic_star_green : R.drawable.ic_star_green_pressed);
                Database database = Database.getInstance(mContext);
                database.addFavoritesDtmf(recentDtmf);
                ((IvrActivity) mContext).sendLike(recentDtmf, !liked);
                updateClickTime();
            }
        });

        List<ParentDtmfItem> parentItems = recentDtmf.getParentItems();

        Log.d(TAG, "onBindHolder parent items " +
                (parentItems == null ? "null" : "size " + parentItems.size()));

        if(parentItems == null || parentItems.size() == 0){
            holder.llParents.setVisibility(View.GONE);
            return;
        }

        holder.llParents.setVisibility(View.VISIBLE);

        setContent(holder, parentItems);
    }

//    private void addTextViews(ViewHolder holder, String[] parentItems){
//        for (int i = 0; i < parentItems.length; i++){
//            TextView textView = new TextView(mContext);
//            textView.setPadding(30 * i, 0, 0, 0);
//            textView.setText(parentItems[i]);
//            holder.llParents.addView(textView);
//        }
//    }

    private void setContent(ViewHolder holder, List<ParentDtmfItem> parentItems){
        Log.d(TAG, "setContent " + parentItems.size());

        for (int i = 0; i < parentItems.size(); i++){
            ParentDtmfItem parentDtmfItem = parentItems.get(i);
            Log.d(TAG, "setContent " + parentDtmfItem.getItemName());
            TextView textView = ((TextView) holder.llParents.getChildAt(i));
            textView.setText(parentDtmfItem.getItemName());
            textView.setPadding(parentDtmfItem.getLevel() * 30, 0, 0, 0);
        }
    }
}
