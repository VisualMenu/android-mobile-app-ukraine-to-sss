package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Utils.DtmfItemList;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.IVRMenuItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by user on 15.03.16.
 */
public class MenuFlatAdapter extends MenuAdapter{

    Set<String> openedChild = new HashSet<>();

    List<IVRMenuItem> itemsWithoutChild;
    HashMap<String, DtmfItemList> dtmfMap;
    List<String> headers;
    int colorBlueDark, colorBlueLight;

    public MenuFlatAdapter(Context context, String ivrNumber, int resId,
                           List<IVRMenuItem> list, Set<String> openedChild) {
        super(context, ivrNumber, resId);

        if(list != null)
            items = list;

        if(openedChild != null)
            this.openedChild = openedChild;

        Log.d(TAG, "list size " + ivr.getMenuItems().size());
        initItems();
        colorBlueDark = context.getResources().getColor(R.color.menuDarkBlue);
        colorBlueLight = context.getResources().getColor(R.color.menuLightBlue);
        Log.d(TAG, "dtmfMap size " + dtmfMap.size());
    }

    void initItems(){
        dtmfMap = Database.listToDtmfMap(ivr.getMenuItems());
        itemsWithoutChild = Database.getItemsWithoutChild(dtmfMap);
        headers = new ArrayList<>();

        if(itemsWithoutChild.size() > 2)
            Log.d(TAG, "first " + itemsWithoutChild.get(0).getOrder() +
                    " second " + itemsWithoutChild.get(1).getOrder());
        for(IVRMenuItem ivrMenuItem : itemsWithoutChild){
            String dtmf = ivrMenuItem.getDtmf();
            StringBuilder builder = new StringBuilder();
            int len = dtmf.length() - 1;
            for(int i = len; i > 0; i--){
                builder.insert(0,
                        ((DtmfItemList) dtmfMap.get(dtmf.substring(0, i)))
                                .getParentItem().getName() +
                                (i == len ? "\n" : " -> \n"));
            }
            builder.insert(0, "(\n").insert(builder.length(), ")");
            headers.add(builder.toString());
        }
    }

    @Override
    public void onBindViewHolder(MenuHolder holder, int position) {
        IVRMenuItem ivrMenuItem = getItem(position);
        holder.tvName.setText(ivrMenuItem.getName());
        holder.itemView.setTag(position);
//        holder.tvTopInfo.setText(headers.get(position));
//        holder.itemView.setPadding(ivrMenuItem.getDtmf().length() * 50, 0, 0, 0);
        holder.itemView.setBackgroundColor(position % 2 == 0 ? colorBlueDark : colorBlueLight);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick");
                int position = (Integer) v.getTag();
                IVRMenuItem ivrMenuItem = itemsWithoutChild.get(position);
                String dtmf = ivrMenuItem.getDtmf();
                String stime = ivrMenuItem.getIvrMenuStime();
//                if(dtmfMap.get(dtmf + ":" + stime).size() == 0)
//                    call(dtmf, ivrMenuItem.getName(), stime, ivrMenuItem.getSubtype());
//                updateMenu(position, dtmf);
            }
        });
    }

    @Override
    public int getItemCount() {
        return itemsWithoutChild.size();
    }

    @Override
    public IVRMenuItem getItem(int position){
        return itemsWithoutChild.get(position);
    }


    @Override
    void onChangeData() {
        initItems();
        notifyDataSetChanged();
    }

    public Set<String> getOpenedChild(){
        return openedChild;
    }
}
