package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Utils.CallUtils;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.IVR;
import com.visumenu.clientmenu.model.IVRMenuItem;
import com.visumenu.clientmenu.model.ParentDtmfItem;
import com.visumenu.clientmenu.model.User;
import com.visumenu.clientmenu.service.FetchDataConstants;

import java.util.List;

import io.realm.RealmChangeListener;

/**
 * Created by user on 15.03.16.
 */
public abstract class MenuAdapter extends RecyclerView.Adapter<MenuHolder> {

    final String TAG = "MenuAdapter";

    List<IVRMenuItem> items;
    protected IVR ivr;
    int layout;
    Context mContext;
    String ivrNumber;
    Typeface font;

    private RealmChangeListener mListener = new RealmChangeListener() {
        @Override
        public void onChange(Object element) {
            Log.d(TAG, "onChange");
            items = Database.getInstance(mContext).getIVRMenuItems(ivrNumber, "", "");
            if(items.size() > 0)
                ((IvrActivity)mContext).dismissProgressDialog();
            onChangeData();
        }
    };

    public MenuAdapter(Context context, String ivrNumber, int resId) {
        ivr = Database.getInstance(context).findIVRbyNumber(ivrNumber);
        items = Database.getInstance(mContext).getIVRMenuItems(ivrNumber, "", "");
        this.ivrNumber = ivrNumber;
        ivr.addChangeListener(mListener);
        layout = resId;
        mContext = context;
        font = Typeface.createFromAsset(mContext.getAssets(), "MyriadPro-Regular.otf");
        if(items.size() == 0)
            ((IvrActivity)mContext).showWaitDialog();
    }

    @Override
    public MenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View companyItem = LayoutInflater.from(mContext).inflate(layout, parent, false);
        MenuHolder menuHolder = new MenuHolder(companyItem);
        menuHolder.tvName.setTypeface(font);
        return menuHolder;
    }

    @Override
    public void onBindViewHolder(MenuHolder holder, final int position) {
        holder.tvName.setText(getItem(position).getName());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public IVRMenuItem getItem(int position){
        return items.get(position);
    }

    void call(IVRMenuItem ivrMenuItem, List<ParentDtmfItem> parentNames){
        call(ivrMenuItem.getDtmf(), ivrMenuItem.getName(), ivrMenuItem.getIvrMenuStime(),
                ivrMenuItem.getSubtype(), parentNames);
    }

    void call(String dtmf, String name, String sTime, String subType, List<ParentDtmfItem> parentNames){
        showCallDialog(dtmf, name, sTime, subType, parentNames);
    }

    void call(String dtmf, String stime, List<ParentDtmfItem> parentNames){
        IVRMenuItem menuItem = Database.getMenuItemByDtmfStime(ivr, dtmf, stime);
        call(menuItem.getDtmf(), menuItem.getName(), menuItem.getIvrMenuStime(),
                menuItem.getSubtype(), parentNames);
    }

    private void showCallDialog(final String dtmf, final String name, final String sTime,
                                final String subType, final List<ParentDtmfItem> parentItems){
        Database database = Database.getInstance(mContext);
        User user = database.getUser();
        String phoneNumberUnchecked = (user.getPhoneNumber() == null ?
                CallUtils.getPhoneNumber(mContext.getApplicationContext()) :
                user.getPhoneNumber());

        if(phoneNumberUnchecked == null){
            Toast.makeText(mContext, "You don't have phone number", Toast.LENGTH_SHORT).show();
            return;
        }

        final String phoneNumber = ((phoneNumberUnchecked.startsWith("380")) ? "1002" : "") +
                phoneNumberUnchecked;
        Log.d(TAG, "calling to " + ivr.getIvrNumber() + " with dtmf " + dtmf +
                " callback phonenumber " + phoneNumber);
        String callbackMessage;

        if(!FetchDataConstants.TYPE_INPUT.equals(subType))
            callbackMessage = "\n Agent will call back to " + phoneNumber;
        else
            callbackMessage = "";

        new MaterialDialog.Builder(mContext)
                .title(R.string.call)
                .content(mContext.getString(R.string.call_message) + " "
                        + ivr.getIvrName() + " "
                        + mContext.getString(R.string.with_number) + " "
                        + ivr.getIvrNumber() + "?"
                + callbackMessage)
                .positiveText(R.string.call)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        Database.getInstance(mContext).addRecentDtmf(ivr.getIvrNumber(),
                                ivr.getIvrName(), name, dtmf, sTime, subType, parentItems);
//                        if(!BuildConfig.DEBUG)
                            ((IvrActivity) mContext)
                                    .callBack(ivr.getIvrNumber(), dtmf, sTime, phoneNumber, subType);
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog dialog, DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .negativeText(R.string.cancel)
                .show();
    }

    abstract void onChangeData();

    public List<IVRMenuItem> getItems(){
        return items;
    }

    public IVR getIvr() {
        return ivr;
    }
}


