package com.visumenu.clientmenu.adapter;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.visumenu.clientmenu.DialerActivity_;
import com.visumenu.clientmenu.R;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by user on 14.07.16.
 */
public class CallsHistoryAdapter extends RecyclerViewCursorAdapter<CallsHistoryHolder> {

    int numberColumnIndex;
    int nameColumnIndex;
    int dateColumnIndex;
    int durationColumnIndex;
    int contactIdColumnIndex;
    LayoutInflater inflater;
    boolean underLollipop = Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP;

    Typeface contentFont;

    HashMap<String, Bitmap> numbersPhotoMap;
    ContentResolver cr;
    private static final String[] PHOTO_BITMAP_PROJECTION = new String[] {
            ContactsContract.CommonDataKinds.Photo.PHOTO
    };

    public CallsHistoryAdapter(Context context, Cursor cursor) {
        super(context, cursor);
        inflater = LayoutInflater.from(context);
        numberColumnIndex = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        nameColumnIndex = cursor.getColumnIndex(CallLog.Calls.CACHED_NAME);
        dateColumnIndex = cursor.getColumnIndex(CallLog.Calls.DATE);
        durationColumnIndex = cursor.getColumnIndex(CallLog.Calls.DURATION);
        contentFont     = Typeface.createFromAsset(context.getAssets(), "MyriadPro-Regular.otf");
        numbersPhotoMap = new HashMap<>();
        if (underLollipop) {
            cr = context.getContentResolver();
        } else {
            contactIdColumnIndex = cursor.getColumnIndex(CallLog.Calls.CACHED_PHOTO_ID);
        }

    }

    @Override
    public void onBindViewHolder(CallsHistoryHolder viewHolder, Cursor cursor) {
        String number = cursor.getString(numberColumnIndex);
        String name = cursor.getString(nameColumnIndex);
        viewHolder.tvName.setTypeface(contentFont);
        viewHolder.tvDateTime.setTypeface(contentFont);
        viewHolder.tvName.setText(name == null || name.length() == 0 ? number : name);
        String date = cursor.getString(dateColumnIndex);
        SimpleDateFormat formatter = new SimpleDateFormat(
                "dd-MMM-yyyy HH:mm", Locale.ENGLISH);
        String dateString = formatter.format(new Date(Long
                .parseLong(date)));
        viewHolder.tvDateTime.setText(dateString);
        if (cursor.getPosition() % 2 == 0)
            viewHolder.itemView.setBackgroundResource(R.color.bgListItemLight);
        else
            viewHolder.itemView.setBackgroundResource(R.color.bgListItemDark);
        viewHolder.itemView.setTag(number);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String phoneNumber = (String) v.getTag();
                ((DialerActivity_) getContext()).performDialWithCheckPermission(phoneNumber);
            }
        });


        Bitmap bitmap;
        if(numbersPhotoMap.containsKey(number)){
            bitmap = numbersPhotoMap.get(number);
        } else {
            bitmap = underLollipop ?
                    retrieveContactPhoto(number) :
                    fetchThumbnail(cursor.getInt(contactIdColumnIndex));

            numbersPhotoMap.put(number, bitmap);
        }

        setContactPhoto(viewHolder.ivPhoto, bitmap);
    }

    private void setContactPhoto(ImageView ivPhoto, Bitmap bitmap){
        if (bitmap == null)
            ivPhoto.setImageResource(R.drawable.contact_image);
        else
            ivPhoto.setImageBitmap(bitmap);
    }

    @Override
    public CallsHistoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new CallsHistoryHolder(inflater.inflate(R.layout.calls_history_item, parent, false));
    }

    final Bitmap fetchThumbnail(final int thumbnailId) {

        final Uri uri = ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI, thumbnailId);
        final Cursor cursor = getContext().getContentResolver().query(uri, PHOTO_BITMAP_PROJECTION, null, null, null);

        try {
            Bitmap thumbnail = null;
            if (cursor.moveToFirst()) {
                final byte[] thumbnailBytes = cursor.getBlob(0);
                if (thumbnailBytes != null) {
                    thumbnail = BitmapFactory.decodeByteArray(thumbnailBytes, 0, thumbnailBytes.length);
                }
            }
            return thumbnail;
        }
        finally {
            cursor.close();
        }
    }

    //for under lollipop
    public Bitmap retrieveContactPhoto(String number) {
        String contactId = null;
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));

        String[] projection = new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID};

        Cursor cursor = cr.query(
                        uri,
                        projection,
                        null,
                        null,
                        null
                );

        if (cursor != null) {
            while (cursor.moveToNext()) {
                contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
            }
            cursor.close();
        }

        Bitmap photo = null;

        try {
            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getContext().getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, Long.parseLong(contactId)));

            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
            }

            assert inputStream != null;
            if (inputStream != null)
                inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return photo;
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        for (Map.Entry<String, Bitmap> entry : numbersPhotoMap.entrySet()){
            Bitmap bitmap = entry.getValue();
            if(bitmap != null && !bitmap.isRecycled())
                bitmap.recycle();
        }
        super.onDetachedFromRecyclerView(recyclerView);
    }
}
