package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.IVR;

import java.util.List;

/**
 * Created by user on 20.06.16.
 */
public class SearchPopupAdapter extends ArrayAdapter<IVR> {

    final String TAG = "SearchPopupAdapter";
    int res;
    List<IVR> items;
    Database database;
    int lastSize;

    LayoutInflater inflater;
    public SearchPopupAdapter(Context context, int resource, List<IVR> items) {
        super(context, resource, items);
        this.items = items;
        Log.d(TAG, "size " + items.size());
        lastSize = items.size();
        res = resource;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        database = Database.getInstance(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
            convertView = inflater.inflate(res, parent, false);
        ((TextView) convertView).setText(items.get(position).getIvrName());
        return convertView;
    }

    public boolean search(String query){
        items = database.getIVRs(query);
        notifyDataSetChanged();
        return items.size() > 0;
    }

    @Override
    public int getCount() {
        int newSize = items.size();
        if(newSize != lastSize){
            lastSize = newSize;
            notifyDataSetChanged();
        }
        return newSize;
    }
}
