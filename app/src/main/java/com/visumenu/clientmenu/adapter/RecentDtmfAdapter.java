package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;

import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.RecentDtmf;
import com.visumenu.clientmenu.model.RecentItem;

import java.util.List;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by user on 21.04.16.
 */
public class RecentDtmfAdapter extends MenuAdapter {

    List<RecentDtmf> recentDtmfs;
    List<RecentItem> recentItems;

    private RealmChangeListener mListener = new RealmChangeListener() {
        @Override
        public void onChange(Object element) {
            Log.d(TAG, "onChange()");
            recentDtmfs = Database.getInstance(mContext).getRecentDtmfs(ivrNumber);
            if(recentDtmfs == null)
                return;

            if(recentItems != null){
                ((RealmResults) recentItems).removeChangeListeners();
                recentItems = null;
            }

            notifyDataSetChanged();
        }
    };

    public RecentDtmfAdapter(Context context, String ivrNumber, int resId) {
        super(context, ivrNumber, resId);
        recentDtmfs = Database.getInstance(mContext).getRecentDtmfs(ivrNumber);
        if(recentDtmfs == null){
            recentItems = Database.getInstance(mContext).getAllRecent();
            ((RealmResults) recentItems).addChangeListener(mListener);
        }else
            ((RealmResults<RecentDtmf>) recentDtmfs).addChangeListener(mListener);
    }

    @Override
    public int getItemCount() {
        if(recentDtmfs == null)
            return 0;
        return recentDtmfs.size();
    }

    @Override
    public void onBindViewHolder(MenuHolder holder, final int position) {
        holder.tvName.setText(recentDtmfs.get(position).getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RecentDtmf recentDtmf = recentDtmfs.get(position);
//                call(recentDtmf.getDtmf(), recentDtmf.getStime());
            }
        });
    }

    @Override
    void onChangeData() {

    }
}


