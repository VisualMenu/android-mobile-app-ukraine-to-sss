package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.IVR;
import com.visumenu.clientmenu.model.ParentDtmfItem;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Adapter for companies list
 */

public class CompaniesAdapter extends IVRCallAdapter<ViewHolder> {

    final String TAG = "CompaniesAdapter";
    List<IVR> mCompanies;
    int layout;
    Database database;
    Typeface font;
    boolean search = false;
    int favoritesTextColor, defaultTextColor;
    int lightItemColor, darkItemColor;


    private RealmChangeListener mListener = new RealmChangeListener() {
        @Override
        public void onChange(Object element) {
            Log.d(TAG, "onChange()");
            getCompaniesFromRealm();
            notifyDataSetChanged();
        }
    };

    public CompaniesAdapter(Context context, int resId) {
        Log.d(TAG, "adapter created");
        database = Database.getInstance(context.getApplicationContext());
        mCompanies = database.getPopularIVRs();
        setContext(context);
        if(mCompanies.size() == 0)
            ((IvrActivity) getContext()).showWaitDialog();
        layout = resId;
        ((RealmResults<IVR>) mCompanies).addChangeListener(mListener);
        font = Typeface.createFromAsset(getContext().getAssets(), "MyriadPro-Regular.otf");
        Resources res = context.getResources();

        defaultTextColor = res.getColor(R.color.list_text_color);
        favoritesTextColor = res.getColor(R.color.favorites_text_color);
        lightItemColor = res.getColor(R.color.mainMenuItemLight);
        darkItemColor = res.getColor(R.color.mainMenuItemDark);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View companyItem = LayoutInflater.from(getContext()).inflate(layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(companyItem);
        viewHolder.tvName.setTypeface(font);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        IVR ivrItem = mCompanies.get(position);
        holder.tvName.setText(ivrItem.getIvrName());
//        holder.tvNumber.setText(ivrItem.getIvrNumber());

        if(position % 2 == 0)
            holder.itemView.setBackgroundColor(lightItemColor);
        else
            holder.itemView.setBackgroundColor(darkItemColor);

        holder.ivState.setVisibility(ivrItem.isEmptyMenu() ? View.VISIBLE : View.INVISIBLE);

        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!enableClick())
                    return;

                int position = (Integer) view.getTag();
                IVR ivrItem = mCompanies.get(position);
                if(ivrItem.isEmptyMenu())
                    call(ivrItem.getIvrNumber(), ivrItem.getIvrName(), "", "", "", "",
                            new ArrayList<ParentDtmfItem>());
                else
                    ((IvrActivity) getContext())
                            .openMenu(mCompanies.get(position).getIvrNumber(), true);
                updateClickTime();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCompanies.size();
    }


    public IVR getItem(int position){
        return mCompanies.get(position);
    }

    public boolean searchCompanies(String query){
        Log.d(TAG, "query " + query);
        if(query == null || query.length() < 3) {
            setSearch(false);
            return false;
        }

        ((IvrActivity) getContext()).sendSearchRequest(query);
        setSearch(true);
        return mCompanies.size() != 0;
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        ((RealmResults)mCompanies).removeChangeListeners();
        super.onDetachedFromRecyclerView(recyclerView);
    }

    public boolean isSearch() {
        return search;
    }

    private void setSearch(boolean search) {
        Log.d(TAG, "set search " + search + " search in adapter " + this.search);
        if (this.search == search)
            return;

        Log.d(TAG, "set new search value " + search);
        this.search = search;
        mCompanies = search ? database.getSearchResultIVRs() : database.getPopularIVRs();
        notifyDataSetChanged();
    }

    public void realmDataWasChanged() {
        Log.d(TAG, "realmDataWasChanged");
        getCompaniesFromRealm();
        ((RealmResults) mCompanies).addChangeListener(mListener);
        Log.d(TAG, "companies search list" + mCompanies.toString());
        notifyDataSetChanged();
    }

    void getCompaniesFromRealm() {
        mCompanies = search ? database.getSearchResultIVRs() : database.getPopularIVRs();
        Log.d(TAG, "size is " + mCompanies.size() + " search " + search);
        if(mCompanies.size() > 0 || search)
            ((IvrActivity) getContext()).dismissProgressDialog();
    }
}
