package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Util;
import com.visumenu.clientmenu.Utils.DtmfItemList;
import com.visumenu.clientmenu.Utils.DtmfUtils;
import com.visumenu.clientmenu.Utils.Favorites;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.IVRMenuItem;
import com.visumenu.clientmenu.model.ParentDtmfItem;
import com.visumenu.clientmenu.service.FetchDataConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * hierarchy menu for displaying IVR menu
 */
public class MenuTreeAdapter extends MenuAdapter {

    private static final int STATE_SUBMENU_REMOVED = -1;
    private static final int STATE_SUBMENU_ADDED = -2;
    private static final int STATE_CALLING = -3;

    Set<String> openedChild = new HashSet<>();
    Set<String> keyItemsWithoutChild;
    Set<String> favoritesSet = Favorites.getFavorites();
    HashMap <String, DtmfItemList> dtmfMap;

    int mainItemsLight;
    int mainItemsDark;
    int childItemsLight;
    int childItemsDark;

    public MenuTreeAdapter(Context context, String ivrNumber, int resId,
                           List<IVRMenuItem> list, Set<String> openedChild) {
        super(context, ivrNumber, resId);

        if(list != null)
            items = list;

        if(openedChild != null)
            this.openedChild = openedChild;

        dtmfMap = Database.listToDtmfMap(ivr.getMenuItems());
        keyItemsWithoutChild = Database.getItemKeysWithoutChild(dtmfMap);
        Resources res = context.getResources();
        mainItemsLight = res.getColor(R.color.mainMenuItemLight);
        mainItemsDark = res.getColor(R.color.mainMenuItemDark);

        childItemsLight = res.getColor(R.color.childMenuItemLight);
        childItemsDark = res.getColor(R.color.childMenuItemDark);
    }

    @Override
    public void onBindViewHolder(MenuHolder holder, int position) {
        IVRMenuItem ivrMenuItem = getItem(position);
        holder.tvName.setText(ivrMenuItem.getName());
        holder.itemView.setTag(position);
        int len = ivrMenuItem.getDtmf().length();
        holder.tvName.setPadding(len < 2 ? 0 : (len - 1) * 50, 0, 0, 0);

        if(position % 2 == 0)
            holder.itemView.setBackgroundColor(len < 2 ? mainItemsLight : childItemsDark);
        else
            holder.itemView.setBackgroundColor(len < 2 ? mainItemsDark : childItemsLight);

        String dtmfStime = ivrMenuItem.getDtmf() + ":" +ivrMenuItem.getIvrMenuStime();

        if(keyItemsWithoutChild.contains(dtmfStime)){
            holder.ivStar.setVisibility(View.VISIBLE);
            if(FetchDataConstants.TYPE_AGENT.equals(ivrMenuItem.getSubtype())){
                holder.ivItemState.setImageResource(R.drawable.ic_call_icon);
            }else if(FetchDataConstants.TYPE_INPUT.equals(ivrMenuItem.getSubtype())){
                holder.ivItemState.setImageResource(R.drawable.ic_info_icon);
            }else
                holder.ivItemState.setImageResource(R.drawable.ic_call_icon_no_agent);

            holder.ivStar.setImageResource(favoritesSet.contains(dtmfStime) ?
                    R.drawable.ic_star_green_pressed : R.drawable.ic_star_green);

            holder.ivItemState.setRotation(0);
            holder.ivStar.setTag(position);
            holder.ivStar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    IVRMenuItem menuItem = getItem((Integer) v.getTag());
                    ImageView ivStar = (ImageView) v;
                    boolean liked = favoritesSet.contains(menuItem.getDtmf() + ":"
                            + menuItem.getIvrMenuStime());
                    ivStar.setImageResource(liked ? R.drawable.ic_star_green : R.drawable.ic_star_green_pressed);

                    ((IvrActivity) mContext).sendLike(ivr.getIvrNumber(),
                            menuItem.getDtmf(),
                            menuItem.getIvrMenuStime(),
                            !liked);

                    Database database = Database.getInstance(mContext);
                    List<ParentDtmfItem> parentItems =
                            DtmfUtils.getParentDtmfItemNames(items,
                                    menuItem.getDtmf(),
                                    menuItem.getIvrMenuStime());

                    database.addFavoritesDtmf(ivr.getIvrNumber(),
                            ivr.getIvrName(),
                            menuItem, parentItems);

                    favoritesSet = Favorites.getFavorites();
                }
            });
        }else{
            holder.ivStar.setVisibility(View.GONE);
            holder.ivItemState.setImageResource(R.drawable.ic_menu_closed);
            holder.ivItemState.setRotation(openedChild.contains(dtmfStime) ? 90 : 0);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick");
                int position = (Integer) v.getTag();
                IVRMenuItem ivrMenuItem = items.get(position);
                updateMenu(
                        position,
                        ivrMenuItem.getDtmf(),
                        ivrMenuItem.getIvrMenuStime()
                );
            }
        });
    }

    @Override
    void onChangeData() {
        dtmfMap = Database.listToDtmfMap(ivr.getMenuItems());
        keyItemsWithoutChild = Database.getItemKeysWithoutChild(dtmfMap);
        notifyDataSetChanged();
    }

    int updateMenu(int position, String dtmf, String stime){
        Log.d(TAG, "update menu in position " + position);
        String dtmfStime = dtmf + ":" + stime;
        if(openedChild.contains(dtmfStime)){
            removeSubMenu(dtmf, stime);
            return STATE_SUBMENU_REMOVED;
        }

        if(!addSubmenu(position, dtmf, stime)){
            List<ParentDtmfItem> parentItems =
                    DtmfUtils.getParentDtmfItemNames(items,
                            dtmf,
                            stime);

            call(items.get(position), parentItems);
            return STATE_CALLING;
        }

        return STATE_SUBMENU_ADDED;
    }

    void removeSubMenu(String dtmf, String stime){
        Log.d(TAG, "removed sub menu with dtmf " + dtmf + " and stime " + stime);
        List<IVRMenuItem> itemsForRemove = new ArrayList<>();
        String dtmfStime = dtmf + ":" + stime;
        for(IVRMenuItem item : items)
            if(item.getDtmf().startsWith(dtmf) &&
                    !item.getDtmf().equals(dtmf) &&
                    item.getIvrMenuStime().startsWith(stime) &&
                    !item.getIvrMenuStime().equals(stime)){
                itemsForRemove.add(item);
                openedChild.remove(item.getDtmf() + ":" + item.getIvrMenuStime());
            }

        items.removeAll(itemsForRemove);
        openedChild.remove(dtmfStime);
        notifyDataSetChanged();
    }

    boolean addSubmenu(int position, String dtmf, String stime) {
        Log.d(TAG, "added submenu to position " + position + " dtmf " + dtmf);

        if(dtmf.length() == 0)
            return false;

        List<IVRMenuItem> ivrMenuItemList =
                Database.getInstance(mContext).getIVRMenuItems(ivr, dtmf, stime);

        Database.getInstance(mContext)
                .addStatItem(ivr.getIvrNumber(), dtmf, stime);
        Log.d(TAG, "setSendStatDataTime");
        Util.setSendStatDataTime(mContext);
        if(ivrMenuItemList.size() == 0){
            return false;
        }

        items.addAll(position + 1, ivrMenuItemList);
        openedChild.add(dtmf + ":" + stime);
        notifyDataSetChanged();

        return true;
    }

    public Set<String> getOpenedChild(){
        return openedChild;
    }
}
