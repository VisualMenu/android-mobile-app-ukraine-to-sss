package com.visumenu.clientmenu.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.visumenu.clientmenu.R;

/**
 * Created by user on 03.05.16.
 */
public class NumberViewHolder extends RecyclerView.ViewHolder {

    ImageView ivNumber;
    TextView tvNumber;

    public NumberViewHolder(View itemView) {
        super(itemView);
        ivNumber = (ImageView) itemView.findViewById(R.id.ivNumber);
        tvNumber = (TextView) itemView.findViewById(R.id.tvNumber);
    }
}
