package com.visumenu.clientmenu.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.Util;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.ParentDtmfItem;
import com.visumenu.clientmenu.model.RecentDtmf;

import java.util.List;

/**
 * Created by user on 09.06.16.
 */
public class FavoritesAdapter extends BaseRecentAdapter{

    public FavoritesAdapter(Context context, int resId) {
        super(context, resId);
    }

    @Override
    public List<RecentDtmf> getData() {
        return Database.getInstance(mContext).getAllFavorites();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RecentDtmf recentDtmf = mRecent.get(position);
        holder.tvName.setText(recentDtmf.getCompanyName());
        holder.tvNumber.setText(recentDtmf.getIVRNumber());
        String menuItemName = recentDtmf.getName();
        if(menuItemName == null || menuItemName.length() == 0){
            holder.tvMenuItemName.setVisibility(View.GONE);
        }else{
            holder.tvMenuItemName.setVisibility(View.VISIBLE);
            holder.tvMenuItemName.setText(menuItemName);
        }

        setStateIcon(holder, recentDtmf.getSubType());
        holder.ivStar.setTag(position);
        holder.itemView.setTag(position);
        holder.ivStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!enableClick())
                    return;

                int position = (Integer) v.getTag();
                RecentDtmf recentDtmf = mRecent.get(position);
                ((IvrActivity) mContext).sendLike(recentDtmf, false);
                Database.getInstance(mContext).removeFavorites(recentDtmf);
                updateClickTime();
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!enableClick())
                    return;

                int position = (Integer) view.getTag();
                RecentDtmf recentDtmf = mRecent.get(position);
                Database.getInstance(mContext)
                        .addStatItem(recentDtmf.getIVRNumber(),
                                recentDtmf.getDtmf(),
                                recentDtmf.getStime());
                Log.d(TAG, "setSendStatDataTime");
                Util.setSendStatDataTime(mContext);

                call(recentDtmf);
                updateClickTime();
            }
        });

        List<ParentDtmfItem> parentItems = recentDtmf.getParentItems();

        Log.d(TAG, "onBindHolder parent items " +
                (parentItems == null ? "null" : "size " + parentItems.size()));


        if(parentItems == null || parentItems.size() == 0){
            holder.llParents.setVisibility(View.GONE);
            return;
        }

        holder.llParents.setVisibility(View.VISIBLE);

        setContent(holder, parentItems);
    }

    private void setContent(ViewHolder holder, List<ParentDtmfItem> parentItems){
        Log.d(TAG, "setContent " + parentItems.size());

        for (int i = 0; i < parentItems.size(); i++){
            ParentDtmfItem parentDtmfItem = parentItems.get(i);
            Log.d(TAG, "setContent " + parentDtmfItem.getItemName());
            TextView textView = ((TextView) holder.llParents.getChildAt(i));
            if(textView == null)
                return;

            textView.setText(parentDtmfItem.getItemName());
            textView.setPadding(parentDtmfItem.getLevel() * 30, 0, 0, 0);
        }
    }
}
