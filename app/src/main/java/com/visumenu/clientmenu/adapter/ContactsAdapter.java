package com.visumenu.clientmenu.adapter;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.model.Database;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by user on 14.07.16.
 */
public class ContactsAdapter  extends RecyclerViewCursorAdapter<ContactHolder>{

    int columnIndexName;
    int columnIndexID;
    int columnHasNumber;
    int columnThumbnail;

    Database database;
    Set<String> favorites;
    LayoutInflater inflater;
    final static String TAG = "ContactsAdapter";
    String[] displayNames;
    boolean search = false;
    Typeface contentFont;

    public ContactsAdapter(Context context, Cursor cursor) {
        super(context, cursor);
        columnIndexID   = cursor.getColumnIndex(ContactsContract.Contacts._ID);
        columnIndexName = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
        columnHasNumber = cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
        columnThumbnail = cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI);
        inflater        = LayoutInflater.from(context);
        displayNames    = new String[cursor.getCount()];
        database        = Database.getInstance(context);
        contentFont     = Typeface.createFromAsset(context.getAssets(), "MyriadPro-Regular.otf");
        initFavorites();
    }

    @Override
    public void onBindViewHolder(ContactHolder viewHolder, Cursor cursor) {
        if (cursor.getInt(columnHasNumber) <= 0){
            viewHolder.itemView.setVisibility(View.GONE);
            return;
        }
        View container = ((ViewGroup) viewHolder.itemView).getChildAt(1);
        container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contactId = (String) v.getTag();
                setDataToContactBadge(contactId);
//                List<String> numbers = getPhoneNumbersById(contactId);
//                if (numbers == null || numbers.size() == 0)
//                    return;
//
//                if (numbers.size() == 1){
//                    ((DialerActivity_) getContext()).performDialWithCheckPermission(numbers.get(0));
//                    return;
//                }
//
//                ArrayAdapter<String> adapter = new ArrayAdapter<>(
//                        getContext(),
//                        R.layout.phonenumber_item,
//                        numbers
//                );
//                MaterialDialog materialDialog = new MaterialDialog.Builder(getContext())
//                        .title(R.string.choice_number)
//                        .adapter(adapter, new MaterialDialog.ListCallback() {
//                            @Override
//                            public void onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
//                                Log.d(TAG, "selection text " + text);
//                                ((DialerActivity_) getContext())
//                                        .performDialWithCheckPermission(text.toString());
//                            }
//                        })
//                        .build();
//                materialDialog.show();
            }
        });
        String name = cursor.getString(columnIndexName);
        int position = 0;
        if (!search) {
             position = cursor.getPosition();
            displayNames[position] = name;
        }

        if (position % 2 == 0)
            viewHolder.itemView.setBackgroundResource(R.color.bgListItemLight);
        else
            viewHolder.itemView.setBackgroundResource(R.color.bgListItemDark);

        if (!search && (position == 0 || name.charAt(0) != displayNames[position - 1].charAt(0))) {
            viewHolder.tvGroupName.setVisibility(View.VISIBLE);
            viewHolder.tvGroupName.setTypeface(contentFont);
            viewHolder.tvGroupName.setText(name.substring(0, 1));
        } else {
            viewHolder.tvGroupName.setVisibility(View.GONE);
        }
        viewHolder.tvName.setTypeface(contentFont);
        viewHolder.tvName.setText(name);
        String id = cursor.getString(columnIndexID);
        container.setTag(id);
        String photoUri = cursor.getString(columnThumbnail);
        if (photoUri != null)
            viewHolder.ivPhoto.setImageURI(Uri.parse(photoUri));
        else
            viewHolder.ivPhoto.setImageResource(R.drawable.contact_image);
        viewHolder.ivStar.setImageResource(favorites.contains(id) ? R.drawable.ic_star_green_pressed : R.drawable.ic_star_green);
        viewHolder.ivStar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String contactId = (String) ((RelativeLayout) v.getParent()).getTag();
                boolean isLiked = favorites.contains(contactId);

                if (isLiked){
                    ((ImageView) v).setImageResource(R.drawable.ic_star_green);
                    favorites.remove(contactId);
                    database.removeContactFromFavorites(contactId);
                } else {
                    ((ImageView) v).setImageResource(R.drawable.ic_star_green_pressed);
                    favorites.add(contactId);
                    database.addFavoriteContact(contactId);
                }

                Log.d(TAG, "like click " + contactId);
            }
        });
    }

    protected void initFavorites(){
        favorites = database.getFavoritesContactIds();
    }

    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ContactHolder(inflater.inflate(R.layout.contact_item, parent, false));
    }

    protected List<String> getPhoneNumbersById(String contactId){
        ContentResolver cr = getContext().getContentResolver();
        Cursor pCur = cr.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                new String[]{contactId}, null);

        if (pCur == null)
            return null;

        ArrayList<String> numbers = new ArrayList<>();
        while (pCur.moveToNext()) {
            String phoneNo = pCur.getString(pCur.getColumnIndex(
                    ContactsContract.CommonDataKinds.Phone.NUMBER));
            numbers.add(phoneNo);
            Log.d(TAG, "Phone No: " + phoneNo);
        }
        pCur.close();
        return numbers;
    }

    public boolean isSearch() {
        return search;
    }

    public void setSearch(boolean search) {
        this.search = search;
    }

    void setDataToContactBadge(String contactId) {

        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI, String.valueOf(contactId));
        intent.setData(uri);
        getContext().startActivity(intent);
    }
}
