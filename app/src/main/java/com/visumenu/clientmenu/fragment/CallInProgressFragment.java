package com.visumenu.clientmenu.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Util;
import com.visumenu.clientmenu.service.FetchDataConstants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by user on 03.06.16.
 */

@EFragment(R.layout.dialer_call_process)
public class CallInProgressFragment extends Fragment{

    @ViewById
    ImageView ivEndCall;

    @ViewById
    TextView tvPhoneNumber;

    @AfterViews
    void setData(){
        Bundle bundle = getArguments();

        String phoneNumber =
                bundle.getString(FetchDataConstants.PHONE_NUMBER);
        tvPhoneNumber.setText(phoneNumber);
    }

    @Click
    void ivEndCall(){
        Util.endCall(getActivity().getApplicationContext());
        getActivity().finish();
    }
}
