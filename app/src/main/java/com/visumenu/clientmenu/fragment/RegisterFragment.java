package com.visumenu.clientmenu.fragment;

import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.visumenu.clientmenu.DialerActivity_;
import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Utils.CallUtils;
import com.visumenu.clientmenu.service.AuthService;
import com.visumenu.clientmenu.service.FetchDataConstants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Fragment for sign up new user
 */

@EFragment(R.layout.register_fragment)
public class RegisterFragment extends Fragment {

    final String TAG = "RegisterFragment";
    private final static int REQUEST_CODE_READ_SMS = 105;

    @ViewById
    Button btnSignUp;

    @ViewById
    EditText etPhone;

    @ViewById
    EditText etPassword;

    @ViewById
    EditText etCountryCode;

    @ViewById
    EditText etEmail;

    @ViewById
    AppCompatCheckBox cbTermsCond;

    @ViewById
    TextView tvTerms;

    @ViewById
    TextView tvPrivacyPolicy;

    @ViewById
    EditText etFirstName;

    @ViewById
    EditText etLastName;

    boolean registerBySocial = false;

    String socialNetwork;
    String userIdSocialNetwork;

    @AfterViews
    void setContent(){

        Bundle bundle = getArguments();
        if(bundle == null){
            setPhoneNumberFromDeviceWithCheckPermission();
            return;
        }

        if(registerBySocial = bundle.getBoolean(IvrActivity.AUTH_BY_SOCIAL_NETWORK, false)) {
            etEmail.setVisibility(View.GONE);
            etPassword.setVisibility(View.GONE);
        }

        String firstName = bundle.getString(FetchDataConstants.FIRST_NAME, "");
        String lastName = bundle.getString(FetchDataConstants.LAST_NAME, "");
        String phoneNumber = bundle.getString(FetchDataConstants.PHONE_NUMBER, "");
        userIdSocialNetwork = bundle.getString(FetchDataConstants.USER_ID_SOCIAL_NETWORK, "");
        socialNetwork = bundle.getString(FetchDataConstants.SOCIAL_NETWORK);
        if (phoneNumber.length() == 0)
            setPhoneNumberFromDeviceWithCheckPermission();
        else
            setPhoneNumber(phoneNumber);
        etFirstName.setText(firstName);
        etLastName.setText(lastName);
    }

    void setPhoneNumberFromDeviceWithCheckPermission(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean hasPermission = (ContextCompat.checkSelfPermission(getActivity().getBaseContext(),
                    Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED);
            if (!hasPermission) {
                requestPermissions(
                        new String[]{Manifest.permission.READ_SMS},
                        REQUEST_CODE_READ_SMS);
                return;
            }
        }

        setPhoneNumberFromDevice();
    }

    void setPhoneNumberFromDevice(){
        String phoneNumber = CallUtils.getPhoneNumber(getActivity().getApplicationContext());
        Log.d(TAG, "phonenumber " + phoneNumber);
        setPhoneNumber(phoneNumber);
    }

    void setPhoneNumber(String phoneNumber){
        if(phoneNumber == null)
            return;

        if(!phoneNumber.startsWith("+"))
            phoneNumber = "+" + phoneNumber;

        String[] parts = CallUtils.getPartsNumber(phoneNumber);

        if(parts == null)
            return;

        etCountryCode.setText(parts[0]);
        etPhone.setText(parts[1]);
    }

    @Click
    void btnSignUp(){
        if(!cbTermsCond.isChecked()){
            ((IvrActivity)getActivity()).showAlertDialog(getString(R.string.please_accept_terms), null);
            return;
        }

        if(etEmail.length() == 0 && !registerBySocial){
            ((IvrActivity)getActivity()).showAlertDialog(getString(R.string.empty_email), null);
            return;
        }

        if(etPhone.length() == 0 || etCountryCode.length() == 0){
            ((IvrActivity)getActivity()).showAlertDialog(getString(R.string.phone_number_required), null);
            return;
        }

        if(etPassword.length() == 0 && !registerBySocial){
            ((IvrActivity)getActivity()).showAlertDialog(getString(R.string.empty_password), null);
            return;
        }

        if(etFirstName.length() == 0){
            ((IvrActivity)getActivity()).showAlertDialog(getString(R.string.first_name_required), null);
            return;
        }

        if(etLastName.length() == 0){
            ((IvrActivity)getActivity()).showAlertDialog(getString(R.string.last_name_required), null);
            return;
        }

        ((IvrActivity)getActivity()).showWaitDialog();
        String phone = etCountryCode.getText().toString() + etPhone.getText().toString();
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        Intent intent = new Intent(getActivity(), AuthService.class);
        intent.putExtra(FetchDataConstants.PHONE_NUMBER, phone);
        intent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_SIGN_UP);
        intent.putExtra(FetchDataConstants.FIRST_NAME, firstName);
        intent.putExtra(FetchDataConstants.LAST_NAME, lastName);
        if (registerBySocial) {
            intent.putExtra(FetchDataConstants.SOCIAL_NETWORK, socialNetwork);
            intent.putExtra(FetchDataConstants.USER_ID_SOCIAL_NETWORK,
                    userIdSocialNetwork + System.currentTimeMillis());
        } else {
            String password = etPassword.getText().toString();
            String email = etEmail.getText().toString();
            intent.putExtra(FetchDataConstants.PASSWORD, password);
            intent.putExtra(FetchDataConstants.EMAIL, email);
        }
        getActivity().startService(intent);
    }

    @Click
    void tvTerms(){
        TextView textView = new TextView(getActivity());
        textView.setText(R.string.terms);
        ((IvrActivity) getActivity()).showDialog(getString(R.string.terms_title),
                getString(R.string.terms), textView, null);

    }

    @Click
    void tvPrivacyPolicy(){
        TextView textView = new TextView(getActivity());
        textView.setText(R.string.terms);
        ((IvrActivity) getActivity()).showDialog(getString(R.string.terms_title),
                getString(R.string.privacy_policy_content), textView, null);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case REQUEST_CODE_READ_SMS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    setPhoneNumberFromDevice();
                } else
                {
                    ((DialerActivity_) getActivity())
                            .showAlertDialog("Read sms permission required", null);
                }
            }
        }
    }
}
