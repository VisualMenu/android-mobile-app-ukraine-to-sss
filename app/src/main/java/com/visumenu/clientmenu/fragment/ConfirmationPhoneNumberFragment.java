package com.visumenu.clientmenu.fragment;

import android.app.Fragment;
import android.widget.Button;
import android.widget.EditText;

import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by user on 18.11.16.
 */

@EFragment(R.layout.confirm_phone_number_fragment)
public class ConfirmationPhoneNumberFragment extends Fragment {

    @ViewById(R.id.btnResend)
    Button btnResend;

    @ViewById
    Button btnConfirm;

    @ViewById
    EditText etConfirmCode;

    @Click
    void btnResend() {
        ((IvrActivity) getActivity()).requestConfirmationCode();
    }

    @Click
    void btnConfirm() {
        if(etConfirmCode.length() == 0) {
            ((IvrActivity) getActivity())
                    .showAlertDialog(getString(R.string.empty_confirm_code), null);
            return;
        }

        ((IvrActivity) getActivity()).sendConfirmationCode(
                etConfirmCode.getText().toString(), true
        );
    }

    public void setConfirmationCode(String SMSCode) {
        etConfirmCode.setText(SMSCode);
    }
}
