package com.visumenu.clientmenu.fragment;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.visumenu.clientmenu.DialerActivity_;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Util;
import com.visumenu.clientmenu.Utils.AutoCompleteTextViewNoEnter;
import com.visumenu.clientmenu.Utils.UnicodeDialerKeyListener;
import com.visumenu.clientmenu.Utils.ViewUtil;
import com.visumenu.clientmenu.adapter.GridNumberAdapter;
import com.visumenu.clientmenu.adapter.OnCallClickListener;
import com.visumenu.clientmenu.adapter.OnDialerButtonClickListener;
import com.visumenu.clientmenu.adapter.OnVoicemailButtonClickListener;
import com.visumenu.clientmenu.adapter.SearchAdapter;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.IVR;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Arrays;
import java.util.List;

import io.realm.RealmResults;

/**
 * Fragment for dial from our app
 */
@EFragment(R.layout.dialer_fragment)
public class DialerFragment extends Fragment {

    private static final int COLUMN_COUNT = 3;

    final String TAG = "DialerFragment";

    @ViewById
    EditText etNumber;

    @ViewById
    Button btnCompanies;

    @ViewById
    RecyclerView rvDial;

    @ViewById
    ImageView ivSearch;

    @ViewById
    AutoCompleteTextViewNoEnter avCompanies;

    @ViewById
    RelativeLayout rlSearch;

    @ViewById
    ImageView ivClear;

    @AfterViews
    void addContent(){
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Geometria-Light.otf");
        etNumber.setTypeface(tf);
        List<String> dialButtons =
                Arrays.asList(getResources().getStringArray(R.array.dial_buttons));
        GridLayoutManager mLayoutManager = new GridLayoutManager(getActivity(), COLUMN_COUNT);
        rvDial.setLayoutManager(mLayoutManager);
        GridNumberAdapter gridNumberAdapter =
                new GridNumberAdapter(getActivity(), dialButtons, R.layout.dialer_item, etNumber);
        rvDial.setAdapter(gridNumberAdapter);
        gridNumberAdapter.setOnDialerButtonClickListener(new OnDialerButtonClickListener() {
            @Override
            public void onButtonClick(String s) {
                etNumber.getText().insert(etNumber.getSelectionEnd(), s);
            }
        });

        etNumber.setTypeface(tf);

        gridNumberAdapter.setOnCallClickListener(new OnCallClickListener() {
            @Override
            public void onCallClick(String number) {
                proceedNumber(number);
            }
        });

        gridNumberAdapter.setOnVoicemailButtonClickListener(new OnVoicemailButtonClickListener() {
            @Override
            public void onVoicemailButtonClick() {
                voicemailCall();
            }
        });

        RealmResults<IVR> items =
                (RealmResults<IVR>) Database.getInstance(getActivity()).getSearchResultIVRs();
        Log.d(TAG, "size " + items);
        SearchAdapter adapter = new SearchAdapter(
                getActivity(),
                R.layout.popup_ivr_item,
                items,
                avCompanies
        );

        avCompanies.setThreshold(3);
        avCompanies.setAdapter(adapter);

        avCompanies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String ivrNumber = ((IVR) parent.getAdapter().getItem(position)).getIvrNumber();
                openMenu(ivrNumber);
                avCompanies.setText("");
            }
        });

        Typeface tfCompanies =
                Typeface.createFromAsset(getActivity().getAssets(), "HelveticaNeueCyr-Roman.otf");
        btnCompanies.setTypeface(tfCompanies, Typeface.NORMAL);

        etNumber.setKeyListener(UnicodeDialerKeyListener.INSTANCE);

        etNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                etNumber.setText(Uri.encode(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    void proceedNumber(String number) {
        Log.d(TAG, "proceedNumber " + number);
        Database database = Database.getInstance(getActivity());
        if(database.hasIvrNumber(number))
            openMenu(number);
        else
            gsmCall(number);
    }

    void gsmCall(String number){
        ((DialerActivity_) getActivity()).performDialWithCheckPermission(number);
    }

    void openMenu(String ivrNumber){
        closeSearch();
        ((DialerActivity_) getActivity()).showMenu(ivrNumber);
    }

    @Click
    void btnCompanies(){
        ((DialerActivity_) getActivity()).openCompanies();
    }

    @Click
    void ivSearch(){
        if (Util.isNeedUpdateIVRs(getActivity().getApplicationContext()))
            ((DialerActivity_) getActivity()).updateIVRlist();
        rlSearch.setVisibility(View.VISIBLE);
        ivSearch.setVisibility(View.GONE);
        avCompanies.requestFocus();
        ViewUtil.openKeyboard(getActivity(), avCompanies);
    }

    @Click
    void ivClear(){
        if(avCompanies.length() == 0)
            closeSearch();
        else
            avCompanies.setText("");
    }

    void closeSearch(){
        ViewUtil.closeKeyboard(getActivity(), avCompanies);
        rlSearch.setVisibility(View.GONE);
        ivSearch.setVisibility(View.VISIBLE);
    }

    void voicemailCall() {
        TelephonyManager telephonyManager =
                (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String number = telephonyManager.getVoiceMailNumber();
        if(number == null || number.equals("")) {
            ((DialerActivity_) getActivity())
                    .showAlertDialog(getString(R.string.number_voicemail_not_set), null);
        } else {
            gsmCall(number);
        }
    }
}
