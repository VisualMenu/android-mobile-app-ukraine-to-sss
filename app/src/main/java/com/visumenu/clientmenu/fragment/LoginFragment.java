package com.visumenu.clientmenu.fragment;

import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.AuthListener;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;
import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Utils.CallUtils;
import com.visumenu.clientmenu.service.AuthService;
import com.visumenu.clientmenu.service.FetchDataConstants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by user on 25.03.16.
 */
@EFragment(R.layout.login_fragment)
public class LoginFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener{

    final String TAG = "LoginFragment";
    private final static int RC_G_SIGN_IN = 10;
    private final static int RC_L_SIGN_IN = 20;
    private final static int REQUEST_CODE_READ_SMS = 106;
    GoogleSignInOptions gso;
    AuthListener mLIAuthnListener;
    GoogleApiClient mGoogleApiClient = null;
    CallbackManager callbackManager;

    @ViewById
    EditText etEmail;

    @ViewById
    EditText etPassword;

    @ViewById
    Button btnSignIn;

    @ViewById
    Button btnSignUp;

    @ViewById
    LoginButton btnFBLogin;

    @ViewById
    TwitterLoginButton btnTwitter;

    @ViewById
    ImageView ivTwitter;

    @ViewById
    ImageView ivLinkedIn;

    @ViewById
    ImageView ivGplus;

    @ViewById
    ImageView ivFacebook;

    String id, snetwork, firstName, lastName;

    void initGAPIClient(){
        if(mGoogleApiClient == null)

            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .addApi(Plus.API)
                    .build();
    }

    @Click
    void ivGplus(){
        initGAPIClient();
        if(mGoogleApiClient != null){
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_G_SIGN_IN);
        }
    }

    @Click
    void ivFacebook(){
        btnFBLogin.performClick();
    }

    @Click
    void ivLinkedIn(){
        mLIAuthnListener = new AuthListener() {
            @Override
            public void onAuthSuccess() {
                Log.d(TAG, "auth linkedIn success");
                String url = "https://api.linkedin.com/v1/people/~";

                APIHelper apiHelper = APIHelper.getInstance(getActivity().getApplicationContext());
                apiHelper.getRequest(getActivity(), url, new ApiListener() {

                    @Override
                    public void onApiSuccess(ApiResponse apiResponse){
                        Log.d(TAG, "linkedin: getting info success " + apiResponse.toString());

                        try {
                            JSONObject response = apiResponse.getResponseDataAsJson();
                            String id = response.getString("id");
                            String firstName = response.getString("firstName");
                            String lastName =response.getString("lastName");
                            register(id, "linkedin", firstName, lastName);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onApiError(LIApiError liApiError) {

                    }
                });
            }

            @Override
            public void onAuthError(LIAuthError error) {
                Log.e(TAG, "auth linkedIn failed " + error.toString());
                // Handle authentication errors
            }
        };

        LISessionManager.getInstance(getActivity()
                .getApplicationContext()).init(getActivity(), buildScope(), mLIAuthnListener, true);
    }

    @Click
    void ivTwitter(){
        btnTwitter.performClick();
    }

    @AfterViews
    void initSocialButtons(){
        callbackManager = CallbackManager.Factory.create();
        gso = new GoogleSignInOptions.Builder()
                .requestEmail()
                .requestScopes(new Scope(Scopes.PROFILE))
                .requestScopes(new Scope(Scopes.PLUS_LOGIN))
                .requestProfile()
                .build();

        btnFBLogin.setFragment(this);
        // Other app specific specialization

        // Callback registration
        btnFBLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook: auth success");

                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {

                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.d(TAG, response.toString());
                        // Get facebook data from login
                        try {
                            String id = "", firstName = "", lastName = "";
                            if (object.has("id"))
                                id = object.getString("id");
                            if (object.has("first_name"))
                                firstName =  object.getString("first_name");
                            if (object.has("last_name"))
                                lastName = object.getString("last_name");
                            register(id, "facebook",firstName , lastName);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id, first_name, last_name");
                request.setParameters(parameters);
                request.executeAsync();

                // App code
            }

            @Override
            public void onCancel() {
            }

            @Override
            public void onError(FacebookException exception) {
            }
        });

        btnTwitter.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                Log.d(TAG, "auth twitter success");
                register(String.valueOf(result.data.getUserId()), "twitter", "", "");
            }

            @Override
            public void failure(TwitterException exception) {
                Log.e(TAG, "auth failed", exception);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "requestCode " + requestCode + " resultCode " + resultCode
                + " data " + (data == null ? null : data.toString()));
        switch (requestCode){

            case RC_G_SIGN_IN:
                handleSignInResult(
                        Auth.GoogleSignInApi.getSignInResultFromIntent(data));
                break;

            case 3672:
                LISessionManager.getInstance(getActivity().getApplicationContext())
                        .onActivityResult(getActivity(), requestCode, resultCode, data);
        }


        btnTwitter.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Click
    void btnSignIn(){
        if(etEmail.getText().length() == 0){
            ((IvrActivity)getActivity()).showAlertDialog(getString(R.string.empty_email), null);
            return;
        }

        if(etPassword.getText().length() == 0){
            ((IvrActivity)getActivity()).showAlertDialog(getString(R.string.empty_password), null);
            return;
        }

        ((IvrActivity) getActivity()).showWaitDialog();
        Intent intent = new Intent(getActivity(), AuthService.class);
        intent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_SIGN_IN);
        String username = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        intent.putExtra(FetchDataConstants.EMAIL, username);
        intent.putExtra(FetchDataConstants.PASSWORD, password);
        getActivity().startService(intent);
    }

    @Click
    void btnSignUp(){
        ((IvrActivity)getActivity()).openRegister();
    }

    @Click
    void btnFBLogin(){

    }

    void showAlert(int resId){

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(TAG, "g sign: connection failed" + connectionResult.toString());
    }

    private static com.linkedin.platform.utils.Scope buildScope() {
        return com.linkedin.platform.utils.Scope.build(
                com.linkedin.platform.utils.Scope.R_BASICPROFILE);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            Log.d(TAG, "result success");
            Log.d(TAG, "google auth: " + result.getSignInAccount().getId());
            GoogleSignInAccount googleSignInAccount =
                    result.getSignInAccount();

            String displayName = googleSignInAccount.getDisplayName();

            Log.d(TAG, "display name: " + displayName);
            if (displayName == null) {
                register(googleSignInAccount.getId(), "gplus", "","");
                return;
            }

            int lastSpacePosition = displayName.lastIndexOf(" ");
            if (lastSpacePosition == -1){
                register(googleSignInAccount.getId(), "gplus", displayName, "");
                return;
            }

            String firstName, lastName;
            firstName = displayName.substring(0, lastSpacePosition);
            lastName = displayName.substring(lastSpacePosition + 1, displayName.length());

            register(googleSignInAccount.getId(), "gplus", firstName, lastName);
//            Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
//            Log.d(TAG, "gplus display name " + person.getDisplayName());


        }
    }

    String getPhoneNumberFromDeviceWithCheckPermission(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean hasPermission = (ContextCompat.checkSelfPermission(getActivity().getBaseContext(),
                    Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED);
            if (!hasPermission) {
                requestPermissions(
                        new String[]{Manifest.permission.READ_SMS},
                        REQUEST_CODE_READ_SMS);
                return null;
            }
        }

        return CallUtils.getPhoneNumber(getActivity().getApplicationContext());
    }

    void register(String id, String snetwork, String firstName, String lastName){
        if(id == null || id.equals(""))
            return;

        String phoneNumber = getPhoneNumberFromDeviceWithCheckPermission();
        if(phoneNumber == null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.id = id;
            this.snetwork = snetwork;
            this.firstName = firstName;
            this.lastName = lastName;
            return;
        }

        register(id, snetwork, firstName, lastName, phoneNumber);
    }

    void register(String id, String snetwork, String firstName, String lastName, String phoneNumber) {
        if (firstName.length() == 0     ||
                lastName.length() == 0  ||
                phoneNumber == null     ||
                phoneNumber.length() == 0){

            Bundle bundle = new Bundle();
            bundle.putBoolean(IvrActivity.AUTH_BY_SOCIAL_NETWORK, true);
            bundle.putString(FetchDataConstants.FIRST_NAME, firstName);
            bundle.putString(FetchDataConstants.LAST_NAME, lastName);
            bundle.putString(FetchDataConstants.PHONE_NUMBER, phoneNumber);
            bundle.putString(FetchDataConstants.SOCIAL_NETWORK, snetwork);
            bundle.putString(FetchDataConstants.USER_ID_SOCIAL_NETWORK,
                    id + System.currentTimeMillis());
            ((IvrActivity) getActivity()).openRegister(bundle);
            return;
        }


        Intent intent = new Intent(getActivity(), AuthService.class);
        intent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_SIGN_UP);
        intent.putExtra(FetchDataConstants.SOCIAL_NETWORK, snetwork);
        intent.putExtra(FetchDataConstants.USER_ID_SOCIAL_NETWORK,
                id + System.currentTimeMillis());
        intent.putExtra(FetchDataConstants.FIRST_NAME, firstName);
        intent.putExtra(FetchDataConstants.LAST_NAME, lastName);
        getActivity().startService(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case REQUEST_CODE_READ_SMS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    String phoneNumber = getPhoneNumberFromDeviceWithCheckPermission();
                    register(id, snetwork, firstName, lastName, phoneNumber);
                } else {
                    register(id, snetwork, firstName, lastName, null);
//                    ((DialerActivity_) getActivity())
//                            .showAlertDialog("Read sms permission required", null);
                }
            }
        }
    }
}
