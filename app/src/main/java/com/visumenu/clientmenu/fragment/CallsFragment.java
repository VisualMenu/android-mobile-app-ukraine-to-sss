package com.visumenu.clientmenu.fragment;

import android.Manifest;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.provider.CallLog;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.visumenu.clientmenu.DialerActivity_;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Utils.DividerItemDecoration;
import com.visumenu.clientmenu.adapter.CallsHistoryAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by user on 20.06.16.
 */
@EFragment(R.layout.calls_fragment)
public class CallsFragment extends Fragment {

    final static int REQUEST_READ_CONTACTS = 101;
    final static int REQUEST_READ_CALLS = 102;

    @ViewById
    RecyclerView rvCalls;

    @AfterViews
    void displayCallsWithCheckPermissions(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean hasPermission = (ContextCompat.checkSelfPermission(getActivity().getBaseContext(),
                    Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED);
            if (!hasPermission) {
                requestPermissions(
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        REQUEST_READ_CALLS);
                return;
            }
        }

        displayCallsHistoryWithCheckContactsPermission();
    }


    void displayCallsHistoryWithCheckContactsPermission(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean hasPermission = (ContextCompat.checkSelfPermission(getActivity().getBaseContext(),
                    Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED);
            if (!hasPermission) {
                requestPermissions(
                        new String[]{Manifest.permission.READ_CONTACTS},
                        REQUEST_READ_CONTACTS);
                return;
            }
        }
        displayCallsHistory();
    }

    void displayCallsHistory() {
        CallsHistoryAdapter adapter = new CallsHistoryAdapter(getActivity(), initCursor());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rvCalls.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL_LIST));
        rvCalls.setLayoutManager(layoutManager);
        rvCalls.setAdapter(adapter);
    }

    @SuppressWarnings("MissingPermission")
    Cursor initCursor(){
        ContentResolver cr = getActivity().getContentResolver();
       return cr.query(CallLog.Calls.CONTENT_URI, null,
                null, null, CallLog.Calls.DATE + " DESC");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case REQUEST_READ_CALLS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    displayCallsHistoryWithCheckContactsPermission();
                } else {
                    ((DialerActivity_) getActivity())
                            .showAlertDialog("Read call log permission required", null);
                }
            }
            break;

            case REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    displayCallsHistory();
                } else {
                    ((DialerActivity_) getActivity())
                            .showAlertDialog("Read contacts permission required", null);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(rvCalls.getAdapter() != null)
            ((CallsHistoryAdapter) rvCalls.getAdapter()).swapCursor(initCursor());
    }
}
