package com.visumenu.clientmenu.fragment;

import android.Manifest;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.visumenu.clientmenu.DialerActivity_;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Utils.DividerItemDecoration;
import com.visumenu.clientmenu.adapter.FavoritesContactsAdapter;
import com.visumenu.clientmenu.model.Database;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Set;

/**
 * Created by user on 20.06.16.
 */
@EFragment(R.layout.favorites_fragment)
public class FavoritesFragment extends Fragment {

    final int REQUEST_READ_CONTACTS = 104;

    @ViewById
    RecyclerView rvFavorites;

    @AfterViews
    void displayContactsWithCheckPermission(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean hasPermission = (ContextCompat.checkSelfPermission(getActivity().getBaseContext(),
                    Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED);
            if (!hasPermission) {
                requestPermissions(
                        new String[]{Manifest.permission.READ_CONTACTS},
                        REQUEST_READ_CONTACTS);
                return;
            }
        }
        displayContacts();
    }

    void displayContacts(){
        Set<String> favorites = Database.getInstance(getActivity()).getFavoritesContactIds();
        Cursor cursor =  FavoritesContactsAdapter.initCursor(getActivity(), favorites);
        if (cursor == null)
            return;

        FavoritesContactsAdapter adapter = new FavoritesContactsAdapter(
                getActivity(),
                cursor,
                favorites
        );
        rvFavorites.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvFavorites.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL_LIST));
        rvFavorites.setAdapter(adapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    displayContacts();
                } else
                {
                    ((DialerActivity_) getActivity())
                            .showAlertDialog("Read contacts permission required", null);
                }
            }
        }
    }
}
