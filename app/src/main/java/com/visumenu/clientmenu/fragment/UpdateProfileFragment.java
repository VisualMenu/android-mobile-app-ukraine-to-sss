package com.visumenu.clientmenu.fragment;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Intent;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.User;
import com.visumenu.clientmenu.service.AuthService;
import com.visumenu.clientmenu.service.FetchDataConstants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by user on 30.03.16.
 */

@EFragment(R.layout.update_profile_fragment)
public class UpdateProfileFragment extends Fragment {

    final String TAG = "UpdateProfileFragment";
    User user;

    @ViewById
    EditText etPhone;

    @ViewById
    EditText etFirst;

    @ViewById
    EditText etGender;

    @ViewById
    EditText etLast;

    @ViewById
    EditText etAddress;

    @ViewById
    EditText etEmail;

    @ViewById
    EditText etLogin;

    @ViewById
    EditText etPassword;

    @ViewById
    Button btnUpdateProfile;

    @AfterViews
    void fillFields(){
        user = Database.getInstance(getActivity()).getUser();
        Log.d(TAG, "access token " + user.getAccessToken());
        if(user == null)
            return;

        etLogin.setText(user.getLogin());
        etPhone.setText(user.getPhoneNumber());
        etAddress.setText(user.getAddress());
        etGender.setText(user.getGender());
        etFirst.setText(user.getFirstName());
        etLast.setText(user.getLastName());
        etEmail.setText(user.getEmail());
        etPassword.setText(user.getPassword());

    }

    @Click
    void btnUpdateProfile(){
        if(hasEmptyFields()){
            new AlertDialog.Builder(getActivity())
                    .setMessage("You have empty fields")
                    .create()
                    .show();
            return;
        }

        String login = etLogin.getText().toString();
        String password = etPassword.getText().toString();
        String firstName = etFirst.getText().toString();
        String lastName = etLast.getText().toString();
        String address = etAddress.getText().toString();
        String email = etEmail.getText().toString();
        String phone = etPhone.getText().toString();
        String gender = etGender.getText().toString();

        Intent intent = new Intent(getActivity(), AuthService.class);
        intent.putExtra(FetchDataConstants.ACCESS_TOKEN, user.getAccessToken());
        intent.putExtra(FetchDataConstants.USERNAME, login);
        intent.putExtra(FetchDataConstants.PASSWORD, password);
        intent.putExtra(FetchDataConstants.FIRST_NAME, firstName);
        intent.putExtra(FetchDataConstants.LAST_NAME, lastName);
        intent.putExtra(FetchDataConstants.ADDRESS, address);
        intent.putExtra(FetchDataConstants.EMAIL, email);
        intent.putExtra(FetchDataConstants.PHONE_NUMBER, phone);
        intent.putExtra(FetchDataConstants.REQUEST_CODE, FetchDataConstants.CODE_UPDATE_PROFILE);
        //TODO change to user data
        intent.putExtra(FetchDataConstants.GENDER, "male");
        getActivity().startService(intent);

//        String accessToken;
        if(user == null) {
            Log.e(TAG, "no entity user");
            return;
        }


    }

    boolean hasEmptyFields(){
        return etLogin.length()     == 0 ||
                etPassword.length() == 0 ||
                etEmail.length()    == 0 ||
                etFirst.length()    == 0 ||
                etLast.length()     == 0 ||
                etGender.length()   == 0 ||
                etAddress.length()  == 0 ;
    }

}
