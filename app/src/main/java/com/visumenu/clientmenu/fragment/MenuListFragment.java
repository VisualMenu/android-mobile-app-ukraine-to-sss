package com.visumenu.clientmenu.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.adapter.MenuTreeAdapter;
import com.visumenu.clientmenu.model.Database;
import com.visumenu.clientmenu.model.IVRMenuItem;
import com.visumenu.clientmenu.service.FetchDataConstants;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Main IVR menu fragment
 */

@EFragment(R.layout.menu_fragment)
public class MenuListFragment extends Fragment {

    final String TAG = "MenuListFragment";

    private final static String TREE_MENU_OPENED_NODES = "flat_menu_opened";
    private final static String TREE_MENU_DTMFS_LIST = "flat_menu";

    String ivrNumber;

    @ViewById
    RecyclerView rvMenu;

    @ViewById
    Button btnBack;

    @ViewById
    TextView tvCompany;

    @ViewById
    TextView tvPhoneNumber;

    MenuTreeAdapter adapter;

    @Nullable
    @Override
    @SuppressWarnings("unchecked")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        ivrNumber = null;
        if(bundle != null)
            ivrNumber = bundle.getString(FetchDataConstants.IVR_NUMBER, "");
        Log.d(TAG, "init menu with ivr number " + ivrNumber);
        ((IvrActivity)getActivity()).fetchMenuData(ivrNumber);
        return null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        HashSet<String> openedNodes = null;
        List<IVRMenuItem> savedMenu = null;
        if(savedInstanceState != null){
            Log.d(TAG, "restore state");
            openedNodes = (HashSet<String>)
                    savedInstanceState.getSerializable(TREE_MENU_OPENED_NODES);

            List<String> dtmfs = (ArrayList<String>)
                    savedInstanceState.getSerializable(TREE_MENU_DTMFS_LIST);
            savedMenu = Database.getInstance(getActivity())
                    .getIvrMenuItems(ivrNumber, dtmfs);
        }
        adapter = new MenuTreeAdapter(getActivity(), ivrNumber,
                R.layout.ivr_menu_item, savedMenu, openedNodes);
    }

    @AfterViews
    void initMenu(){
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        rvMenu.setLayoutManager(mLayoutManager);
        rvMenu.setAdapter(adapter);
        tvCompany.setText(adapter.getIvr().getIvrName());
        tvPhoneNumber.setText(ivrNumber);
    }

    @Click
    void btnBack(){
        getActivity().onBackPressed();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if(adapter == null)
            return;

        Set<String> openedNodes;
        List<IVRMenuItem> flatMenuList;

        if(rvMenu == null)
            return;

        MenuTreeAdapter menuFlatAdapter = (MenuTreeAdapter) rvMenu.getAdapter();
        openedNodes = menuFlatAdapter.getOpenedChild();
        flatMenuList = menuFlatAdapter.getItems();

        outState.putSerializable(TREE_MENU_OPENED_NODES,
                (HashSet) openedNodes);

        List<String> dtmfs = new ArrayList<>();

        if(flatMenuList == null)
            return;

        for(IVRMenuItem ivrMenuItem : flatMenuList)
            dtmfs.add(ivrMenuItem.getDtmf());

        outState.putSerializable(TREE_MENU_DTMFS_LIST,
                (ArrayList)dtmfs);
    }
}
