package com.visumenu.clientmenu.fragment;

import android.app.Fragment;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;

import com.visumenu.clientmenu.IvrActivity;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Utils.ViewUtil;
import com.visumenu.clientmenu.adapter.CompaniesAdapter;
import com.visumenu.clientmenu.adapter.PagerCompaniesAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * This fragment contains all company names with IVR numbers
 *
 */

@EFragment(R.layout.companies_list_fragment)
public class CompaniesListFragment extends Fragment {

    final String TAG = "CompaniesListFragment";
//    @ViewById
//    RecyclerView rvCompanies;

//    @ViewById
    SearchView searchCompanies;

    @ViewById
    ViewPager pagerMenu;

    @ViewById
    TabLayout tlMenu;

    final int[] iconResources = {
            R.drawable.ic_categories_selector,
            R.drawable.ic_recent_selector,
            R.drawable.ic_heart_selector
    };

    @AfterViews
    void initAdapter() {
        PagerCompaniesAdapter adapter =
                new PagerCompaniesAdapter(getActivity(), getResources().getStringArray(R.array.companies_tabs));
        pagerMenu.setAdapter(adapter);
        tlMenu.setupWithViewPager(pagerMenu);
        ViewUtil.addDividersTab(getActivity(), tlMenu, iconResources);
        pagerMenu.setCurrentItem(1);
        searchCompanies = ((IvrActivity) getActivity()).getSearchBar();
        adapter.setQuery(searchCompanies.getQuery().toString());
        tlMenu.getTabAt(0).select();
//        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
//        rvCompanies.setLayoutManager(mLayoutManager);
//        CompaniesAdapter adapter =
//                new CompaniesAdapter(getActivity(), R.layout.companies_list_item);
//
//        rvCompanies.setAdapter(adapter);
//
        searchCompanies.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(final String newText) {
                search(newText);
                return false;
            }
        });

        searchCompanies.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                search("");
//                ((IvrActivity) getActivity()).setIconDialerVisibility(View.VISIBLE);
                return false;
            }
        });

        Typeface tf =
                Typeface.createFromAsset(getActivity().getAssets(), "TimesNewRoman.ttf");
        ViewUtil.changeTabsFont(tlMenu, tf, false, 0);

    }

    void search(final String query) {

        ((PagerCompaniesAdapter) pagerMenu.getAdapter()).setQuery(query);

        if(pagerMenu.getCurrentItem() != 0){
            pagerMenu.setCurrentItem(0);
            pagerMenu.post(new Runnable() {
                @Override
                public void run() {
                    CompaniesAdapter adapter = ((CompaniesAdapter) ((PagerCompaniesAdapter) pagerMenu
                            .getAdapter()).getAdapter(0));
                    if(adapter != null)
                        adapter.searchCompanies(query);
                }
            });
        } else {
            CompaniesAdapter adapter = ((CompaniesAdapter) ((PagerCompaniesAdapter) pagerMenu
                    .getAdapter()).getAdapter(0));
            if(adapter != null)
                adapter.searchCompanies(query);
        }
    }

    public SearchView getSearchCompaniesView() {
        return searchCompanies;
    }

    public void realmDataWasChanged(){
        Log.d(TAG, "realmDataWasChanged");
        RecyclerView.Adapter adapter = ((PagerCompaniesAdapter) pagerMenu.getAdapter()).getAdapter(0);
        if (adapter == null) {
            Log.d(TAG, "adapter null");
            return;
        }
        Log.d(TAG, "adapter not null, call realmDataWasChanged");
        ((CompaniesAdapter) adapter).realmDataWasChanged();
    }
}
