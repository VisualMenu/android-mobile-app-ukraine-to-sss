package com.visumenu.clientmenu.fragment;

import android.Manifest;
import android.app.Fragment;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.QuickContactBadge;

import com.visumenu.clientmenu.DialerActivity_;
import com.visumenu.clientmenu.R;
import com.visumenu.clientmenu.Utils.DividerItemDecoration;
import com.visumenu.clientmenu.adapter.ContactsAdapter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by user on 20.06.16.
 */
@EFragment(R.layout.contacts_fragment)
public class ContactsFragment extends Fragment {

    final static int REQUEST_READ_CONTACTS = 101;
    final static int REQUEST_ADD_CONTACT = 102;

    @ViewById
    RecyclerView rvContacts;

    @ViewById
    AppCompatEditText etSearch;

    @ViewById
    FloatingActionButton fab;

    @ViewById
    QuickContactBadge quickbadge;

    ContactsAdapter adapter;

    ContentResolver cr;
    @AfterViews
    void displayContactsWithCheckPermissions(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean hasPermission = (ContextCompat.checkSelfPermission(getActivity().getBaseContext(),
                    Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED);
            if (!hasPermission) {
                requestPermissions(
                        new String[]{Manifest.permission.READ_CONTACTS},
                        REQUEST_READ_CONTACTS);
                return;
            }
        }

        displayContacts();
    }

    void displayContacts(){
        cr = getActivity().getContentResolver();
        Cursor cur = initCursor("");

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 1){
                    adapter.setSearch(true);
                    adapter.swapCursor(initCursor(s.toString()));
                }else{
                    if (adapter.isSearch()){
                        adapter.swapCursor(initCursor(""));
                        adapter.setSearch(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        adapter = new ContactsAdapter(getActivity(), cur);
        rvContacts.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvContacts.addItemDecoration(new DividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL_LIST));
        rvContacts.setAdapter(adapter);
    }

    Cursor initCursor(String name){
        return cr.query(
                ContactsContract.Contacts.CONTENT_URI,
                null,
                ContactsContract.Contacts.HAS_PHONE_NUMBER + " > 0" +
                        (name.length() == 0 ? "" :
                                " AND " +
                                        ContactsContract.Contacts.DISPLAY_NAME +
                                        " LIKE '" + name + "%'"),
                null,
                ContactsContract.Contacts.DISPLAY_NAME
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case REQUEST_READ_CONTACTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    displayContacts();
                } else
                {
                    ((DialerActivity_) getActivity())
                            .showAlertDialog("Read contacts permission required", null);
                }
            }
        }
    }

    @Click
    void fab(){
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
        startActivity(intent);
    }
}
