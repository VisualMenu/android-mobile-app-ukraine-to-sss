package com.visumenu.clientmenu;

/**
 * Created by user on 14.03.16.
 */
public class Preferences {

    public static final String CALL_PREFERECES = "call_preferences";
    public static final String IVR_NUMBERS_VERSION = "ivr_numbers_versions";
    public static final String OUTGOING_CALL_NUMBER = "outgoing_number";
    public static final String TIME_LAST_CALL = "time_last_call";
}
